﻿(function() {
	'use strict';
	var $asm = {};
	global.JSLib = global.JSLib || {};
	global.JSLib.Controller = global.JSLib.Controller || {};
	global.JSLib.Data = global.JSLib.Data || {};
	global.JSLib.DesktopComponent = global.JSLib.DesktopComponent || {};
	global.JSLib.Extension = global.JSLib.Extension || {};
	global.JSLib.Extension.md5 = global.JSLib.Extension.md5 || {};
	global.JSLib.Helper = global.JSLib.Helper || {};
	global.JSLib.IScrollWraper = global.JSLib.IScrollWraper || {};
	global.JSLib.Map = global.JSLib.Map || {};
	global.JSLib.Mobile = global.JSLib.Mobile || {};
	global.JSLib.Oauth = global.JSLib.Oauth || {};
	global.JSLib.Plugin = global.JSLib.Plugin || {};
	global.JSLib.ResourceManager = global.JSLib.ResourceManager || {};
	global.JSLib.UnionLogin = global.JSLib.UnionLogin || {};
	global.JSLib.Util = global.JSLib.Util || {};
	global.JSLib.WebSql = global.JSLib.WebSql || {};
	global.JSLib.Widget = global.JSLib.Widget || {};
	global.JSLib.Widget.Behavior = global.JSLib.Widget.Behavior || {};
	global.JSLib.Widget.Common = global.JSLib.Widget.Common || {};
	global.JSLib.Widget.Pager = global.JSLib.Widget.Pager || {};
	global.JSLib.Widget.Panel = global.JSLib.Widget.Panel || {};
	global.JSLib.Widget.SwipeView = global.JSLib.Widget.SwipeView || {};
	global.JSLib.Widget.ViewPager = global.JSLib.Widget.ViewPager || {};
	global.Phone = global.Phone || {};
	global.System = global.System || {};
	global.System.ComponentModel = global.System.ComponentModel || {};
	global.System.ComponentModel.DataAnnotations = global.System.ComponentModel.DataAnnotations || {};
	global.System.ComponentModel.DataAnnotations.Schema = global.System.ComponentModel.DataAnnotations.Schema || {};
	global.System.Web = global.System.Web || {};
	global.System.Web.WebPages = global.System.Web.WebPages || {};
	ss.initAssembly($asm, 'JSLib');
	////////////////////////////////////////////////////////////////////////////////
	// BasePageEx
	var $BasePageEx = function() {
	};
	$BasePageEx.__typeName = 'BasePageEx';
	$BasePageEx.DataBind = function(page, data) {
		return $JSLib_Data_DataBindHelper.Bind(data, page.UniqueId);
	};
	global.BasePageEx = $BasePageEx;
	////////////////////////////////////////////////////////////////////////////////
	// ConsoleEx
	var $ConsoleEx = function() {
	};
	$ConsoleEx.__typeName = 'ConsoleEx';
	global.ConsoleEx = $ConsoleEx;
	////////////////////////////////////////////////////////////////////////////////
	// ControllerBaseEx
	var $ControllerBaseEx = function() {
	};
	$ControllerBaseEx.__typeName = 'ControllerBaseEx';
	$ControllerBaseEx.DataBind = function(control, data) {
		return $JSLib_Data_DataBindHelper.Bind(data, control.UniqueId);
	};
	global.ControllerBaseEx = $ControllerBaseEx;
	////////////////////////////////////////////////////////////////////////////////
	// CoreEx
	var $CoreEx = function() {
	};
	$CoreEx.__typeName = 'CoreEx';
	$CoreEx.Abs = function(data) {
		return Math.abs(data);
	};
	$CoreEx.Abs$1 = function(data) {
		return Math.abs(data);
	};
	$CoreEx.ToInt = function(target, defaultValue) {
		var result = parseInt(target, 10);
		if (isNaN(result)) {
			return defaultValue;
		}
		return result;
	};
	$CoreEx.ToIntNullable = function(target, defaultValue) {
		if (ss.isNullOrEmptyString(target)) {
			return null;
		}
		var result = parseInt(target);
		if (isNaN(result)) {
			return null;
		}
		if (result === -1) {
			return null;
		}
		return result;
	};
	$CoreEx.ToDouble = function(target) {
		return parseFloat(target);
	};
	$CoreEx.ToDoubleWithDefault = function(target, defaultValue) {
		if (ss.isNullOrEmptyString(target)) {
			return defaultValue;
		}
		var result = parseFloat(target);
		if (isNaN(result)) {
			return defaultValue;
		}
		return result;
	};
	$CoreEx.ToDoubleNullable = function(target) {
		if (ss.isNullOrEmptyString(target)) {
			return null;
		}
		var result = parseFloat(target);
		if (isNaN(result)) {
			return null;
		}
		return result;
	};
	$CoreEx.Ensure = function(target, length) {
		if (ss.isNullOrEmptyString(target)) {
			return '';
		}
		if (target.length <= length) {
			return target;
		}
		return target.substr(0, length) + '...';
	};
	$CoreEx.ToEnumNameString = function(T) {
		return function(target) {
			return ss.Enum.toString(T, target);
		};
	};
	$CoreEx.ToEnumNameString$1 = function(T) {
		return function(target) {
			return ss.Enum.toString(T, target);
		};
	};
	$CoreEx.Clone = function(T) {
		return function(target) {
			return JSON.parse(JSON.stringify(target));
		};
	};
	$CoreEx.CloneBySerilization = function(T) {
		return function(target) {
			return JSON.parse(JSON.stringify(target));
		};
	};
	$CoreEx.ToDebugString = function(T) {
		return function(target) {
			return JSON.stringify(target);
		};
	};
	$CoreEx.ToAbsoluteUrl = function(target) {
		if (!ss.startsWithString(target, 'http') && !ss.startsWithString(target, window.location.origin)) {
			return window.location.origin + target;
		}
		else {
			return target;
		}
	};
	$CoreEx.ToPureText = function(target) {
		return $(target).text();
	};
	$CoreEx.AsImageElement = function(target) {
		return target[0];
	};
	$CoreEx.AsInputElement = function(target) {
		return target[0];
	};
	$CoreEx.AppendFormat = function(target, format, value) {
		return target.append(ss.formatString.apply(null, [format].concat(value)));
	};
	global.CoreEx = $CoreEx;
	////////////////////////////////////////////////////////////////////////////////
	// CssEx
	var $CssEx = function() {
	};
	$CssEx.__typeName = 'CssEx';
	$CssEx.PxToInt = function(target) {
		if (ss.isNullOrEmptyString(target)) {
			return 0;
		}
		if (!ss.endsWithString(target, 'px')) {
			return 0;
		}
		var indexOfPx = target.indexOf('px');
		return $CoreEx.ToInt(target.substr(0, indexOfPx), 0);
	};
	global.CssEx = $CssEx;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Tool.CssGeter
	var $CssGeter = function() {
	};
	$CssGeter.__typeName = 'CssGeter';
	$CssGeter.GetCssPath = function(element, parent, output) {
		var data = [];
		var current = $(element);
		data.push(current);
		current.parentsUntil('.ui-page').each(function(index, element1) {
			data.push($(element1));
		});
		var pageroot = current.parents('.ui-page');
		if (pageroot.length === 0) {
			return '';
		}
		parent = pageroot.attr('class').split(String.fromCharCode(32))[0];
		var sb = new ss.StringBuilder();
		sb.append('.').append(parent);
		data.reverse();
		for (var $t1 = 0; $t1 < data.length; $t1++) {
			var item = data[$t1];
			sb.append(' ');
			if (!ss.isNullOrEmptyString(item.attr('id'))) {
				sb.append('#').append(item.attr('id'));
			}
			else if (!ss.isNullOrEmptyString(item.attr('class'))) {
				sb.append('.').append(item.attr('class'));
			}
			else {
				sb.append(item[0].tagName);
			}
		}
		sb.append('{}');
		if (output) {
			console.info(sb.toString());
		}
		return sb.toString();
	};
	$CssGeter.ListCssPath = function(element) {
		var current = $(element);
		var data = [];
		$CssGeter.$IterateItem(current, data);
		var sb = new ss.StringBuilder();
		for (var $t1 = 0; $t1 < data.length; $t1++) {
			var item = data[$t1];
			sb.appendLine(item);
		}
		console.info(sb.toString());
	};
	$CssGeter.$IterateItem = function(jqueryObject, data) {
		if (jqueryObject.length > 0) {
			jqueryObject.each(function(index, element) {
				data.push($CssGeter.GetCssPath(element, '', false));
			});
			$CssGeter.$IterateItem(jqueryObject.children(), data);
		}
	};
	global.CssGeter = $CssGeter;
	////////////////////////////////////////////////////////////////////////////////
	// DateTimeEx
	var $DateTimeEx = function() {
	};
	$DateTimeEx.__typeName = 'DateTimeEx';
	$DateTimeEx.ToDateTime = function(datetimeStr) {
		var result = moment(datetimeStr)._d;
		if (isNaN(result)) {
			return null;
		}
		else {
			return result;
		}
	};
	$DateTimeEx.ToDateTimeForce$1 = function(datetimeStr) {
		var result = moment(datetimeStr)._d;
		return result;
	};
	$DateTimeEx.ToDateTimeForce = function(dateTime) {
		var result = moment(dateTime)._d;
		return result;
	};
	$DateTimeEx.ToDateStr = function(target, format) {
		return $DateTimeEx.ToDateString(target, format);
	};
	$DateTimeEx.ToDateString = function(target, format) {
		if (ss.staticEquals(target, null)) {
			return '';
		}
		var type = ss.getInstanceType(target);
		if (ss.referenceEquals(type, String)) {
			target = $DateTimeEx.ToDateTime(target);
		}
		if (!ss.staticEquals(target, null)) {
			return ss.formatDate(ss.unbox(target), format);
		}
		return '';
	};
	$DateTimeEx.ToDateTimeStr = function(target, format) {
		return $DateTimeEx.ToDateString(target, format);
	};
	$DateTimeEx.FakeDateTimeToStr = function(target, format) {
		return ss.formatDate($DateTimeEx.ToDateTimeForce$1(target), format);
	};
	$DateTimeEx.FakeDateTimeToStr$1 = function(target, format) {
		if (ss.staticEquals(target, null)) {
			return '';
		}
		return ss.formatDate($DateTimeEx.ToDateTimeForce$1(target), format);
	};
	global.DateTimeEx = $DateTimeEx;
	////////////////////////////////////////////////////////////////////////////////
	// DateTimeExtension
	var $DateTimeExtension = function() {
	};
	$DateTimeExtension.__typeName = 'DateTimeExtension';
	$DateTimeExtension.JsonDateToString = function(value, format) {
		return ss.formatDate(new Date(parseInt(ss.replaceAllString(ss.replaceAllString(value.toString(), '/Date(', ''), ')/', ''))), format);
	};
	global.DateTimeExtension = $DateTimeExtension;
	////////////////////////////////////////////////////////////////////////////////
	// DebugEx
	var $DebugEx = function() {
	};
	$DebugEx.__typeName = 'DebugEx';
	$DebugEx.ExportMe = function(target, name) {
		var windowabc = window;
		windowabc[name] = target;
	};
	global.DebugEx = $DebugEx;
	////////////////////////////////////////////////////////////////////////////////
	// ElementEx
	var $ElementEx = function() {
	};
	$ElementEx.__typeName = 'ElementEx';
	global.ElementEx = $ElementEx;
	////////////////////////////////////////////////////////////////////////////////
	// EnumImport
	var $EnumImport = function() {
	};
	$EnumImport.__typeName = 'EnumImport';
	global.EnumImport = $EnumImport;
	////////////////////////////////////////////////////////////////////////////////
	// FileReaderUtil
	var $FileReaderUtil = function() {
	};
	$FileReaderUtil.__typeName = 'FileReaderUtil';
	$FileReaderUtil.LoadComplete = function(target) {
		var tsk = new ss.TaskCompletionSource();
		target.onload = ss.delegateCombine(target.onload, function(event) {
			tsk.setResult(true);
		});
		return tsk.task;
	};
	global.FileReaderUtil = $FileReaderUtil;
	////////////////////////////////////////////////////////////////////////////////
	// FlagEnumExtension
	var $FlagEnumExtension = function() {
	};
	$FlagEnumExtension.__typeName = 'FlagEnumExtension';
	$FlagEnumExtension.HasFlag$1 = function(value, flagValue) {
		return (value & flagValue) === flagValue;
	};
	$FlagEnumExtension.HasFlag = function(value, flagValue) {
		return (value & flagValue) === flagValue;
	};
	global.FlagEnumExtension = $FlagEnumExtension;
	////////////////////////////////////////////////////////////////////////////////
	// ForeignKeyAttribute
	var $ForeignKeyAttribute = function() {
	};
	$ForeignKeyAttribute.__typeName = 'ForeignKeyAttribute';
	global.ForeignKeyAttribute = $ForeignKeyAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// FromBodyAttribute
	var $FromBodyAttribute = function() {
	};
	$FromBodyAttribute.__typeName = 'FromBodyAttribute';
	global.FromBodyAttribute = $FromBodyAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// GenerateInfoAttribute
	var $GenerateInfoAttribute = function() {
		this.$2$ClassNameField = null;
		this.$2$BaseClassNameField = null;
		this.$2$UsingField = null;
		this.$2$NameSpaceField = null;
	};
	$GenerateInfoAttribute.__typeName = 'GenerateInfoAttribute';
	global.GenerateInfoAttribute = $GenerateInfoAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// HttpPostAttribute
	var $HttpPostAttribute = function() {
	};
	$HttpPostAttribute.__typeName = 'HttpPostAttribute';
	global.HttpPostAttribute = $HttpPostAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// IScrollEx
	var $IScrollEx = function() {
	};
	$IScrollEx.__typeName = 'IScrollEx';
	$IScrollEx.GotoTop = function(iscroll) {
		iscroll.scrollTo(0, 0, 100, false);
	};
	global.IScrollEx = $IScrollEx;
	////////////////////////////////////////////////////////////////////////////////
	// jQueryObjectEx
	var $jQueryObjectEx = function() {
	};
	$jQueryObjectEx.__typeName = 'jQueryObjectEx';
	$jQueryObjectEx.SetEnable = function(target, isEnable) {
		if (isEnable) {
			target.removeAttr('disabled');
		}
		else {
			target.attr('disabled', 'disabled');
		}
	};
	$jQueryObjectEx.SetReadonly = function(target, isReadOnly) {
		if (!isReadOnly) {
			target.removeAttr('readonly');
		}
		else {
			target.attr('readonly', 'readonly');
		}
	};
	$jQueryObjectEx.IsEnable = function(target) {
		return ss.isNullOrEmptyString(target.attr('disabled'));
	};
	$jQueryObjectEx.SetCheckState = function(target, isChecked) {
		if (isChecked) {
			target.attr('checked', 'checked');
		}
		else {
			target.removeAttr('checked');
		}
	};
	$jQueryObjectEx.IsChecked = function(target) {
		if (target.length < 0) {
			return false;
		}
		return target[0].checked;
	};
	$jQueryObjectEx.SetVisible = function(target, isVisible) {
		return (isVisible ? target.show() : target.hide());
	};
	$jQueryObjectEx.GetSelectedText = function(target) {
		return target.find(':checked').text();
	};
	$jQueryObjectEx.GetSelectedOption = function(target) {
		return target.find(':checked');
	};
	$jQueryObjectEx.IsVisible = function(target) {
		return target.is(':visible');
	};
	$jQueryObjectEx.ParentUntil = function(target, selector) {
		var temp = target.parent();
		while (temp.length > 0 && !temp.is(selector)) {
			temp = temp.parent();
		}
		return temp;
	};
	$jQueryObjectEx.ShowAsBox = function(target) {
		return target.css('display', '-webkit-box');
	};
	$jQueryObjectEx.Scrollable = function(target) {
		var wraper = target.wrapInner('<div  />').children()[0];
		var iscroll = new iScroll(wraper, null);
		return iscroll;
	};
	$jQueryObjectEx.TouchStart$1 = function(target, eventHandler) {
		return target.on('touchstart', eventHandler);
	};
	$jQueryObjectEx.TouchStart = function(target, eventHandler) {
		return target.on('touchstart', eventHandler);
	};
	$jQueryObjectEx.Tap$1 = function(target, eventHandler) {
		return target.on('tap', eventHandler);
	};
	$jQueryObjectEx.Tap = function(target, eventHandler) {
		return target.on('tap', eventHandler);
	};
	$jQueryObjectEx.Touch$1 = function(target, eventHandler) {
		return target.on('touch', eventHandler);
	};
	$jQueryObjectEx.Touch = function(target, eventHandler) {
		return target.on('touch', eventHandler);
	};
	$jQueryObjectEx.TotalHeight = function(target) {
		var result = 0;
		var lastMarginBottom = 0;
		target.each(function(index, element) {
			var targetobject = $(element);
			//元素高度
			if ($JSLib_JSLibConfig.IsUseZepto) {
				result += targetobject.height();
			}
			else {
				result += targetobject.outerHeight();
			}
			//marginTop
			var marginTop = $jQueryObjectEx.MarginTop(targetobject);
			result += Math.max(lastMarginBottom, marginTop);
			//marginBottom
			lastMarginBottom = $jQueryObjectEx.MarginBottom(targetobject);
		});
		//最后一个元素的MarginBottom;
		result += lastMarginBottom;
		return result;
	};
	$jQueryObjectEx.MarginTop = function(target) {
		if (target.length < 0) {
			return 0;
		}
		var marginTop = $CssEx.PxToInt(window.getComputedStyle(target[0])['marginTop']);
		return marginTop;
	};
	$jQueryObjectEx.MarginBottom = function(target) {
		if (target.length < 0) {
			return 0;
		}
		var marginTop = $CssEx.PxToInt(window.getComputedStyle(target[0])['marginBottom']);
		return marginTop;
	};
	$jQueryObjectEx.BackgroundImage = function(target, imageUrl) {
		target.each(function(index, element) {
			element.style.backgroundImage = ss.formatString('url({0})', imageUrl);
		});
		return target;
	};
	$jQueryObjectEx.GetBackgroundImage = function(target) {
		var url = target[0].style.backgroundImage;
		if (ss.isNullOrEmptyString(url)) {
			return '';
		}
		var beginIndex = url.indexOf('url(') + 4;
		var endIndex = url.indexOf(')');
		return url.substr(beginIndex, endIndex - beginIndex);
	};
	$jQueryObjectEx.ToJqueryObject = function(target) {
		return $(target);
	};
	$jQueryObjectEx.ToJqueryObject$1 = function(target) {
		return $(target.toString());
	};
	$jQueryObjectEx.TranslateX = function(target, x) {
		return target.css('transform', ss.formatString('translateX({0}px)', x));
	};
	$jQueryObjectEx.TranslateY = function(target, y) {
		return target.css('transform', ss.formatString('translateY({0}px)', y));
	};
	$jQueryObjectEx.MinusHalfHeightMarginTop = function(target) {
		if (ss.isValue(target) && target.length > 0) {
			target.each(function(index, element) {
				element.style.marginTop = ss.Int32.div(-$(element).outerHeight(), 2) + 'px';
			});
		}
		return target;
	};
	$jQueryObjectEx.FindTextNode = function(taret) {
		var data = [];
		var $t1 = taret[0].childNodes;
		for (var $t2 = 0; $t2 < $t1.length; $t2++) {
			var ele = $t1[$t2];
			if (ele.nodeType === 3) {
				data.push(ele);
			}
		}
		return Array.prototype.slice.call(data);
	};
	$jQueryObjectEx.IsAll = function(target, predicateFunc) {
		var totalCount = target.length;
		var actualCount = 0;
		target.each(function(index, element) {
			if (predicateFunc(element)) {
				actualCount++;
			}
		});
		return totalCount === actualCount;
	};
	$jQueryObjectEx.IsOverLapRec = function(target, dest) {
		var rect = target[0].getBoundingClientRect();
		var x1 = rect.left;
		var y1 = rect.top;
		var w1 = rect.width;
		var h1 = rect.height;
		var rect2 = dest[0].getBoundingClientRect();
		var x2 = rect2.left;
		var y2 = rect2.top;
		var w2 = rect2.width;
		var h2 = rect2.top;
		var xfitable = x2 + w2 >= x1 && x2 <= x1 + w1;
		var yfitable = y2 + h2 >= y1 && y2 <= y1 + h1;
		return xfitable && yfitable;
	};
	$jQueryObjectEx.CenterOver = function(target, dest) {
		var pos = dest.offset();
		var containerRec = dest[0].getBoundingClientRect();
		var targetRec = target[0].getBoundingClientRect();
		pos.left += ss.Int32.div(containerRec.width - targetRec.width, 2);
		pos.top += ss.Int32.div(containerRec.height - targetRec.height, 2);
		target.offset(pos);
		return pos;
	};
	$jQueryObjectEx.Top = function(target, value) {
		target.each(function(index, element) {
			element.style.top = value + 'px';
		});
		return target;
	};
	$jQueryObjectEx.Right = function(target, value) {
		target.each(function(index, element) {
			element.style.right = value + 'px';
		});
		return target;
	};
	$jQueryObjectEx.Bottom = function(target, value) {
		target.each(function(index, element) {
			element.style.bottom = value + 'px';
		});
		return target;
	};
	$jQueryObjectEx.Left = function(target, value) {
		target.each(function(index, element) {
			element.style.left = value + 'px';
		});
		return target;
	};
	$jQueryObjectEx.SlidIn = function(target, slideInClass, delay) {
		var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1;
		var $sm = function() {
			try {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							//return target.AddClass("SlideInPage").Animate(new System.Collections.JsDictionary("left", "0"), EffectDuration.Fast, EffectEasing.Swing, delegate
							//{
							//    target.RemoveClass("SlideInPage");
							//});
							target.addClass(slideInClass);
							if (delay) {
								$t1 = ss.Task.delay(500);
								$state = 2;
								$t1.continueWith($sm);
								return;
							}
							$state = 1;
							continue $sm1;
						}
						case 2: {
							$state = -1;
							$t1.getAwaitedResult();
							$state = 1;
							continue $sm1;
						}
						case 1: {
							$state = -1;
							target.removeClass(slideInClass);
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
				$tcs.setResult(null);
			}
			catch ($t2) {
				$tcs.setException(ss.Exception.wrap($t2));
			}
		};
		$sm();
		return $tcs.task;
	};
	$jQueryObjectEx.SlidOutTask = function(target, slideInReverseClass, delay) {
		var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1;
		var $sm = function() {
			try {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							target.addClass(slideInReverseClass);
							if (delay) {
								$t1 = ss.Task.delay(500);
								$state = 2;
								$t1.continueWith($sm);
								return;
							}
							$state = 1;
							continue $sm1;
						}
						case 2: {
							$state = -1;
							$t1.getAwaitedResult();
							$state = 1;
							continue $sm1;
						}
						case 1: {
							$state = -1;
							target.hide();
							//target.RemoveClass(slideInReverseClass);
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
				$tcs.setResult(null);
			}
			catch ($t2) {
				$tcs.setException(ss.Exception.wrap($t2));
			}
		};
		$sm();
		return $tcs.task;
	};
	$jQueryObjectEx.GetEnumValue = function(T) {
		return function(target, nullValue) {
			var value = $CoreEx.ToInt(target.val(), 0);
			if (value === nullValue) {
				return ss.getDefaultValue(T);
			}
			else {
				return value;
			}
		};
	};
	$jQueryObjectEx.IsValidateForm = function(target) {
		var inputs = target.find('input[required]').toArray();
		for (var $t1 = 0; $t1 < inputs.length; $t1++) {
			var item = inputs[$t1];
			var targetEle = $(item);
			if (ss.isNullOrEmptyString(targetEle.val())) {
				targetEle.focus();
				return false;
			}
		}
		return true;
	};
	global.jQueryObjectEx = $jQueryObjectEx;
	////////////////////////////////////////////////////////////////////////////////
	// KeyAttribute
	var $KeyAttribute = function() {
	};
	$KeyAttribute.__typeName = 'KeyAttribute';
	global.KeyAttribute = $KeyAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// NotMappedAttribute
	var $NotMappedAttribute = function() {
	};
	$NotMappedAttribute.__typeName = 'NotMappedAttribute';
	global.NotMappedAttribute = $NotMappedAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// ObjectEx
	var $ObjectEx = function() {
	};
	$ObjectEx.__typeName = 'ObjectEx';
	$ObjectEx.ToJson = function(target) {
		if (ss.isNullOrUndefined(target)) {
			return '';
		}
		return JSON.stringify(target);
	};
	$ObjectEx.FromJson = function(T) {
		return function(target) {
			if (ss.isNullOrEmptyString(target)) {
				return ss.getDefaultValue(T);
			}
			return JSON.parse(target);
		};
	};
	$ObjectEx.Base64EncodeWithJsonData = function(target) {
		if (ss.isNullOrUndefined(target)) {
			return '';
		}
		return Base64.encode($ObjectEx.ToJson(target));
	};
	$ObjectEx.Base64DecodeWithJsonData = function(T) {
		return function(target) {
			if (ss.isNullOrEmptyString(target)) {
				return ss.getDefaultValue(T);
			}
			return $ObjectEx.FromJson(T).call(null, Base64.decode(target));
		};
	};
	$ObjectEx.CastTo = function(T) {
		return function(data) {
			if (ss.isNullOrUndefined(data)) {
				return ss.getDefaultValue(T);
			}
			var target = ss.createInstance(T);
			var props = data;
			var $t1 = new ss.ObjectEnumerator(props);
			try {
				while ($t1.moveNext()) {
					var prop = $t1.current();
					target[prop.key] = prop.value;
				}
			}
			finally {
				$t1.dispose();
			}
			return target;
		};
	};
	$ObjectEx.ParseJsonTo = function(T) {
		return function(target) {
			return JSON.parse(target);
		};
	};
	$ObjectEx.ToEnumString = function(T) {
		return function(target) {
			return ss.Enum.toString(T, target);
		};
	};
	global.ObjectEx = $ObjectEx;
	////////////////////////////////////////////////////////////////////////////////
	// RequiredAttribute
	var $RequiredAttribute = function() {
	};
	$RequiredAttribute.__typeName = 'RequiredAttribute';
	global.RequiredAttribute = $RequiredAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// StringHelper
	var $StringHelper = function() {
	};
	$StringHelper.__typeName = 'StringHelper';
	$StringHelper.ToNatureTime = function(datetime) {
		var now = new Date();
		var timespan = new ss.TimeSpan($StringHelper.GetTicks(now) - $StringHelper.GetTicks(datetime));
		var result = '';
		if ((timespan.ticks / 864000000000 | 0) > 2) {
			result = ss.formatString('{0}天前', timespan.ticks / 864000000000 | 0);
		}
		else if ((timespan.ticks / 864000000000 | 0) > 1) {
			result = ss.formatString('昨天');
		}
		else if ((timespan.ticks / 36000000000 % 24 | 0) > 1) {
			result = ss.formatString('{0}小时前', timespan.ticks / 36000000000 % 24 | 0);
		}
		else if ((timespan.ticks / 600000000 % 60 | 0) > 1) {
			result = ss.formatString('{0}分钟前', timespan.ticks / 600000000 % 60 | 0);
		}
		else {
			result = ss.formatString('刚刚');
		}
		return result;
	};
	$StringHelper.GetTicks = function(dateTime) {
		// the number of .net ticks at the unix epoch
		var epochTicks = 6.21355968E+17;
		// there are 10000 .net ticks per millisecond
		var ticksPerMillisecond = 10000;
		// calculate the total number of .net ticks for your date
		var ticks = epochTicks + dateTime.getTime() * ticksPerMillisecond;
		return ticks;
	};
	$StringHelper.FormatBRToHtml = function(target) {
		if (ss.isNullOrEmptyString(target)) {
			return target;
		}
		var result = ss.replaceAllString(ss.replaceAllString(ss.replaceAllString(ss.replaceAllString(target, '\r\n', '<br/>'), '\n\r', '<br/>'), '\r', '<br/>'), '\n', '<br/>');
		result = ss.replaceAllString(result, '<br>', '<br/>');
		for (var i = 0; i < 6; i++) {
			result = ss.replaceAllString(result, '<br/><br/>', '<br/>');
		}
		return result;
	};
	$StringHelper.GetLeftTime = function(target) {
		return new ss.TimeSpan((target - new Date()) * 10000);
	};
	$StringHelper.WithDefault = function(target, defaultValue) {
		if (ss.isNullOrEmptyString(target)) {
			return defaultValue;
		}
		else {
			return target;
		}
	};
	$StringHelper.RemoveEndStr = function(target, endStr) {
		if (ss.endsWithString(target, endStr)) {
			return ss.removeString(target, target.lastIndexOf(endStr), endStr.length);
		}
		else {
			return target;
		}
	};
	$StringHelper.WithBase64ImageHeader = function(target) {
		return 'data:image/png;charset=utf-8;base64,' + target;
	};
	global.StringHelper = $StringHelper;
	////////////////////////////////////////////////////////////////////////////////
	// StringLengthAttribute
	var $StringLengthAttribute = function(length) {
	};
	$StringLengthAttribute.__typeName = 'StringLengthAttribute';
	global.StringLengthAttribute = $StringLengthAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// TableAttribute
	var $TableAttribute = function(tableName) {
	};
	$TableAttribute.__typeName = 'TableAttribute';
	global.TableAttribute = $TableAttribute;
	////////////////////////////////////////////////////////////////////////////////
	// UIDataEx
	var $UIDataEx = function() {
	};
	$UIDataEx.__typeName = 'UIDataEx';
	$UIDataEx.GetBindData = function(T) {
		return function(ele) {
			var result = $UIDataEx.GetElementBindData(ele);
			return result;
		};
	};
	$UIDataEx.GetBindData$1 = function(T) {
		return function(ele) {
			return $UIDataEx.GetBindData(T).call(null, ele[0]);
		};
	};
	$UIDataEx.GetElementBindData = function(ele) {
		if (ss.isNullOrUndefined(ele)) {
			return null;
		}
		var bindid = ele.getAttribute('data-c-bindid');
		if (ss.isNullOrEmptyString(bindid)) {
			return null;
		}
		var bindData = Enumerable.from($JSLib_Data_DataBindHelper.bindDatas).selectMany(function(n) {
			return n.Data;
		}).firstOrDefault(function(n1) {
			return ss.referenceEquals(n1.bindId, bindid);
		}, ss.getDefaultValue($JSLib_Data_BindDataItem));
		if (ss.isNullOrUndefined(bindData)) {
			return null;
		}
		var result = bindData.data;
		return result;
	};
	global.UIDataEx = $UIDataEx;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.CommonService
	var $JSLib_CommonService = function() {
	};
	$JSLib_CommonService.__typeName = 'JSLib.CommonService';
	$JSLib_CommonService.ScanBarcode = function(showloading, defaultValue) {
		var tsk = new ss.TaskCompletionSource();
		if (showloading) {
			$JSLib_Mobile_Application.ShowLoading();
		}
		if ($JSLib_Mobile_Application.IsBrowser) {
			tsk.setResult({ IsScucess: true, Data: { text: defaultValue } });
		}
		else {
			try {
				cordova.exec(function(o) {
					if (showloading) {
						$JSLib_Mobile_Application.HideLoading();
					}
					var result = {};
					result.IsScucess = true;
					result.Data = o;
					tsk.setResult(result);
				}, function(o1) {
					if (showloading) {
						$JSLib_Mobile_Application.HideLoading();
					}
					var result1 = {};
					result1.IsScucess = false;
					result1.ErrorMessage = o1;
					tsk.setResult(result1);
				}, 'BarcodeScanner', 'scan', []);
			}
			catch ($t1) {
				var ex = ss.Exception.wrap($t1);
				if (showloading) {
					$JSLib_Mobile_Application.HideLoading();
				}
				tsk.setException(ex);
			}
		}
		return tsk.task;
	};
	$JSLib_CommonService.TakePicture = function(option, showloading) {
		if (showloading) {
			$JSLib_Mobile_Application.ShowLoading();
		}
		var tsk = new ss.TaskCompletionSource();
		try {
			navigator.camera.getPicture(function(result) {
				if (showloading) {
					$JSLib_Mobile_Application.HideLoading();
				}
				var $t1 = new $JSLib_TakePictureResult();
				$t1.base64Data = result;
				var takePictureResult = $t1;
				if (option.destinationType === 1 || option.destinationType === 2) {
					takePictureResult.fileName = result;
				}
				else if (option.destinationType === 0) {
					takePictureResult.base64Data = result;
				}
				tsk.setResult({ IsScucess: true, Data: takePictureResult });
			}, function(message) {
				if (showloading) {
					$JSLib_Mobile_Application.HideLoading();
				}
				tsk.setResult({ IsScucess: false, ErrorMessage: message });
			}, option);
		}
		catch ($t2) {
			var ex = ss.Exception.wrap($t2);
			if (showloading) {
				$JSLib_Mobile_Application.HideLoading();
			}
			tsk.setException(ex);
		}
		return tsk.task;
	};
	$JSLib_CommonService.GenerateNatvieMapAppUrl = function(point, AppName) {
		var addr = '';
		var baiduMapAppProtocolName = '';
		if ($JSLib_Mobile_Application.DeviceType === 1) {
			if ($JSLib_Mobile_Application.DeviceType === 1) {
				baiduMapAppProtocolName = 'baidumap';
			}
			else if ($JSLib_Mobile_Application.DeviceType === 10) {
				baiduMapAppProtocolName = 'bdapp';
			}
			if (point.Lat <= 0) {
				addr = ss.formatString('{2}://map/geocoder?address={0}&src={1}', encodeURI(point.Address), AppName, baiduMapAppProtocolName);
			}
			else {
				addr = ss.formatString('{5}://map/marker?location={0},{1}&title={2}&content={3}&src={4}', point.Lat, point.Lng, encodeURI(point.Title), encodeURI(point.Address), AppName, baiduMapAppProtocolName);
			}
		}
		else if ($JSLib_Mobile_Application.DeviceType === 10) {
			var protocolName = 'geo';
			if (point.Lat <= 0) {
				addr = ss.formatString('{0}:0,0?q={1}({2})', protocolName, encodeURI(point.Address), encodeURI(point.Title));
			}
			else {
				addr = ss.formatString('{0}:{1},{2}({3})', protocolName, point.Lat, point.Lng, encodeURI(point.Title));
			}
		}
		return addr;
	};
	$JSLib_CommonService.ShowKeyBoard = function() {
		cordova.exec(null, null, 'Keyboard', 'show', []);
	};
	$JSLib_CommonService.CloseKeyBoard = function() {
		cordova.exec(null, null, 'Keyboard', 'close', []);
	};
	$JSLib_CommonService.ShowSplashScreen = function() {
		cordova.exec(null, null, 'SplashScreen', 'show', []);
	};
	$JSLib_CommonService.HideSplashScreen = function() {
		cordova.exec(null, null, 'SplashScreen', 'hide', []);
	};
	global.JSLib.CommonService = $JSLib_CommonService;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Device
	var $JSLib_Device = function() {
	};
	$JSLib_Device.__typeName = 'JSLib.Device';
	$JSLib_Device.DialNo = function(phoneNo) {
		//Window.Location.Href = "tel:" + No;
		var anchor = document.createElement('a');
		anchor.setAttribute('href', 'tel:' + phoneNo);
		anchor.setAttribute('target', '_self');
		var dispatch = document.createEvent('HTMLEvents');
		dispatch.initEvent('click', true, true);
		anchor.dispatchEvent(dispatch);
	};
	$JSLib_Device.ChooseDate = function(curDate) {
		var datePicker = document.createElement('input');
		datePicker.type = 'date';
		if (ss.isValue(curDate)) {
			datePicker.value = ss.formatDate(ss.unbox(curDate), 'yyyy-MM-dd');
		}
		var dispatch = document.createEvent('HTMLEvents');
		dispatch.initEvent('click', true, true);
		datePicker.dispatchEvent(dispatch);
		var tsk = new ss.TaskCompletionSource();
		datePicker.onchange = function() {
			tsk.setResult(datePicker.valueAsDate);
		};
		return tsk.task;
	};
	global.JSLib.Device = $JSLib_Device;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.JSLibConfig
	var $JSLib_JSLibConfig = function() {
	};
	$JSLib_JSLibConfig.__typeName = 'JSLib.JSLibConfig';
	global.JSLib.JSLibConfig = $JSLib_JSLibConfig;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.MapPoint
	var $JSLib_MapPoint = function() {
		this.Address = '';
		this.Lng = 0;
		this.Lat = 0;
		this.Title = null;
	};
	$JSLib_MapPoint.__typeName = 'JSLib.MapPoint';
	$JSLib_MapPoint.$ctor1 = function(lng, lat, address, Title) {
		this.Address = '';
		this.Lng = 0;
		this.Lat = 0;
		this.Title = null;
		this.Lng = lng;
		this.Lat = lat;
		this.Address = address;
		this.Title = Title;
	};
	global.JSLib.MapPoint = $JSLib_MapPoint;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.SingleTask
	var $JSLib_SingleTask = function() {
	};
	$JSLib_SingleTask.__typeName = 'JSLib.SingleTask';
	$JSLib_SingleTask.Enquee = function(task) {
		if ($JSLib_SingleTask.$IsRunning) {
			return;
		}
		if (ss.contains($JSLib_SingleTask.$TaskList, task)) {
			return;
		}
		$JSLib_SingleTask.$TaskList.push(task);
		$JSLib_SingleTask.$Do();
	};
	$JSLib_SingleTask.$Do = function() {
		var $state = 0, $t1;
		var $sm = function() {
			$sm1:
			for (;;) {
				switch ($state) {
					case 0: {
						$state = -1;
						if (!(!$JSLib_SingleTask.$IsRunning && $JSLib_SingleTask.$TaskList.length > 0)) {
							$state = -1;
							break $sm1;
						}
						$JSLib_SingleTask.$IsRunning = true;
						$t1 = ss.arrayPeekFront($JSLib_SingleTask.$TaskList)();
						$state = 1;
						$t1.continueWith($sm);
						return;
					}
					case 1: {
						$state = -1;
						$t1.getAwaitedResult();
						$JSLib_SingleTask.$TaskList.shift();
						$JSLib_SingleTask.$IsRunning = false;
						$state = 0;
						continue $sm1;
					}
					default: {
						break $sm1;
					}
				}
			}
		};
		$sm();
	};
	global.JSLib.SingleTask = $JSLib_SingleTask;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.TakePictureResult
	var $JSLib_TakePictureResult = function() {
		this.fileName = null;
		this.base64Data = null;
	};
	$JSLib_TakePictureResult.__typeName = 'JSLib.TakePictureResult';
	global.JSLib.TakePictureResult = $JSLib_TakePictureResult;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.TaskEx
	var $JSLib_TaskEx = function() {
	};
	$JSLib_TaskEx.__typeName = 'JSLib.TaskEx';
	$JSLib_TaskEx.CancelSilently = function(task) {
		var tsk = task;
		if (!!ss.isValue(tsk.onCancelSilently)) {
			tsk.onCancelSilently();
		}
	};
	$JSLib_TaskEx.OnCancelSilently = function(task, onCancelSilently) {
		task.onCancelSilently = onCancelSilently;
	};
	global.JSLib.TaskEx = $JSLib_TaskEx;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.TaskQuee
	var $JSLib_TaskQuee = function() {
	};
	$JSLib_TaskQuee.__typeName = 'JSLib.TaskQuee';
	$JSLib_TaskQuee.Enquee = function(task) {
		$JSLib_TaskQuee.$TaskList.push(task);
		$JSLib_TaskQuee.Do();
	};
	$JSLib_TaskQuee.Do = function() {
		var $state = 0, $t1;
		var $sm = function() {
			$sm1:
			for (;;) {
				switch ($state) {
					case 0: {
						$state = -1;
						if (!(!$JSLib_TaskQuee.$IsRunning && $JSLib_TaskQuee.$TaskList.length > 0)) {
							$state = -1;
							break $sm1;
						}
						$JSLib_TaskQuee.$IsRunning = true;
						$t1 = $JSLib_TaskQuee.$TaskList.shift()();
						$state = 1;
						$t1.continueWith($sm);
						return;
					}
					case 1: {
						$state = -1;
						$t1.getAwaitedResult();
						$JSLib_TaskQuee.$IsRunning = false;
						$state = 0;
						continue $sm1;
					}
					default: {
						break $sm1;
					}
				}
			}
		};
		$sm();
	};
	global.JSLib.TaskQuee = $JSLib_TaskQuee;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.WebData
	var $JSLib_WebData = function() {
	};
	$JSLib_WebData.__typeName = 'JSLib.WebData';
	$JSLib_WebData.SetupDataUrl = function(baseUrl) {
		$JSLib_WebData.dataBaseUrl = baseUrl;
	};
	$JSLib_WebData.PostData = function(data, successCallaback, showLoading, url, isGet, OnComplete, showConnectionError, errorCallBack, requestContentType) {
		var $t1 = url;
		if (ss.isNullOrUndefined($t1)) {
			$t1 = $JSLib_WebData.dataBaseUrl;
		}
		var postUrl = $t1;
		//if (data != null)
		//{
		//    postUrl = postUrl + "?" + data;
		//}
		var option = {
			url: postUrl,
			data: data,
			async: true,
			type: (isGet ? 'get' : 'post'),
			timeout: $JSLib_Mobile_Application.WebRequestTimeOut,
			contentType: requestContentType,
			beforeSend: function(request1) {
			}
		};
		if (showLoading) {
			$JSLib_Mobile_Application.ShowLoading();
		}
		option.success = function(responsedata, textStatus, jqueryrequest) {
			if (showLoading) {
				$JSLib_Mobile_Application.HideLoading();
			}
			if (!ss.staticEquals(successCallaback, null)) {
				successCallaback(responsedata);
			}
		};
		option.error = function(jqueryrequest1, textStatus1, error) {
			if (showLoading) {
				$JSLib_Mobile_Application.HideLoading();
			}
			if (showConnectionError) {
				$JSLib_Mobile_MessageBox.Alert((ss.isValue(error) ? '网络连接出现错误！' : '网络不给力！'), '警告');
			}
			if (!ss.staticEquals(errorCallBack, null)) {
				errorCallBack();
			}
		};
		if (!ss.staticEquals(OnComplete, null)) {
			option.complete = function(request, status) {
				OnComplete();
			};
		}
		$.ajax(option);
	};
	$JSLib_WebData.PostDataTask = function(T) {
		return function(data, showLoading, url, isGet, useCache, requestContentType, errorCallBack, page) {
			return $JSLib_WebData.PostDataTaskCompletionSource(T).call(null, data, showLoading, url, isGet, useCache, requestContentType, errorCallBack, page).task;
		};
	};
	$JSLib_WebData.PostDataTaskCompletionSource = function(T) {
		return function(data, showLoading, url, isGet, useCache, requestContentType, errorCallBack, page) {
			var task = new ss.TaskCompletionSource();
			var $t1 = url;
			if (ss.isNullOrUndefined($t1)) {
				$t1 = $JSLib_WebData.dataBaseUrl;
			}
			var option = {
				url: $t1,
				async: true,
				data: data,
				type: (isGet ? 'get' : 'post'),
				timeout: $JSLib_Mobile_Application.WebRequestTimeOut,
				contentType: requestContentType,
				cache: useCache,
				beforeSend: function(request1) {
				}
			};
			window.setTimeout(function() {
				var $state = 0, loadingDialog, $t2, request;
				var $sm = function() {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								loadingDialog = null;
								if (showLoading && ss.isValue(page)) {
									$t2 = page.ShowLoading(0);
									$state = 2;
									$t2.continueWith($sm);
									return;
								}
								$state = 1;
								continue $sm1;
							}
							case 2: {
								$state = -1;
								loadingDialog = $t2.getAwaitedResult();
								$state = 1;
								continue $sm1;
							}
							case 1: {
								$state = -1;
								option.success = function(responsedata, textStatus, jqueryrequest) {
									if (showLoading && ss.isValue(loadingDialog)) {
										if ($JSLib_Mobile_Application.EnableSystemLog) {
											console.log('移除Loading!');
										}
										loadingDialog.Destroy();
									}
									try {
										var result = $ObjectEx.ParseJsonTo(T).call(null, jqueryrequest.responseText);
										task.trySetResult(result);
									}
									catch ($t3) {
										var ex = ss.Exception.wrap($t3);
										task.setException(ex);
									}
								};
								option.error = function(jqueryrequest1, textStatus1, error) {
									if (showLoading && ss.isValue(loadingDialog)) {
										if ($JSLib_Mobile_Application.EnableSystemLog) {
											console.log('移除Loading!');
										}
										loadingDialog.Destroy();
									}
									if (jqueryrequest1.statusText == 'abort') {
										return;
									}
									var result1 = ss.getDefaultValue(T);
									if (!ss.staticEquals(errorCallBack, null)) {
										result1 = errorCallBack(jqueryrequest1, textStatus1, error);
									}
									task.setResult(result1);
									//MessageBox.Alert(!string.IsNullOrEmpty(error.As<string>()) ? "服务端出现异常【" + error
									//    + "】！" : "网络不给力或无法连接服务端！");
								};
								request = $.ajax(option);
								$JSLib_TaskEx.OnCancelSilently(task.task, function() {
									request.abort();
								});
								$state = -1;
								break $sm1;
							}
							default: {
								break $sm1;
							}
						}
					}
				};
				$sm();
			}, 0);
			return task;
		};
	};
	$JSLib_WebData.PostDataSync = function(T) {
		return function(data, url, isGet, reuqestContentType) {
			var $t1 = url;
			if (ss.isNullOrUndefined($t1)) {
				$t1 = $JSLib_WebData.dataBaseUrl;
			}
			var option = {
				url: $t1,
				async: false,
				data: data,
				type: (isGet ? 'get' : 'post'),
				timeout: $JSLib_Mobile_Application.WebRequestTimeOut,
				contentType: reuqestContentType,
				beforeSend: function(request1) {
				}
			};
			option.error = function(jqueryrequest, textStatus, error) {
				$JSLib_Mobile_MessageBox.Alert((ss.isValue(error) ? '网络连接出现错误！' : '网络不给力！'), '警告');
			};
			var request = $.ajax(option);
			return $.parseJSON(request.responseText);
		};
	};
	$JSLib_WebData.GetScript = function(url, callBackAction) {
		var script = document.createElement('script');
		script.async = false;
		script.src = url;
		if (!ss.staticEquals(callBackAction, null)) {
			script.onload = callBackAction;
		}
		document.getElementsByTagName('head')[0].appendChild(script);
	};
	$JSLib_WebData.GetScriptTask = function(url) {
		var tsk = new ss.TaskCompletionSource();
		$JSLib_WebData.GetScript(url, function(event) {
			tsk.setResult(true);
		});
		return tsk.task;
	};
	global.JSLib.WebData = $JSLib_WebData;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Util.IocContainer.TypeRegistration
	var $JSLib_$Util_IocContainer$TypeRegistration = function(implementation) {
		$JSLib_$Util_IocContainer$TypeRegistration.$ctor1.call(this, ss.getInstanceType(implementation), implementation);
	};
	$JSLib_$Util_IocContainer$TypeRegistration.__typeName = 'JSLib.$Util.IocContainer$TypeRegistration';
	$JSLib_$Util_IocContainer$TypeRegistration.$ctor2 = function(type, isSingleton, constructor) {
		this.$isSingleton = true;
		this.$implementationType = null;
		this.$implementationInstance = null;
		this.$constructor = null;
		this.$implementationType = type;
		this.$implementationInstance = null;
		this.$isSingleton = isSingleton;
		this.$constructor = constructor;
	};
	$JSLib_$Util_IocContainer$TypeRegistration.$ctor1 = function(type, implementation) {
		this.$isSingleton = true;
		this.$implementationType = null;
		this.$implementationInstance = null;
		this.$constructor = null;
		this.$implementationType = type;
		this.$implementationInstance = implementation;
		this.$isSingleton = true;
		this.$constructor = null;
	};
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Controller.ControllerBase
	var $JSLib_Controller_ControllerBase = function() {
		this.$2$PageContainerField = null;
		this.Content = null;
		this.$2$OnLoadField = null;
		this.$2$OnShowField = null;
		this.HtmlContent = null;
		$JSLib_Mobile_UIBase.call(this);
		this.add_OnLoad(function(sender, args) {
		});
	};
	$JSLib_Controller_ControllerBase.__typeName = 'JSLib.Controller.ControllerBase';
	global.JSLib.Controller.ControllerBase = $JSLib_Controller_ControllerBase;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Controller.Panel
	var $JSLib_Controller_Panel = function(content) {
		this.Child = [];
		$JSLib_Controller_UIController.call(this);
		this.Content = $(content);
	};
	$JSLib_Controller_Panel.__typeName = 'JSLib.Controller.Panel';
	$JSLib_Controller_Panel.FromElement = function(divElement) {
		return new $JSLib_Controller_Panel(divElement);
	};
	global.JSLib.Controller.Panel = $JSLib_Controller_Panel;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Controller.RazorTemplate
	var $JSLib_Controller_RazorTemplate = function() {
		this.contentBuilder = new ss.StringBuilder();
	};
	$JSLib_Controller_RazorTemplate.__typeName = 'JSLib.Controller.RazorTemplate';
	global.JSLib.Controller.RazorTemplate = $JSLib_Controller_RazorTemplate;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Controller.UIController
	var $JSLib_Controller_UIController = function() {
		this.$3$WidthField = null;
		this.$3$HeightField = null;
		$JSLib_Controller_ControllerBase.call(this);
	};
	$JSLib_Controller_UIController.__typeName = 'JSLib.Controller.UIController';
	global.JSLib.Controller.UIController = $JSLib_Controller_UIController;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Data.BindData
	var $JSLib_Data_BindData = function() {
		this.Data = [];
		this.ContainerId = null;
	};
	$JSLib_Data_BindData.__typeName = 'JSLib.Data.BindData';
	global.JSLib.Data.BindData = $JSLib_Data_BindData;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Data.BindDataItem
	var $JSLib_Data_BindDataItem = function() {
		this.bindId = null;
		this.data = null;
	};
	$JSLib_Data_BindDataItem.__typeName = 'JSLib.Data.BindDataItem';
	global.JSLib.Data.BindDataItem = $JSLib_Data_BindDataItem;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Data.DataBindHelper
	var $JSLib_Data_DataBindHelper = function() {
	};
	$JSLib_Data_DataBindHelper.__typeName = 'JSLib.Data.DataBindHelper';
	$JSLib_Data_DataBindHelper.Bind = function(target, containerId) {
		var bindId = ss.Guid.newGuid().toString();
		var bindData = Enumerable.from($JSLib_Data_DataBindHelper.bindDatas).firstOrDefault(function(n) {
			return ss.referenceEquals(n.ContainerId, containerId);
		}, ss.getDefaultValue($JSLib_Data_BindData));
		if (ss.isNullOrUndefined(bindData)) {
			var $t1 = new $JSLib_Data_BindData();
			$t1.ContainerId = containerId;
			bindData = $t1;
			$JSLib_Data_DataBindHelper.bindDatas.push(bindData);
		}
		var $t3 = bindData.Data;
		var $t2 = new $JSLib_Data_BindDataItem();
		$t2.bindId = bindId;
		$t2.data = target;
		$t3.push($t2);
		if (ss.isValue(target)) {
			return ss.formatString("data-c-bindid='{0}' data-c-type='{1}'", bindId, ss.getTypeFullName(ss.getInstanceType(target)));
		}
		else {
			console.info('绑定值为空');
			return '';
		}
	};
	$JSLib_Data_DataBindHelper.RemoveBindData = function(containerId) {
		var bindData = Enumerable.from($JSLib_Data_DataBindHelper.bindDatas).firstOrDefault(function(n) {
			return ss.referenceEquals(n.ContainerId, containerId);
		}, ss.getDefaultValue($JSLib_Data_BindData));
		if (ss.isValue(bindData)) {
			ss.remove($JSLib_Data_DataBindHelper.bindDatas, bindData);
		}
	};
	global.JSLib.Data.DataBindHelper = $JSLib_Data_DataBindHelper;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Data.DirectionDictionary
	var $JSLib_Data_DirectionDictionary$1 = function(TValue) {
		var $type = function(maxCount) {
			ss.makeGenericType($JSLib_Widget_ViewPager_FixedLengthDictionary$2, [ss.Int32, TValue]).call(this, maxCount);
		};
		ss.registerGenericClassInstance($type, $JSLib_Data_DirectionDictionary$1, [TValue], {
			AddData: function(value) {
				//this.AddItem
			}
		}, function() {
			return ss.makeGenericType($JSLib_Widget_ViewPager_FixedLengthDictionary$2, [ss.Int32, TValue]);
		}, function() {
			return [ss.IEnumerable, ss.IEnumerable, ss.IDictionary];
		});
		return $type;
	};
	$JSLib_Data_DirectionDictionary$1.__typeName = 'JSLib.Data.DirectionDictionary$1';
	ss.initGenericClass($JSLib_Data_DirectionDictionary$1, $asm, 1);
	global.JSLib.Data.DirectionDictionary$1 = $JSLib_Data_DirectionDictionary$1;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Data.GestureDirection
	var $JSLib_Data_GestureDirection = function() {
	};
	$JSLib_Data_GestureDirection.__typeName = 'JSLib.Data.GestureDirection';
	global.JSLib.Data.GestureDirection = $JSLib_Data_GestureDirection;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.DesktopComponent.DataGrid
	var $JSLib_DesktopComponent_DataGrid = function(option) {
		this.$option = null;
		this.$option = option;
		this.$LoadData(0);
		option.Pager.on('page', ss.mkdel(this, function(event) {
			var pageIndex = option.Pager.find('li.active').data('lp') - 1;
			this.$LoadData(pageIndex);
		}));
		option.PnlTableContent.on('mouseout', 'tr', ss.thisFix(function(elem, event1) {
			$(elem).removeClass('trOver');
		}));
		option.PnlTableContent.on('mouseover', 'tr', ss.thisFix(function(elem1, event2) {
			$(elem1).addClass('trOver');
		}));
		if (ss.isValue(option.BtnCheckAll)) {
			option.BtnCheckAll.click(function(event3) {
				option.PnlTableContent.find('input[type=checkbox]').each(function(index, element) {
					element.checked = true;
				});
			});
			option.PnlTableContent.on('click', 'tr', ss.thisFix(function(elem2, event4) {
				if ($(event4.target).is('input[type=checkbox]')) {
					return;
				}
				$(elem2).find('input[type=checkbox]').click();
			}));
		}
		if (ss.isValue(option.BtnReversAll)) {
			option.BtnReversAll.click(function(event5) {
				option.PnlTableContent.find('input[type=checkbox]').each(function(index1, element1) {
					element1.checked = !element1.checked;
				});
			});
		}
	};
	$JSLib_DesktopComponent_DataGrid.__typeName = 'JSLib.DesktopComponent.DataGrid';
	global.JSLib.DesktopComponent.DataGrid = $JSLib_DesktopComponent_DataGrid;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.DesktopComponent.DataGridOption
	var $JSLib_DesktopComponent_DataGridOption = function() {
		this.Pager = null;
		this.PageSize = 0;
		this.PnlTableContent = null;
		this.lblSummary = null;
		this.LoadGridData = null;
		this.BtnCheckAll = null;
		this.BtnReversAll = null;
	};
	$JSLib_DesktopComponent_DataGridOption.__typeName = 'JSLib.DesktopComponent.DataGridOption';
	global.JSLib.DesktopComponent.DataGridOption = $JSLib_DesktopComponent_DataGridOption;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.DesktopComponent.LoadGridDataResult
	var $JSLib_DesktopComponent_LoadGridDataResult = function() {
		this.TableBody = null;
		this.TotalRecords = 0;
	};
	$JSLib_DesktopComponent_LoadGridDataResult.__typeName = 'JSLib.DesktopComponent.LoadGridDataResult';
	global.JSLib.DesktopComponent.LoadGridDataResult = $JSLib_DesktopComponent_LoadGridDataResult;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.BBQ
	var $JSLib_Extension_BBQ = function() {
	};
	$JSLib_Extension_BBQ.__typeName = 'JSLib.Extension.BBQ';
	global.JSLib.Extension.BBQ = $JSLib_Extension_BBQ;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.ImageExtension
	var $JSLib_Extension_ImageExtension = function() {
	};
	$JSLib_Extension_ImageExtension.__typeName = 'JSLib.Extension.ImageExtension';
	$JSLib_Extension_ImageExtension.GetImageUrlBase64Data = function(imageUrl, onComplete) {
		//var image = Document.CreateElement("img").As<ImageElement>();
		//image.AddEventListener("load", delegate(Event @event1)
		//    {
		//        var mycanvasElement = (CanvasElement)Document.CreateElement("canvas");
		//        mycanvasElement.Width = image.Width;
		//        mycanvasElement.Height = image.Height;
		//        ((CanvasRenderingContext2D)mycanvasElement.GetContext("2d")).DrawImage(image, 0, 0, image.Width, image.Height);
		//        var resultUrl = (string)mycanvasElement.GetDataUrl("image/png");
		//        image = null;
		//        onComplete(resultUrl);
		//    }, false);
		//image.Src = imageUrl;
	};
	global.JSLib.Extension.ImageExtension = $JSLib_Extension_ImageExtension;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.MergeMode
	var $JSLib_Extension_MergeMode = function() {
	};
	$JSLib_Extension_MergeMode.__typeName = 'JSLib.Extension.MergeMode';
	global.JSLib.Extension.MergeMode = $JSLib_Extension_MergeMode;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.StyleSheetEx
	var $JSLib_Extension_StyleSheetEx = function() {
	};
	$JSLib_Extension_StyleSheetEx.__typeName = 'JSLib.Extension.StyleSheetEx';
	$JSLib_Extension_StyleSheetEx.AddCssRule = function(cssRule) {
		var cssElement = $(ss.formatString('<style>{0}</style>', cssRule));
		$(document.body).append(cssElement);
	};
	global.JSLib.Extension.StyleSheetEx = $JSLib_Extension_StyleSheetEx;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.TouchDirection
	var $JSLib_Extension_TouchDirection = function() {
	};
	$JSLib_Extension_TouchDirection.__typeName = 'JSLib.Extension.TouchDirection';
	global.JSLib.Extension.TouchDirection = $JSLib_Extension_TouchDirection;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.TouchHandler
	var $JSLib_Extension_TouchHandler = function(mouseContainer) {
		this.$1$OnFlingField = null;
		this.$1$OnCompleteField = null;
		this.$mouseContainer = null;
		this.$_currentX = 0;
		this.$_currentY = 0;
		this.$_preX = 0;
		this.$_preY = 0;
		this.$_totalDeltaX = 0;
		this.$_totalDeltaY = 0;
		this.$canRecord = false;
		this.$timeIDList = [];
		this.$waitTime = 100;
		this.$mouseContainer = mouseContainer;
	};
	$JSLib_Extension_TouchHandler.__typeName = 'JSLib.Extension.TouchHandler';
	global.JSLib.Extension.TouchHandler = $JSLib_Extension_TouchHandler;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.TouchHandlerArgs
	var $JSLib_Extension_TouchHandlerArgs = function() {
		this.currentX = 0;
		this.currentY = 0;
		this.deltaX = 0;
		this.deltaY = 0;
		this.TotalDeltaX = 0;
		this.TotalDeltaY = 0;
		this.IsComplete = false;
		ss.EventArgs.call(this);
	};
	$JSLib_Extension_TouchHandlerArgs.__typeName = 'JSLib.Extension.TouchHandlerArgs';
	global.JSLib.Extension.TouchHandlerArgs = $JSLib_Extension_TouchHandlerArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.TransFormEx
	var $JSLib_Extension_TransFormEx = function() {
	};
	$JSLib_Extension_TransFormEx.__typeName = 'JSLib.Extension.TransFormEx';
	$JSLib_Extension_TransFormEx.Transform = function(target, deltaX, deltaY) {
		var propertiesToAnimate = ss.mkdict(['width', ss.formatString('+={0},+={1}', deltaX, deltaY)]);
		return target.animate(propertiesToAnimate, 0);
	};
	$JSLib_Extension_TransFormEx.TransformX = function(target, xValue) {
		var propertiesToAnimate = ss.mkdict(['left', xValue]);
		return target.animate(propertiesToAnimate, 0);
	};
	$JSLib_Extension_TransFormEx.TransformY = function(target, yValue) {
		var propertiesToAnimate = ss.mkdict(['top', yValue]);
		return target.animate(propertiesToAnimate, 0);
	};
	$JSLib_Extension_TransFormEx.TransformTo = function(target, xValue, yValue) {
		var propertiesToAnimate = ss.mkdict(['left', xValue, 'top', yValue]);
		return target.animate(propertiesToAnimate, 0);
	};
	$JSLib_Extension_TransFormEx.ResetTransform = function(target) {
		var propertiesToAnimate = ss.mkdict(['left', '0', 'top', '0']);
		return target.animate(propertiesToAnimate, 0);
	};
	$JSLib_Extension_TransFormEx.Scale = function(target, deltaValue) {
		var propertiesToAnimate = ss.mkdict(['width', ss.formatString('+={0}', deltaValue), 'height', ss.formatString('+={0}', deltaValue)]);
		return target.animate(propertiesToAnimate);
	};
	$JSLib_Extension_TransFormEx.ScaleTo = function(target, tagetValue) {
		var propertiesToAnimate = ss.mkdict(['width', ss.formatString('{0}', tagetValue), 'height', ss.formatString('{0}', tagetValue)]);
		return target.animate(propertiesToAnimate);
	};
	$JSLib_Extension_TransFormEx.ScaleWidhtTo = function(target, tagetValue) {
		var propertiesToAnimate = ss.mkdict(['width', ss.formatString('{0}', tagetValue)]);
		return target.animate(propertiesToAnimate);
	};
	$JSLib_Extension_TransFormEx.ResetScale = function(target) {
		target[0].style.width = 'initial';
		target[0].style.height = 'initial';
		return target;
	};
	global.JSLib.Extension.TransFormEx = $JSLib_Extension_TransFormEx;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Extension.md5.MD5
	var $JSLib_Extension_md5_MD5 = function() {
	};
	$JSLib_Extension_md5_MD5.__typeName = 'JSLib.Extension.md5.MD5';
	$JSLib_Extension_md5_MD5.GetMd5 = function(source) {
		return hex_md5(source).toUpperCase();
	};
	global.JSLib.Extension.md5.MD5 = $JSLib_Extension_md5_MD5;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Helper.CookieHelper
	var $JSLib_Helper_CookieHelper = function() {
	};
	$JSLib_Helper_CookieHelper.__typeName = 'JSLib.Helper.CookieHelper';
	$JSLib_Helper_CookieHelper.createCookie = function(name, value, days) {
		var expires = '';
		if (days > 0) {
			var date = new Date((new Date()).valueOf() + Math.round(days * 86400000));
			expires = '; expires=' + date.toGMTString();
		}
		else {
			expires = '';
		}
		document.cookie = name + '=' + value + expires + '; path=/';
	};
	$JSLib_Helper_CookieHelper.readCookie = function(name) {
		var nameEQ = name + '=';
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charCodeAt(0) === 32) {
				c = c.substr(1, c.length);
			}
			if (c.indexOf(nameEQ) === 0) {
				return c.substr(nameEQ.length, c.length);
			}
		}
		return null;
	};
	$JSLib_Helper_CookieHelper.eraseCookie = function(name) {
		$JSLib_Helper_CookieHelper.createCookie(name, '', -1);
	};
	global.JSLib.Helper.CookieHelper = $JSLib_Helper_CookieHelper;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.Carousel
	var $JSLib_IScrollWraper_Carousel = function(dataList, elementWidth, useIndicator, autoSwipe, carouselWraperClass) {
		this.$1$OnItemClickField = null;
		this._iscroll = null;
		this.$isStart = false;
		this.$titleObject = null;
		this.$targetUl = null;
		this.$autoSwipeIntervalID = 0;
		this.$currentPageIndex = 0;
		this.$pageCount = 0;
		this.$childElementCount = 0;
		this.$targetUl = $(dataList);
		this.$childElementCount = this.$targetUl.children().length;
		var wrapper = this.$targetUl.wrapAll(ss.formatString("<div class='c-carousel-warper {0}'/>", carouselWraperClass)).wrapAll("<div class='c-carousel'/>").parent().parent();
		var SetWidth = ss.mkdel(this, function() {
			var $state = 0, maxCount, $t1;
			var $sm = ss.mkdel(this, function() {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							maxCount = 10;
							$state = 1;
							continue $sm1;
						}
						case 1: {
							$state = -1;
							if (!(elementWidth === 0 && maxCount >= 0)) {
								$state = 2;
								continue $sm1;
							}
							$t1 = ss.Task.delay(100);
							$state = 3;
							$t1.continueWith($sm);
							return;
						}
						case 3: {
							$state = -1;
							$t1.getAwaitedResult();
							elementWidth = dataList.getBoundingClientRect().width;
							if (elementWidth > 0) {
								$state = 2;
								continue $sm1;
							}
							maxCount--;
							$state = 1;
							continue $sm1;
						}
						case 2: {
							$state = -1;
							wrapper.width(elementWidth);
							this.$targetUl.parent().width(elementWidth * this.$childElementCount);
							this.$targetUl.children().each(function(index, element) {
								$(element).width(elementWidth);
								//.Height(elementHeight);
							});
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			});
			$sm();
		});
		SetWidth();
		this.$targetUl.addClass('c-carousel-dataList');
		this.$pageCount = this.$targetUl.children().length;
		var indicator = null;
		var carouselIndicatorList = null;
		if (useIndicator) {
			var sb = new ss.StringBuilder();
			sb.append("<div class='c-carousel-footer'> ");
			//添加标题
			sb.append("<span class='c-carousel-title'></span>");
			//添加指示器
			sb.append("<ul class='c-carousel-indicator' >");
			for (var i = 0; i < this.$childElementCount; i++) {
				sb.append(ss.formatString('<li > </li>'));
			}
			sb.append('</ul>');
			sb.append('</div>');
			indicator = $(sb.toString());
			carouselIndicatorList = indicator.find('ul');
			carouselIndicatorList.children().first().addClass('active');
			this.$titleObject = indicator.find('span');
			wrapper.append(indicator);
			var fisrtchild = this.$targetUl.children()[0];
			if (ss.isValue(fisrtchild)) {
				var title = fisrtchild.getAttribute('data-title');
				this.$titleObject.text(title);
			}
		}
		var iscrollOption = { snap: true, momentum: false, hScrollbar: false, vScrollbar: false, bounceLock: true, hScroll: true };
		iscrollOption.onScrollEnd = ss.mkdel(this, function(event) {
			if (ss.isValue(indicator)) {
				this.$onScrollEnd(carouselIndicatorList);
			}
		});
		iscrollOption.onScrollStart = function(event1) {
			if (ss.isValue(event1)) {
				event1.stopPropagation();
				event1.preventDefault();
			}
		};
		this._iscroll = new iScroll(wrapper[0], iscrollOption);
		window.setTimeout(ss.mkdel(this, function() {
			if (ss.isValue(this._iscroll)) {
				this._iscroll.refresh();
			}
		}), 500);
		if (autoSwipe && this.$childElementCount > 1) {
			this.$currentPageIndex = 0;
			this.$autoSwipeIntervalID = window.setInterval(ss.mkdel(this, function() {
				if (this._iscroll.scrollerW > 0) {
					this.$currentPageIndex = this._iscroll.currPageX + 1;
					if (this.$currentPageIndex === this.$pageCount) {
						this.$currentPageIndex = 0;
					}
					this._iscroll.scrollToPage(this.$currentPageIndex, 0, 500);
					this.$onScrollEnd(carouselIndicatorList);
				}
			}), 3000);
		}
	};
	$JSLib_IScrollWraper_Carousel.__typeName = 'JSLib.IScrollWraper.Carousel';
	global.JSLib.IScrollWraper.Carousel = $JSLib_IScrollWraper_Carousel;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.CarouselItemArgs
	var $JSLib_IScrollWraper_CarouselItemArgs = function() {
		this.data = null;
		this.target = null;
		ss.EventArgs.call(this);
	};
	$JSLib_IScrollWraper_CarouselItemArgs.__typeName = 'JSLib.IScrollWraper.CarouselItemArgs';
	global.JSLib.IScrollWraper.CarouselItemArgs = $JSLib_IScrollWraper_CarouselItemArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.IScrollContainer
	var $JSLib_IScrollWraper_IScrollContainer = function(ele, option, containerClassName) {
		this.iscroll = null;
		this.$pullDownEl = null;
		this.$pullDownLabel = null;
		this.$isPullDownBegin = false;
		this.$refreshAction = null;
		this.$enablePullDownToRefresh = false;
		this.$onScrollMove = null;
		this.$onScrollEnd = null;
		this.$BuildIscroller(ele, option.scrollbars, option.vScroll, option.hScroll, option.onScrollStart, option.onScrollMove, option.onScrollEnd, containerClassName, false, null, true, true);
	};
	$JSLib_IScrollWraper_IScrollContainer.__typeName = 'JSLib.IScrollWraper.IScrollContainer';
	$JSLib_IScrollWraper_IScrollContainer.$ctor1 = function(ele, showScrollBar, vScroll, hScroll, onScrollStart, onScrollMove, onScrollEnd, containerClassName, enablePullDownToRefresh, refreshAction, enableBounce, enableMomentum) {
		this.iscroll = null;
		this.$pullDownEl = null;
		this.$pullDownLabel = null;
		this.$isPullDownBegin = false;
		this.$refreshAction = null;
		this.$enablePullDownToRefresh = false;
		this.$onScrollMove = null;
		this.$onScrollEnd = null;
		this.$BuildIscroller(ele, showScrollBar, vScroll, hScroll, function(event) {
			if (!ss.staticEquals(onScrollStart, null)) {
				onScrollStart();
			}
		}, function(event1) {
			if (!ss.staticEquals(onScrollMove, null)) {
				onScrollMove();
			}
		}, function(event2) {
			if (!ss.staticEquals(onScrollEnd, null)) {
				onScrollEnd();
			}
		}, containerClassName, enablePullDownToRefresh, refreshAction, enableBounce, enableMomentum);
	};
	$JSLib_IScrollWraper_IScrollContainer.HandleBeforeScrollStart = function(event) {
		var target = event.target;
		while (target.nodeType !== 1) {
			target = target.parentNode;
		}
		if (target.tagName !== 'SELECT' && target.tagName !== 'INPUT' && target.tagName !== 'TEXTAREA') {
			event.preventDefault();
		}
	};
	global.JSLib.IScrollWraper.IScrollContainer = $JSLib_IScrollWraper_IScrollContainer;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.IScrollList
	var $JSLib_IScrollWraper_IScrollList = function(target, pageSize, limitPageCount, enableRefreshData, autoRemoveDom) {
		this.iscroll = null;
		this.$wraper = null;
		this.$targetList = null;
		this.FromPageIndex = $JSLib_Mobile_Application.get_PageIndexStart();
		this.ToPageIndex = $JSLib_Mobile_Application.get_PageIndexStart();
		this.$MaxPageIndex = 2147483647;
		this.$PageSize = 20;
		this.LimitPageCount = 3;
		this.OnScroll = null;
		this.$pullDownLabel = null;
		this.$pullUpLabel = null;
		this.$EnableRefreshData = false;
		this.$isWarped = false;
		this.$pullDownEl = null;
		this.$pullUpEl = null;
		this.$isPullUpBegin = false;
		this.$isPullDownBegin = false;
		this.$IsLoadingData = false;
		this.$autoRemoveDom = false;
		this.$1$GetPageDataField = null;
		this.currentPageIndex = $JSLib_Mobile_Application.get_PageIndexStart();
		this.$autoRemoveDom = autoRemoveDom;
		this.$targetList = $(target);
		this.$PageSize = pageSize;
		this.LimitPageCount = limitPageCount;
		this.$EnableRefreshData = enableRefreshData;
	};
	$JSLib_IScrollWraper_IScrollList.__typeName = 'JSLib.IScrollWraper.IScrollList';
	global.JSLib.IScrollWraper.IScrollList = $JSLib_IScrollWraper_IScrollList;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.IScrollList.PullDirection
	var $JSLib_IScrollWraper_IScrollList$PullDirection = function() {
	};
	$JSLib_IScrollWraper_IScrollList$PullDirection.__typeName = 'JSLib.IScrollWraper.IScrollList$PullDirection';
	global.JSLib.IScrollWraper.IScrollList$PullDirection = $JSLib_IScrollWraper_IScrollList$PullDirection;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.NaivgateBar
	var $JSLib_IScrollWraper_NaivgateBar = function(parent, navigateOption) {
		this.$menuScroll = null;
		this.$datalist = null;
		this.$1$OnItemClickField = null;
		var wrap = $("<div class='menu'><ul></ul></div>");
		this.$datalist = wrap.children('ul');
		var sb = new ss.StringBuilder();
		for (var $t1 = 0; $t1 < navigateOption.length; $t1++) {
			var n = navigateOption[$t1];
			sb.append(ss.formatString("<li data-sid='{0}' data-title='{1}' class='{3}'><i style='background-image: url({1})'></i><span>{2}</span></li>", n.Data, n.IconImageUrl, n.Title, (n.IsActivated ? 'active' : '')));
		}
		this.$datalist.append(sb.toString());
		this.$datalist.on('click', 'li', ss.thisFix(ss.mkdel(this, function(elem, event) {
			this.$datalist.find('li.active').removeClass('active');
			$(elem).addClass('active');
			if (!ss.staticEquals(this.$1$OnItemClickField, null)) {
				var $t3 = this.$1$OnItemClickField;
				var $t2 = new $JSLib_IScrollWraper_NavigateClickArgs();
				$t2.Data = elem.getAttribute('data-sid');
				$t2.Title = elem.getAttribute('data-title');
				$t3(this, $t2);
			}
		})));
		$(parent).append(wrap);
		var totalWidth = 0;
		this.$datalist.children().each(function(index, element) {
			totalWidth += $(element).outerWidth();
		});
		this.$datalist.width(totalWidth);
		this.$menuScroll = new iScroll(wrap[0], { hScroll: true, lockDirection: true, hScrollbar: false, vScrollbar: false, checkDOMChanges: true });
		this.$menuScroll.refresh();
		//datalist.Find("li>span").Each(delegate(int index, Element element)
		//       {
		//           element.Style.MarginLeft = (-(element.ToJQueryObject().GetOuterWidth() / 2)).ToString() + "px";
		//       });
		//Window.SetTimeout(adjustText, 1000);
		//Window.SetTimeout(adjustText, 2000);
	};
	$JSLib_IScrollWraper_NaivgateBar.__typeName = 'JSLib.IScrollWraper.NaivgateBar';
	global.JSLib.IScrollWraper.NaivgateBar = $JSLib_IScrollWraper_NaivgateBar;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.NavigateBarOption
	var $JSLib_IScrollWraper_NavigateBarOption = function() {
		this.Data = null;
		this.Title = null;
		this.IconImageUrl = null;
		this.IsActivated = false;
	};
	$JSLib_IScrollWraper_NavigateBarOption.__typeName = 'JSLib.IScrollWraper.NavigateBarOption';
	global.JSLib.IScrollWraper.NavigateBarOption = $JSLib_IScrollWraper_NavigateBarOption;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.NavigateClickArgs
	var $JSLib_IScrollWraper_NavigateClickArgs = function() {
		this.Data = null;
		this.Title = null;
		ss.EventArgs.call(this);
	};
	$JSLib_IScrollWraper_NavigateClickArgs.__typeName = 'JSLib.IScrollWraper.NavigateClickArgs';
	global.JSLib.IScrollWraper.NavigateClickArgs = $JSLib_IScrollWraper_NavigateClickArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.IScrollWraper.PageData
	var $JSLib_IScrollWraper_PageData = function() {
		this.RecordCount = 0;
		this.ResultElement = null;
		this.PageIndex = $JSLib_Mobile_Application.get_PageIndexStart();
	};
	$JSLib_IScrollWraper_PageData.__typeName = 'JSLib.IScrollWraper.PageData';
	global.JSLib.IScrollWraper.PageData = $JSLib_IScrollWraper_PageData;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Map.BaiduGeocodingService
	var $JSLib_Map_BaiduGeocodingService = function() {
	};
	$JSLib_Map_BaiduGeocodingService.__typeName = 'JSLib.Map.BaiduGeocodingService';
	global.JSLib.Map.BaiduGeocodingService = $JSLib_Map_BaiduGeocodingService;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Map.DbGeographyValue
	var $JSLib_Map_DbGeographyValue = function() {
		this.CoordinateSystemId = 0;
		this.WellKnownText = null;
	};
	$JSLib_Map_DbGeographyValue.__typeName = 'JSLib.Map.DbGeographyValue';
	global.JSLib.Map.DbGeographyValue = $JSLib_Map_DbGeographyValue;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Map.GeographyEx
	var $JSLib_Map_GeographyEx = function() {
	};
	$JSLib_Map_GeographyEx.__typeName = 'JSLib.Map.GeographyEx';
	$JSLib_Map_GeographyEx.ToPoint = function(target) {
		//POINT (104.074895 30.540493)
		var text = target.Geography.WellKnownText;
		return $JSLib_Map_GeographyEx.ToPoint$1(text);
	};
	$JSLib_Map_GeographyEx.ToPoint$1 = function(target) {
		if (ss.isNullOrEmptyString(target)) {
			throw new ss.Exception('不正确的经纬度格式!');
		}
		var result = target.match(new RegExp('(\\d+\\.?\\d*) (\\d+\\.?\\d*)'));
		return new BMap.Point(parseFloat(result[1]), parseFloat(result[2]));
	};
	global.JSLib.Map.GeographyEx = $JSLib_Map_GeographyEx;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Map.GeographyType
	var $JSLib_Map_GeographyType = function() {
		this.Geography = null;
	};
	$JSLib_Map_GeographyType.__typeName = 'JSLib.Map.GeographyType';
	global.JSLib.Map.GeographyType = $JSLib_Map_GeographyType;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.AppGlobalTemplate
	var $JSLib_Mobile_AppGlobalTemplate = function() {
		$JSLib_Controller_RazorTemplate.call(this);
	};
	$JSLib_Mobile_AppGlobalTemplate.__typeName = 'JSLib.Mobile.AppGlobalTemplate';
	global.JSLib.Mobile.AppGlobalTemplate = $JSLib_Mobile_AppGlobalTemplate;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.Application
	var $JSLib_Mobile_Application = function() {
	};
	$JSLib_Mobile_Application.__typeName = 'JSLib.Mobile.Application';
	$JSLib_Mobile_Application.OnDeviceReady = function(action) {
		document.addEventListener('deviceready', function(event1) {
			action();
		});
	};
	$JSLib_Mobile_Application.ListenNetWorkState = function() {
		document.addEventListener('online', function(event) {
			console.info('联机状态');
			$JSLib_Mobile_Application.NetWorkState = 1;
		});
		document.addEventListener('offline', function(event1) {
			console.info('脱机状态');
			$JSLib_Mobile_Application.NetWorkState = 2;
		});
	};
	$JSLib_Mobile_Application.GetDevicePhoneUUID = function() {
		if ($JSLib_Mobile_Application.IsBrowser) {
			if (ss.isNullOrUndefined(window.localStorage['uuid'])) {
				window.localStorage['uuid'] = ss.Guid.newGuid().toString();
			}
			return window.localStorage['uuid'];
		}
		else {
			return device.uuid;
		}
	};
	$JSLib_Mobile_Application.get_PageIndexStart = function() {
		if ($JSLib_Mobile_Application.PageIndexFromZero) {
			return 0;
		}
		else {
			return 1;
		}
	};
	$JSLib_Mobile_Application.PrepareApp = function() {
		if ($JSLib_Mobile_Application.DisableBodyMove) {
			$JSLib_Mobile_MobilePage.PageContainer.on('touchmove', function(event) {
				event.preventDefault();
			});
		}
		if (!$JSLib_Mobile_Application.IsBrowser) {
			console.info('设备信息:' + $JSLib_Mobile_Application.DeviceType);
			$JSLib_Mobile_Application.$LoadDevicePhoneGapJs();
		}
		if (ss.isNullOrUndefined($JSLib_Mobile_MobilePage.PageContainer)) {
			$JSLib_Mobile_MobilePage.PageContainer = $('body');
		}
		//处理andorid上数字输入框不能与placeholder兼容的问题
		$JSLib_Mobile_Application.HandleAndroidNumberTextBox();
	};
	$JSLib_Mobile_Application.$LoadDevicePhoneGapJs = function() {
		if ($JSLib_Mobile_Application.DeviceType === 10) {
			$JSLib_WebData.GetScript($JSLib_Mobile_Application.CordovaJSDir + 'cordova/android/cordova.js', null);
		}
		else if ($JSLib_Mobile_Application.DeviceType === 1) {
			$JSLib_WebData.GetScript($JSLib_Mobile_Application.CordovaJSDir + 'cordova/ios/cordova.js', null);
		}
		else if ($JSLib_Mobile_Application.DeviceType === 0) {
		}
	};
	$JSLib_Mobile_Application.Back = function(backPageCount) {
		var $state = 0, $t1, foo, focusElement, attribute, isTextBox, isTextArea;
		var $sm = function() {
			$sm1:
			for (;;) {
				switch ($state) {
					case 0: {
						$state = -1;
						//如果为下拉列表展开状态，则关闭下拉列表
						//if (SelectMenu.Hide())
						//{
						//    return;
						//}
						//如果为下拉列表展开状态，则关闭下拉列表
						//if (ModalPanel.modalPanles != null && ModalPanel.modalPanles.Count > 0)
						//{
						//    var data = ModalPanel.modalPanles[ModalPanel.modalPanles.Count - 1];
						//    data.Destroy();
						//    return;
						//}
						if (!ss.staticEquals($JSLib_Mobile_Application.BeforeBack, null)) {
							$t1 = $JSLib_Mobile_Application.BeforeBack();
							$state = 2;
							$t1.continueWith($sm);
							return;
						}
						$state = 1;
						continue $sm1;
					}
					case 2: {
						$state = -1;
						foo = $t1.getAwaitedResult();
						if (!foo) {
							return;
						}
						$state = 1;
						continue $sm1;
					}
					case 1: {
						$state = -1;
						//如果文本框处于焦点状态，则取消该元素处于焦点状态
						$JSLib_Mobile_NavigationManager.CurrentBackPageCount = backPageCount;
						if (!$JSLib_Mobile_Application.IsBrowser) {
							focusElement = document.activeElement;
							attribute = focusElement.getAttribute('type');
							isTextBox = ss.isValue(focusElement) && focusElement.tagName.toUpperCase() === 'INPUT' && focusElement.hasAttribute('type') && (attribute === 'text' || attribute === 'password');
							isTextArea = ss.isValue(focusElement) && focusElement.tagName.toUpperCase() === 'TEXTAREA';
							if (isTextBox || isTextArea) {
								focusElement.blur();
							}
							else {
								//Window.History.Back();
								$JSLib_Mobile_NavigationManager.HistoryBack();
							}
						}
						else {
							//Window.History.Back();
							$JSLib_Mobile_NavigationManager.HistoryBack();
						}
						$state = -1;
						break $sm1;
					}
					default: {
						break $sm1;
					}
				}
			}
		};
		$sm();
	};
	$JSLib_Mobile_Application.BackTo = function(T) {
		return function() {
			//查找app是否存有该指定要后退到的页面，如果不存在，则取消后退
			if (Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).all(function(n) {
				return !ss.referenceEquals(n.PageData.PageType, T);
			})) {
				$JSLib_Mobile_Application.SystemLog('指定后退到页面并不存在于历史记录中，取消后退操作!');
				return;
			}
			$JSLib_Mobile_NavigationManager.CurrentBackPageType = T;
			$JSLib_Mobile_Application.Back(1);
		};
	};
	$JSLib_Mobile_Application.Foward = function() {
		//Window.History.Forward();
		//暂时不实现前进功能
	};
	$JSLib_Mobile_Application.ShowLoading = function() {
		$('body').addClass('c-loading');
	};
	$JSLib_Mobile_Application.HideLoading = function() {
		$('body').removeClass('c-loading');
	};
	$JSLib_Mobile_Application.SetUserLocation = function(lon, lat, addr) {
		var $t1 = new $JSLib_Mobile_UserGeoLocation();
		$t1.Lontitiude = lon;
		$t1.Latitude = lat;
		$t1.Address = addr;
		var userGeoLocation = $t1;
		$JSLib_Mobile_Application.set_GeoLocation(userGeoLocation);
		if (!ss.staticEquals($JSLib_Mobile_Application.$1$OnUserLocationChangeField, null)) {
			var $t3 = $JSLib_Mobile_Application.$1$OnUserLocationChangeField;
			var $t2 = new $JSLib_Mobile_UserGeoLocationArgs();
			$t2.LocationData = userGeoLocation;
			$t3(null, $t2);
		}
	};
	$JSLib_Mobile_Application.get_GeoLocation = function() {
		var location = window.localStorage['userLocation'];
		if (ss.isValue(location)) {
			return JSON.parse(location.toString());
		}
		return null;
	};
	$JSLib_Mobile_Application.set_GeoLocation = function(value) {
		window.localStorage['userLocation'] = JSON.stringify(value);
	};
	$JSLib_Mobile_Application.add_OnUserLocationChange = function(value) {
		$JSLib_Mobile_Application.$1$OnUserLocationChangeField = ss.delegateCombine($JSLib_Mobile_Application.$1$OnUserLocationChangeField, value);
	};
	$JSLib_Mobile_Application.remove_OnUserLocationChange = function(value) {
		$JSLib_Mobile_Application.$1$OnUserLocationChangeField = ss.delegateRemove($JSLib_Mobile_Application.$1$OnUserLocationChangeField, value);
	};
	$JSLib_Mobile_Application.get_IsWeixin = function() {
		return window.navigator.userAgent.indexOf('MicroMessenger') !== -1;
	};
	$JSLib_Mobile_Application.get_IsCannotGoBack = function() {
		return window.history.length === 1;
	};
	$JSLib_Mobile_Application.get_BaseNameSpace = function() {
		if (ss.isNullOrEmptyString($JSLib_Mobile_Application.$_baseNameSpace)) {
			throw new ss.Exception('未设置页面命名空间，通常情况该值为 PhoneResource.Pages');
		}
		return $JSLib_Mobile_Application.$_baseNameSpace;
	};
	$JSLib_Mobile_Application.set_BaseNameSpace = function(value) {
		$JSLib_Mobile_Application.$_baseNameSpace = value;
	};
	$JSLib_Mobile_Application.HandleAndroidNumberTextBox = function() {
		$(document).on('focusin', 'input[data-number]', ss.thisFix(function(elem, event) {
			elem.setAttribute('data-oritype', elem.getAttribute('type'));
			elem.type = 'number';
		}));
		$(document).on('focusout', 'input[data-number]', ss.thisFix(function(elem1, event1) {
			elem1.type = elem1.getAttribute('data-oritype');
		}));
	};
	$JSLib_Mobile_Application.RegisterCommonEvent = function() {
		$JSLib_Mobile_Application.HandleAndroidNumberTextBox();
	};
	$JSLib_Mobile_Application.add_OnApplicationError = function(value) {
		$JSLib_Mobile_Application.$1$OnApplicationErrorField = ss.delegateCombine($JSLib_Mobile_Application.$1$OnApplicationErrorField, value);
	};
	$JSLib_Mobile_Application.remove_OnApplicationError = function(value) {
		$JSLib_Mobile_Application.$1$OnApplicationErrorField = ss.delegateRemove($JSLib_Mobile_Application.$1$OnApplicationErrorField, value);
	};
	$JSLib_Mobile_Application.RaiseError = function(errorData) {
		if (!ss.staticEquals($JSLib_Mobile_Application.$1$OnApplicationErrorField, null)) {
			$JSLib_Mobile_Application.$1$OnApplicationErrorField(null, errorData);
		}
	};
	$JSLib_Mobile_Application.SystemLog = function(message) {
		if ($JSLib_Mobile_Application.EnableSystemLog) {
			console.log(message, ss.formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss'));
		}
	};
	$JSLib_Mobile_Application.SystemLog$1 = function(tag, message) {
		if ($JSLib_Mobile_Application.EnableSystemLog) {
			console.log(tag, message, ss.formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss'));
		}
	};
	$JSLib_Mobile_Application.ChangeHash = function(hashString) {
		if (ss.startsWithString(hashString, '#')) {
			$JSLib_Mobile_Application.CurrentHashString = hashString.substr(1, hashString.length - 1);
		}
		else {
			$JSLib_Mobile_Application.CurrentHashString = hashString;
		}
		//Window.Location.Replace(JSLib.Util.URL.GetAllExceptHash() + "#" + CurrentHashString);
		$JSLib_Mobile_Application.SystemLog$1('当前Hash:', $JSLib_Mobile_Application.CurrentHashString);
		if (!ss.staticEquals($JSLib_Mobile_Application.$1$OnHashChangeField, null)) {
			var $t2 = $JSLib_Mobile_Application.$1$OnHashChangeField;
			var $t1 = new $JSLib_Mobile_HashChangeArgs();
			$t1.HashString = $JSLib_Mobile_Application.CurrentHashString;
			$t2(null, $t1);
		}
	};
	$JSLib_Mobile_Application.add_OnHashChange = function(value) {
		$JSLib_Mobile_Application.$1$OnHashChangeField = ss.delegateCombine($JSLib_Mobile_Application.$1$OnHashChangeField, value);
	};
	$JSLib_Mobile_Application.remove_OnHashChange = function(value) {
		$JSLib_Mobile_Application.$1$OnHashChangeField = ss.delegateRemove($JSLib_Mobile_Application.$1$OnHashChangeField, value);
	};
	global.JSLib.Mobile.Application = $JSLib_Mobile_Application;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.ApplicationErrorArgs
	var $JSLib_Mobile_ApplicationErrorArgs = function() {
		this.ErrorMessage = null;
		this.Stack = null;
		ss.EventArgs.call(this);
	};
	$JSLib_Mobile_ApplicationErrorArgs.__typeName = 'JSLib.Mobile.ApplicationErrorArgs';
	global.JSLib.Mobile.ApplicationErrorArgs = $JSLib_Mobile_ApplicationErrorArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.ApplicationMode
	var $JSLib_Mobile_ApplicationMode = function() {
	};
	$JSLib_Mobile_ApplicationMode.__typeName = 'JSLib.Mobile.ApplicationMode';
	global.JSLib.Mobile.ApplicationMode = $JSLib_Mobile_ApplicationMode;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.BasePage
	var $JSLib_Mobile_BasePage = function() {
		this.$2$OnPageInitField = null;
		this.$2$OnPageBeforeShowField = null;
		this.$2$OnPageShowField = null;
		this.$2$OnPageHideField = null;
		this.LastShowDateTime = new Date(0);
		this.IsPrepareDataError = false;
		this.IsIgnoreInHistory = false;
		this.$2$pageTranslateAnimationField = 0;
		this.InitialData = null;
		this.$isDestroyed = false;
		this.$2$OnDestroyField = null;
		$JSLib_Mobile_UIBase.call(this);
		this.LastShowDateTime = new Date();
	};
	$JSLib_Mobile_BasePage.__typeName = 'JSLib.Mobile.BasePage';
	$JSLib_Mobile_BasePage.$LogError = function(ex) {
		var innerException = null;
		innerException = ex;
		while (ss.isValue(innerException.get_innerException())) {
			innerException = innerException.get_innerException();
		}
		var $t1 = new $JSLib_Mobile_ApplicationErrorArgs();
		$t1.ErrorMessage = innerException.get_message();
		$t1.Stack = innerException.get_stack();
		var args = $t1;
		$JSLib_Mobile_Application.RaiseError(args);
	};
	global.JSLib.Mobile.BasePage = $JSLib_Mobile_BasePage;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.DeviceType
	var $JSLib_Mobile_DeviceType = function() {
	};
	$JSLib_Mobile_DeviceType.__typeName = 'JSLib.Mobile.DeviceType';
	global.JSLib.Mobile.DeviceType = $JSLib_Mobile_DeviceType;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.HashChangeArgs
	var $JSLib_Mobile_HashChangeArgs = function() {
		this.HashString = null;
		ss.EventArgs.call(this);
	};
	$JSLib_Mobile_HashChangeArgs.__typeName = 'JSLib.Mobile.HashChangeArgs';
	global.JSLib.Mobile.HashChangeArgs = $JSLib_Mobile_HashChangeArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.IShowLoading
	var $JSLib_Mobile_IShowLoading = function() {
	};
	$JSLib_Mobile_IShowLoading.__typeName = 'JSLib.Mobile.IShowLoading';
	global.JSLib.Mobile.IShowLoading = $JSLib_Mobile_IShowLoading;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.MessageBox
	var $JSLib_Mobile_MessageBox = function() {
	};
	$JSLib_Mobile_MessageBox.__typeName = 'JSLib.Mobile.MessageBox';
	$JSLib_Mobile_MessageBox.Confirm = function(message, title) {
		var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1;
		var $sm = function() {
			try {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							if (!ss.staticEquals($JSLib_Mobile_MessageBox.ConfirmHandler, null)) {
								$t1 = $JSLib_Mobile_MessageBox.ConfirmHandler(message);
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							else {
								$tcs.setResult(window.confirm(message, title));
								return;
							}
						}
						case 1: {
							$state = -1;
							$tcs.setResult($t1.getAwaitedResult());
							return;
						}
						default: {
							break $sm1;
						}
					}
				}
			}
			catch ($t2) {
				$tcs.setException(ss.Exception.wrap($t2));
			}
		};
		$sm();
		return $tcs.task;
	};
	$JSLib_Mobile_MessageBox.Alert = function(message, title) {
		if (!ss.staticEquals($JSLib_Mobile_MessageBox.AlertHandler, null)) {
			$JSLib_Mobile_MessageBox.AlertHandler(message);
			return;
		}
		window.alert(message, title);
	};
	global.JSLib.Mobile.MessageBox = $JSLib_Mobile_MessageBox;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.MobilePage
	var $JSLib_Mobile_MobilePage = function() {
	};
	$JSLib_Mobile_MobilePage.__typeName = 'JSLib.Mobile.MobilePage';
	$JSLib_Mobile_MobilePage.GeneratePageId$1 = function(targetType, initialData) {
		var page = ss.replaceAllString(ss.getTypeFullName(targetType), '.', '-');
		var pageData = '';
		if (ss.isValue(initialData)) {
			pageData = '--' + Base64.encode(JSON.stringify(initialData));
		}
		return page + pageData;
	};
	$JSLib_Mobile_MobilePage.GeneratePageId = function(targetType, initialData) {
		var pageData = '';
		if (ss.isValue(initialData)) {
			pageData = '--' + Base64.encode(JSON.stringify(initialData));
		}
		return targetType + pageData;
	};
	$JSLib_Mobile_MobilePage.Prepare = function(mobilePageTypes, autoLoad, defaultHash) {
		$JSLib_Mobile_MobilePage.AllMobilePageTypes = mobilePageTypes;
		var mobilePageDics = new (ss.makeGenericType(ss.Dictionary$2, [String, Function]))();
		for (var $t1 = 0; $t1 < mobilePageTypes.length; $t1++) {
			var typeItem = mobilePageTypes[$t1];
			mobilePageDics.add(ss.replaceAllString(ss.getTypeFullName(typeItem), '.', '-'), typeItem);
		}
		$JSLib_Mobile_PageLoader.RegisterMobilePages = mobilePageDics;
		//jQuery.Window.On("hashchange", delegate(jQueryEvent @event)
		//{
		$JSLib_Mobile_Application.add_OnHashChange(function(sender, args) {
			var hashstring = args.HashString;
			//如果后退到入口页，则不做任何事
			if (ss.isNullOrEmptyString(hashstring)) {
				if (!ss.isNullOrEmptyString(defaultHash)) {
					hashstring = defaultHash;
				}
				else {
					return;
				}
			}
			if ($JSLib_Mobile_MobilePage.IsForwarding) {
				$JSLib_Mobile_MobilePage.IsForwarding = false;
				return;
			}
			else {
				var currentPageData = $JSLib_Mobile_PageData.FromHashString(hashstring);
				//如果当前页与目标页之间数量大于1，则提前销毁中间页面
				if ($JSLib_Mobile_NavigationManager.CurrentBackPageCount > 1) {
					//获取当前中间页面，并且销毁
					if (Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).any(function(n) {
						return n.PageData.getHashCode() === currentPageData.getHashCode();
					})) {
						Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).first(function(n1) {
							return n1.PageData.getHashCode() === currentPageData.getHashCode();
						}).BasePage.Destroy();
					}
					$JSLib_Mobile_NavigationManager.CurrentBackPageCount--;
					$JSLib_Mobile_Application.Back(1);
					return;
				}
				//如果指定了后退到页面，则执行连续后退，直到遇到指定的页面类型为止
				if (ss.isValue($JSLib_Mobile_NavigationManager.CurrentBackPageType)) {
					if (!ss.referenceEquals(currentPageData.PageType, $JSLib_Mobile_NavigationManager.CurrentBackPageType)) {
						//获取当前中间页面，并且销毁
						if (Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).any(function(n2) {
							return n2.PageData.getHashCode() === currentPageData.getHashCode();
						})) {
							Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).first(function(n3) {
								return n3.PageData.getHashCode() === currentPageData.getHashCode();
							}).BasePage.Destroy();
							$JSLib_Mobile_Application.Back(1);
						}
						return;
					}
					else {
						$JSLib_Mobile_NavigationManager.CurrentBackPageType = null;
					}
				}
				if (ss.isNullOrUndefined(currentPageData.PageType)) {
					return;
				}
				$JSLib_Mobile_Application.SystemLog$1('后退页面!', ss.getTypeFullName(currentPageData.PageType));
				$JSLib_Mobile_MobilePage.OnNavigateToPage(currentPageData, true, null);
			}
		});
		//});
		if (autoLoad) {
			$JSLib_Mobile_Application.SystemLog('----autoload');
			//如果地址栏存在页面信息数据,则自动加载该页面
			$JSLib_Mobile_Application.CurrentHashString = window.location.hash;
			if (ss.isNullOrEmptyString($JSLib_Mobile_Application.CurrentHashString)) {
				return false;
			}
			var pageData = $JSLib_Mobile_PageData.FromHashString($JSLib_Mobile_Application.CurrentHashString);
			if (ss.isNullOrUndefined(pageData)) {
				return false;
			}
			$JSLib_Mobile_MobilePage.IsEntryPage = false;
			$JSLib_Mobile_Application.SystemLog('----OnNavigateToPage');
			$JSLib_Mobile_NavigationManager.HistoryData.push($JSLib_Mobile_Application.CurrentHashString);
			$JSLib_Mobile_MobilePage.OnNavigateToPage(pageData, false, null);
			return true;
		}
		return false;
	};
	$JSLib_Mobile_MobilePage.GotoPage = function(TMobilePage) {
		return function(initialData, OnPageShow) {
			if (!ss.staticEquals($JSLib_Mobile_MobilePage.OnNavigatingPage, null)) {
				if (!$JSLib_Mobile_MobilePage.OnNavigatingPage(TMobilePage)) {
					return;
				}
			}
			$JSLib_SingleTask.Enquee(function() {
				var $state = 0, $tcs = new ss.TaskCompletionSource(), pageType, pageHash, $t1;
				var $sm = function() {
					try {
						$sm1:
						for (;;) {
							switch ($state) {
								case 0: {
									$state = -1;
									pageType = TMobilePage;
									pageHash = $JSLib_Mobile_MobilePage.GeneratePageId$1(pageType, initialData);
									$JSLib_Mobile_MobilePage.$ForwardPage(TMobilePage).call(null, pageHash, OnPageShow);
									if (ss.isValue(initialData)) {
										console.info($CoreEx.ToDebugString(Object).call(null, initialData));
									}
									if ($JSLib_Mobile_Application.DeviceType === 10) {
										$t1 = ss.Task.delay(500);
										$state = 1;
										$t1.continueWith($sm);
										return;
									}
									$state = -1;
									break $sm1;
								}
								case 1: {
									$state = -1;
									$t1.getAwaitedResult();
									$state = -1;
									break $sm1;
								}
								default: {
									break $sm1;
								}
							}
						}
						$tcs.setResult(null);
					}
					catch ($t2) {
						$tcs.setException(ss.Exception.wrap($t2));
					}
				};
				$sm();
				return $tcs.task;
			});
		};
	};
	$JSLib_Mobile_MobilePage.GotoPage$1 = function(pageTypeStr, initialData, OnPageShow) {
		if (ss.isNullOrEmptyString(pageTypeStr)) {
			throw new ss.ArgumentException('{pageTypeStr}不能为空');
		}
		var pageHash = $JSLib_Mobile_MobilePage.GeneratePageId(ss.replaceAllString(pageTypeStr, '.', '-'), initialData);
		$JSLib_Mobile_MobilePage.$ForwardPage($JSLib_Mobile_BasePage).call(null, pageHash, OnPageShow);
	};
	$JSLib_Mobile_MobilePage.GotoPageDynamic = function(pageSpace, pageName, inlitialData, OnPageShow) {
		var type = $JSLib_Mobile_PageLoader.LoadPageType$1(pageSpace, pageName);
		if (!ss.staticEquals($JSLib_Mobile_MobilePage.OnNavigatingPage, null)) {
			if (!$JSLib_Mobile_MobilePage.OnNavigatingPage(type)) {
				return;
			}
		}
		var pageHash = $JSLib_Mobile_MobilePage.GeneratePageId$1(type, inlitialData);
		$JSLib_Mobile_MobilePage.$ForwardPage($JSLib_Mobile_BasePage).call(null, pageHash, OnPageShow);
	};
	$JSLib_Mobile_MobilePage.$ForwardPage = function(TMobilePage) {
		return function(pageHash, OnPageShow) {
			$JSLib_Mobile_MobilePage.IsForwarding = true;
			$JSLib_Mobile_NavigationManager.HistoryData.push(pageHash);
			$JSLib_Mobile_Application.ChangeHash(pageHash);
			var pageData = $JSLib_Mobile_PageData.FromHashString(pageHash);
			if (ss.isValue(pageData)) {
				$JSLib_Mobile_MobilePage.OnNavigateToPage(pageData, false, OnPageShow);
			}
		};
	};
	$JSLib_Mobile_MobilePage.OnNavigateToPage = function(pageData, isBack, OnPageShow) {
		var pageInstance;
		if (Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).any(function(n) {
			return n.PageData.getHashCode() === pageData.getHashCode();
		})) {
			$JSLib_Mobile_Application.SystemLog('全局页面实例中包含此页面对象');
			pageInstance = Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).first(function(n1) {
				return n1.PageData.getHashCode() === pageData.getHashCode();
			}).BasePage;
		}
		else {
			if (!ss.contains($JSLib_Mobile_MobilePage.AllMobilePageTypes, pageData.PageType)) {
				console.error('未找到此页面:', [pageData]);
				return;
			}
			pageInstance = ss.cast(ss.createInstance(pageData.PageType), $JSLib_Mobile_BasePage);
			pageInstance.InitialData = $CoreEx.Clone(Object).call(null, pageData.PageInitialData);
		}
		$JSLib_Mobile_MobilePage.$HandleChangePage($JSLib_Mobile_BasePage).call(null, pageInstance, pageData, OnPageShow);
	};
	$JSLib_Mobile_MobilePage.$HandleChangePage = function(TMobilePage) {
		return function(page, pageData, OnPageShow) {
			var $state1 = 0, initialData, pageType, PrePage, isExistCss, $t1, item, $t2, $t3, isExistPage, $t5, $t4, $t6, $t7, ex, innerException, $t9, args;
			var $sm1 = function() {
				$sm1:
				for (;;) {
					switch ($state1) {
						case 0: {
							$state1 = -1;
							initialData = $CoreEx.Clone(Object).call(null, pageData.PageInitialData);
							pageType = pageData.PageType;
							$JSLib_Mobile_Application.SystemLog('----OnNavigatingPage');
							if (!ss.staticEquals($JSLib_Mobile_MobilePage.OnNavigatingPage, null)) {
								if (!$JSLib_Mobile_MobilePage.OnNavigatingPage(pageType)) {
									return;
								}
							}
							if (ss.isValue($JSLib_Mobile_MobilePage.CurrentPage) && ss.referenceEquals(ss.getInstanceType($JSLib_Mobile_MobilePage.CurrentPage), ss.getInstanceType(page))) {
								if (ss.isNullOrUndefined(pageData.PageInitialData) && ss.isNullOrUndefined($JSLib_Mobile_MobilePage.CurrentPage.InitialData)) {
									return;
								}
								if (ss.referenceEquals(pageData.PageInitialData, $JSLib_Mobile_MobilePage.CurrentPage.InitialData)) {
									return;
								}
							}
							PrePage = $JSLib_Mobile_MobilePage.CurrentPage;
							$JSLib_Mobile_MobilePage.CurrentPage = page;
							//新增并跳转页面
							if (ss.isNullOrUndefined(page.pageObject)) {
								$JSLib_Mobile_Application.SystemLog('----页面数据为空，新创建页面！');
								if ($JSLib_Mobile_Application.AutoLoadCss) {
									$JSLib_Mobile_Application.SystemLog('----AutoLoadCSS');
									//加载CSS
									isExistCss = false;
									for ($t1 = 0; $t1 < $JSLib_Mobile_MobilePage.DomPageRepository.length; $t1++) {
										item = $JSLib_Mobile_MobilePage.DomPageRepository[$t1];
										if (ss.referenceEquals(item.PageData.PageType, pageData.PageType)) {
											isExistCss = true;
										}
									}
									if (!isExistCss) {
										$t2 = $JSLib_Mobile_PageLoader.LoadCss(pageData.PageType);
										$state1 = 2;
										$t2.continueWith($sm1);
										return;
									}
									$state1 = 1;
									continue $sm1;
								}
								$state1 = 1;
								continue $sm1;
							}
							else {
								$JSLib_Mobile_Application.SystemLog('系统后退操作');
								window.setTimeout(function() {
									var $state = 0, pageOutClass, tsk, onAnimationEnd, $t10;
									var $sm = function() {
										$sm1:
										for (;;) {
											switch ($state) {
												case 0: {
													$state = -1;
													//后退
													if (ss.isValue(PrePage)) {
														page.pageObject.show();
														if ($JSLib_JSLibConfig.PageTransitionEffect) {
															pageOutClass = PrePage.GetTraslationInOutClass().item2;
															tsk = new ss.TaskCompletionSource();
															onAnimationEnd = null;
															onAnimationEnd = ss.thisFix(function(elem, event) {
																PrePage.pageObject.off('webkitAnimationEnd', onAnimationEnd);
																tsk.setResult(null);
															});
															PrePage.pageObject.on('webkitAnimationEnd', onAnimationEnd);
															PrePage.pageObject.addClass(pageOutClass);
															//temp add
															//PrePage.pageObject.Show();
															$state = 2;
															tsk.task.continueWith($sm);
															return;
														}
														$state = 1;
														continue $sm1;
													}
													$state = -1;
													break $sm1;
												}
												case 2: {
													$state = -1;
													tsk.task.getAwaitedResult();
													$state = 1;
													continue $sm1;
												}
												case 1: {
													$state = -1;
													$t10 = page.Show(false);
													$state = 3;
													$t10.continueWith($sm);
													return;
												}
												case 3: {
													$state = -1;
													$t10.getAwaitedResult();
													if (!ss.staticEquals(OnPageShow, null)) {
														OnPageShow(page);
													}
													PrePage.TriggerHideEvent();
													//GotoPage导致的后退，不处理前一页面销毁操作
													//PrePage.Destroy();
													$state = -1;
													break $sm1;
												}
												default: {
													break $sm1;
												}
											}
										}
									};
									$sm();
								}, 0);
								$state1 = -1;
								break $sm1;
							}
						}
						case 2: {
							$state1 = -1;
							$t2.getAwaitedResult();
							$state1 = 1;
							continue $sm1;
						}
						case 1: {
							$state1 = -1;
							$JSLib_Mobile_Application.SystemLog('----PreparePage');
							//构建页面dom数据
							$t3 = page.PreparePage();
							$state1 = 3;
							$t3.continueWith($sm1);
							return;
						}
						case 3: {
							$state1 = -1;
							page.pageObject = $t3.getAwaitedResult();
							//page.pageObject.Hide();
							page.InitialData = $CoreEx.Clone(Object).call(null, initialData);
							$JSLib_Mobile_MobilePage.PageContainer.append(page.pageObject);
							page.InitialComponent();
							page.pageObject.attr('data-sid', $JSLib_Mobile_MobilePage.GeneratePageId$1(pageType, initialData));
							//如果加载的页面中不包含即将跳转的页面，则创建对应的页面，并将其加载到dom树中
							isExistPage = Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).any(function(n) {
								return n.PageData.getHashCode() === (new $JSLib_Mobile_PageData(pageType, initialData)).getHashCode();
							});
							if (!isExistPage) {
								$t5 = $JSLib_Mobile_MobilePage.DomPageRepository;
								$t4 = new $JSLib_Mobile_PageInstanceData();
								$t4.PageData = new $JSLib_Mobile_PageData(pageType, initialData);
								$t4.BasePage = page;
								$t5.push($t4);
							}
							//直接显示出错页面
							if (page.IsPrepareDataError) {
								$t6 = page.Show(true);
								$state1 = 5;
								$t6.continueWith($sm1);
								return;
							}
							else {
								$state1 = 6;
								continue $sm1;
							}
						}
						case 5: {
							$state1 = -1;
							$t6.getAwaitedResult();
							$state1 = 4;
							continue $sm1;
						}
						case 6:
						case 7:
						case 8: {
							if ($state1 === 6) {
								$state1 = 7;
							}
							try {
								$sm2:
								for (;;) {
									switch ($state1) {
										case 7: {
											$state1 = -1;
											page.TriggerInitEvent();
											$t7 = page.Show(true);
											$state1 = 8;
											$t7.continueWith($sm1);
											return;
										}
										case 8: {
											$state1 = -1;
											$t7.getAwaitedResult();
											if (ss.isValue(PrePage)) {
												PrePage.Hide();
											}
											if (!ss.staticEquals(OnPageShow, null)) {
												OnPageShow(page);
											}
											$state1 = -1;
											break $sm2;
										}
										default: {
											break $sm2;
										}
									}
								}
							}
							catch ($t8) {
								ex = ss.Exception.wrap($t8);
								console.error(ex);
								innerException = null;
								innerException = ex;
								while (ss.isValue(innerException.get_innerException())) {
									innerException = innerException.get_innerException();
								}
								$t9 = new $JSLib_Mobile_ApplicationErrorArgs();
								$t9.ErrorMessage = innerException.get_message();
								$t9.Stack = innerException.get_stack();
								args = $t9;
								$JSLib_Mobile_Application.RaiseError(args);
							}
							$state1 = 4;
							continue $sm1;
						}
						case 4: {
							$state1 = -1;
							//if (PrePage != null)
							//{
							//    PrePage.Hide();
							//}
							$state1 = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			};
			$sm1();
		};
	};
	global.JSLib.Mobile.MobilePage = $JSLib_Mobile_MobilePage;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.NavigationManager
	var $JSLib_Mobile_NavigationManager = function() {
	};
	$JSLib_Mobile_NavigationManager.__typeName = 'JSLib.Mobile.NavigationManager';
	$JSLib_Mobile_NavigationManager.HistoryBack = function() {
		if ($JSLib_Mobile_NavigationManager.HistoryData.length > 1) {
			ss.removeAt($JSLib_Mobile_NavigationManager.HistoryData, $JSLib_Mobile_NavigationManager.HistoryData.length - 1);
		}
		$JSLib_Mobile_Application.ChangeHash(Enumerable.from($JSLib_Mobile_NavigationManager.HistoryData).last());
	};
	$JSLib_Mobile_NavigationManager.BackToRemoveModalPanel = function() {
		for (var $t1 = 0; $t1 < $JSLib_Mobile_NavigationManager.ibackControls.length; $t1++) {
			var ibackControl = $JSLib_Mobile_NavigationManager.ibackControls[$t1];
			var args = new $JSLib_Widget_BackArgs();
			ibackControl.OnBackButton(args);
			if (!args.IsContinueGoBack) {
				return false;
			}
		}
		return true;
	};
	$JSLib_Mobile_NavigationManager.RegisterBackControl = function(backEvent) {
		$JSLib_Mobile_NavigationManager.ibackControls.push(backEvent);
	};
	$JSLib_Mobile_NavigationManager.UnRegisterBackControl = function(backEvent) {
		ss.remove($JSLib_Mobile_NavigationManager.ibackControls, backEvent);
	};
	global.JSLib.Mobile.NavigationManager = $JSLib_Mobile_NavigationManager;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.NetWorkState
	var $JSLib_Mobile_NetWorkState = function() {
	};
	$JSLib_Mobile_NetWorkState.__typeName = 'JSLib.Mobile.NetWorkState';
	global.JSLib.Mobile.NetWorkState = $JSLib_Mobile_NetWorkState;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.OrientationChangArgs
	var $JSLib_Mobile_OrientationChangArgs = function() {
		this.OrientationValue = 0;
	};
	$JSLib_Mobile_OrientationChangArgs.__typeName = 'JSLib.Mobile.OrientationChangArgs';
	global.JSLib.Mobile.OrientationChangArgs = $JSLib_Mobile_OrientationChangArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.PageData
	var $JSLib_Mobile_PageData = function(pageType, pageInitialData) {
		this.PageType = null;
		this.PageInitialData = null;
		this.PageType = pageType;
		this.PageInitialData = pageInitialData;
	};
	$JSLib_Mobile_PageData.__typeName = 'JSLib.Mobile.PageData';
	$JSLib_Mobile_PageData.FromHashString = function(hash) {
		var hashData = '';
		if (ss.startsWithString(hash, '#')) {
			hashData = hash.substr(1, hash.length - 1);
		}
		else {
			hashData = hash;
		}
		var inlitialData = '';
		var hashSplitData = hashData.split('--');
		var typedata = hashSplitData[0];
		if (hashSplitData.length === 2) {
			inlitialData = hashSplitData[1];
		}
		var pageInitialData = (ss.isNullOrEmptyString(inlitialData) ? null : JSON.parse(Base64.decode(inlitialData)));
		var targetMobilePageType = $JSLib_Mobile_PageData.$GetTypeFromHash(typedata);
		if (ss.isNullOrUndefined(targetMobilePageType)) {
			return null;
		}
		return new $JSLib_Mobile_PageData(targetMobilePageType, pageInitialData);
	};
	$JSLib_Mobile_PageData.$GetTypeFromHash = function(typestr) {
		if (!$JSLib_Mobile_PageLoader.RegisterMobilePages.containsKey(typestr)) {
			return $JSLib_Mobile_PageLoader.LoadPageType(typestr);
		}
		else {
			return $JSLib_Mobile_PageLoader.RegisterMobilePages.get_item(typestr);
		}
	};
	global.JSLib.Mobile.PageData = $JSLib_Mobile_PageData;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.PageInstanceData
	var $JSLib_Mobile_PageInstanceData = function() {
		this.PageData = null;
		this.BasePage = null;
	};
	$JSLib_Mobile_PageInstanceData.__typeName = 'JSLib.Mobile.PageInstanceData';
	global.JSLib.Mobile.PageInstanceData = $JSLib_Mobile_PageInstanceData;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.PageLoader
	var $JSLib_Mobile_PageLoader = function() {
	};
	$JSLib_Mobile_PageLoader.__typeName = 'JSLib.Mobile.PageLoader';
	$JSLib_Mobile_PageLoader.LoadPageType = function(pageTypeStr) {
		var resultType = null;
		var generatePageId = '';
		var baseNameSpaceLength = $JSLib_Mobile_Application.get_BaseNameSpace().length;
		//类似 "Navigation-Page1"
		var leftStr = pageTypeStr.substr(baseNameSpaceLength + 1);
		//类似 "Navigation"
		var pageSpace = ss.replaceAllString(leftStr.substr(0, leftStr.lastIndexOf('-')), '-', '/');
		//类似 "Page1"
		var pageName = (ss.isNullOrEmptyString(pageSpace) ? leftStr.substr(pageSpace.length) : leftStr.substr(pageSpace.length + 1));
		//根据类型字符串中定位JS文件路径
		var pagePathWithoutExtension = $JSLib_Mobile_Application.RootPath + ss.replaceAllString(pageSpace, '.', '/') + '/' + pageName;
		var jsFilePath = pagePathWithoutExtension + '.js';
		if (!$JSLib_Mobile_PageLoader.RegisterMobilePages.containsKey(pageTypeStr)) {
			//同步加载Js文件，并获取运行时的页面类型
			var result = $.ajax({ async: false, url: jsFilePath, dataType: 'script', type: 'get' });
			if (ss.isNullOrEmptyString(result.responseText)) {
				throw new ss.Exception('页面内容为空！');
			}
			//加载的类型为最后一个程序集的第一个类型
			resultType = ss.getAssemblyTypes(Enumerable.from(ss.getAssemblies()).last())[0];
			if (ss.isNullOrUndefined(resultType)) {
				return null;
			}
			generatePageId = $JSLib_Mobile_MobilePage.GeneratePageId$1(resultType, null);
			$JSLib_Mobile_PageLoader.RegisterMobilePages.add(generatePageId, resultType);
		}
		else {
			resultType = $JSLib_Mobile_PageLoader.RegisterMobilePages.get_item(pageTypeStr);
		}
		return resultType;
	};
	$JSLib_Mobile_PageLoader.LoadPageType$1 = function(pageSpace, pageName) {
		var baseNameSpace = $JSLib_Mobile_Application.get_BaseNameSpace();
		if (!ss.isNullOrEmptyString(pageSpace)) {
			baseNameSpace = baseNameSpace + '.' + pageSpace;
		}
		//判断页面类型是否已注册
		//如果未注册需要加载对应的JS文件
		var pagetypeStr = ss.replaceAllString(baseNameSpace, '.', '-') + '-' + pageName;
		var pagePathWithoutExtension = $JSLib_Mobile_Application.RootPath + ss.replaceAllString(pageSpace, '.', '/') + '/' + pageName;
		if (!$JSLib_Mobile_PageLoader.RegisterMobilePages.containsKey(pagetypeStr)) {
			var jsPath = pagePathWithoutExtension + '.js';
			//同步加载Js文件，并获取运行时的页面类型
			$.ajax({ async: false, url: jsPath, dataType: 'script', type: 'get' });
			var type = ss.getType(baseNameSpace + '.' + pageName, ss.load(pageName));
			$JSLib_Mobile_PageLoader.RegisterMobilePages.add($JSLib_Mobile_MobilePage.GeneratePageId$1(type, null), type);
			return type;
		}
		else {
			var resultType = $JSLib_Mobile_PageLoader.RegisterMobilePages.get_item(pagetypeStr);
			return resultType;
		}
	};
	$JSLib_Mobile_PageLoader.LoadCss = function(pageType) {
		var tsk = new ss.TaskCompletionSource();
		//加载页面对应的CSS文件
		var generatePageId = $JSLib_Mobile_MobilePage.GeneratePageId$1(pageType, null);
		var pagePathWithoutExtension = '';
		pagePathWithoutExtension = $JSLib_Mobile_Application.RootPath + ss.replaceAllString(ss.getTypeFullName(pageType).substr($JSLib_Mobile_Application.get_BaseNameSpace().length + 1), '.', '/');
		var pageCSSSelector = ss.formatString("link[data-type='{0}']", generatePageId);
		if ($(pageCSSSelector).length === 0) {
			var cssPath = pagePathWithoutExtension + '.css';
			if ($JSLib_Mobile_Application.LoadCssForce) {
				cssPath = cssPath + '?r=' + Math.random();
			}
			var styleEle = $(ss.formatString("<link data-type='{0}' rel='stylesheet' href='{1}' />", generatePageId, cssPath));
			styleEle.appendTo('head');
			$JSLib_Mobile_Application.SystemLog('----AuoLoadCSS CSS Append To Head');
			var appendResult = false;
			styleEle[0].onload = function(event) {
				appendResult = true;
				tsk.setResult(true);
			};
			styleEle[0].onerror = function(event1) {
				$JSLib_Mobile_Application.SystemLog('----AuoLoadCSS Error');
				appendResult = false;
				tsk.setResult(false);
			};
			window.setTimeout(function() {
				if (!appendResult) {
					tsk.setResult(true);
				}
			}, 1000);
		}
		else {
			tsk.setResult(true);
		}
		return tsk.task;
	};
	global.JSLib.Mobile.PageLoader = $JSLib_Mobile_PageLoader;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.PageResourceManager
	var $JSLib_Mobile_PageResourceManager = function() {
	};
	$JSLib_Mobile_PageResourceManager.__typeName = 'JSLib.Mobile.PageResourceManager';
	$JSLib_Mobile_PageResourceManager.CheckAndReleasePage = function() {
		if ($JSLib_Mobile_MobilePage.DomPageRepository.length > $JSLib_Mobile_PageResourceManager.MaxPageCount) {
			var releaseCount = $JSLib_Mobile_MobilePage.DomPageRepository.length - $JSLib_Mobile_PageResourceManager.MaxPageCount;
			var pages = Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).orderBy(function(n) {
				return n.BasePage.LastShowDateTime;
			}).take(releaseCount);
			var $t1 = pages.getEnumerator();
			try {
				while ($t1.moveNext()) {
					var page = $t1.current();
					page.BasePage.Destroy();
				}
			}
			finally {
				$t1.dispose();
			}
		}
	};
	global.JSLib.Mobile.PageResourceManager = $JSLib_Mobile_PageResourceManager;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.PageTranslateAnimation
	var $JSLib_Mobile_PageTranslateAnimation = function() {
	};
	$JSLib_Mobile_PageTranslateAnimation.__typeName = 'JSLib.Mobile.PageTranslateAnimation';
	global.JSLib.Mobile.PageTranslateAnimation = $JSLib_Mobile_PageTranslateAnimation;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.PhoneGap
	var $JSLib_Mobile_PhoneGap = function() {
	};
	$JSLib_Mobile_PhoneGap.__typeName = 'JSLib.Mobile.PhoneGap';
	$JSLib_Mobile_PhoneGap.IsExistDirectory = function(directoryPath) {
		var tsk = new ss.TaskCompletionSource();
		(window.requestFileSystem || window.webkitRequestFileSystem)(1, 0, function(system) {
			system.root.getDirectory(directoryPath, { create: false }, function(entry) {
				tsk.setResult(true);
			}, function(error) {
				tsk.setResult(false);
			});
		}, function(o) {
		});
		return tsk.task;
	};
	$JSLib_Mobile_PhoneGap.CreateDirectory = function(directoryPath) {
		var tsk = new ss.TaskCompletionSource();
		(window.requestFileSystem || window.webkitRequestFileSystem)(1, 0, function(system) {
			system.root.getDirectory(directoryPath, { create: true }, function(entry) {
				tsk.setResult(true);
			}, function(error) {
				tsk.setResult(false);
			});
		}, function(o) {
		});
		return tsk.task;
	};
	$JSLib_Mobile_PhoneGap.Exec = function(T) {
		return function(pluginName, actionName, arguments1) {
			var tsk = new ss.TaskCompletionSource();
			cordova.exec(function(o) {
				tsk.setResult(o);
			}, function(o1) {
			}, pluginName, actionName, arguments1);
			return tsk.task;
		};
	};
	global.JSLib.Mobile.PhoneGap = $JSLib_Mobile_PhoneGap;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.UIBase
	var $JSLib_Mobile_UIBase = function() {
		this.$1$OnOrientationChangeField = null;
		this.contentBuilder = new ss.StringBuilder();
		this.pageObject = null;
		this.$iscrollContainers = [];
		this.$iscrollListContainers = [];
		this.UniqueId = ss.Guid.newGuid().toString();
		if (ss.isNullOrUndefined(window.screen.orientation)) {
			return;
		}
		window.screen.orientation.addEventListener('change', ss.mkdel(this, function(event) {
			if (!ss.staticEquals(this.$1$OnOrientationChangeField, null)) {
				window.setTimeout(ss.mkdel(this, function() {
					var $t2 = this.$1$OnOrientationChangeField;
					var $t1 = new $JSLib_Mobile_OrientationChangArgs();
					$t1.OrientationValue = window.orientation;
					$t2($t1);
				}), 200);
			}
		}));
	};
	$JSLib_Mobile_UIBase.__typeName = 'JSLib.Mobile.UIBase';
	global.JSLib.Mobile.UIBase = $JSLib_Mobile_UIBase;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.UserGeoLocation
	var $JSLib_Mobile_UserGeoLocation = function() {
		this.Lontitiude = 0;
		this.Latitude = 0;
		this.Address = null;
	};
	$JSLib_Mobile_UserGeoLocation.__typeName = 'JSLib.Mobile.UserGeoLocation';
	global.JSLib.Mobile.UserGeoLocation = $JSLib_Mobile_UserGeoLocation;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Mobile.UserGeoLocationArgs
	var $JSLib_Mobile_UserGeoLocationArgs = function() {
		this.LocationData = null;
		ss.EventArgs.call(this);
	};
	$JSLib_Mobile_UserGeoLocationArgs.__typeName = 'JSLib.Mobile.UserGeoLocationArgs';
	global.JSLib.Mobile.UserGeoLocationArgs = $JSLib_Mobile_UserGeoLocationArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Oauth.SinaLogin
	var $JSLib_Oauth_SinaLogin = function() {
	};
	$JSLib_Oauth_SinaLogin.__typeName = 'JSLib.Oauth.SinaLogin';
	$JSLib_Oauth_SinaLogin.LoadSinaLoginJSSDK = function() {
	};
	global.JSLib.Oauth.SinaLogin = $JSLib_Oauth_SinaLogin;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Plugin.Printer
	var $JSLib_Plugin_Printer = function() {
	};
	$JSLib_Plugin_Printer.__typeName = 'JSLib.Plugin.Printer';
	$JSLib_Plugin_Printer.IsPrinterAvaiable = function() {
		var tsk = new ss.TaskCompletionSource();
		cordova.exec(function(o) {
			console.info('pinter available:' + o.toString());
			tsk.setResult(o);
		}, function(o1) {
			tsk.setResult(false);
		}, 'PrintPlugin', 'isPrintingAvailable', []);
		return tsk.task;
	};
	$JSLib_Plugin_Printer.Print = function(printHtmlContent) {
		var $t1 = [];
		$t1.push(printHtmlContent);
		$t1.push(null);
		cordova.exec(null, null, 'PrintPlugin', 'print', $t1);
	};
	global.JSLib.Plugin.Printer = $JSLib_Plugin_Printer;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.ResourceManager.ResourceLoader
	var $JSLib_ResourceManager_ResourceLoader = function() {
	};
	$JSLib_ResourceManager_ResourceLoader.__typeName = 'JSLib.ResourceManager.ResourceLoader';
	global.JSLib.ResourceManager.ResourceLoader = $JSLib_ResourceManager_ResourceLoader;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.UnionLogin.QQLogin
	var $JSLib_UnionLogin_QQLogin = function() {
	};
	$JSLib_UnionLogin_QQLogin.__typeName = 'JSLib.UnionLogin.QQLogin';
	$JSLib_UnionLogin_QQLogin.PreapareJS = function() {
		var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1;
		var $sm = function() {
			try {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							if ($JSLib_UnionLogin_QQLogin.$isPreparedJs) {
								$tcs.setResult(null);
								return;
							}
							$t1 = (new $JSLib_ResourceManager_ResourceLoader()).LoadJS('http://qzonestyle.gtimg.cn/qzone/openapi/qc-1.0.1.js', ['data-appid', $JSLib_UnionLogin_QQLogin.$QQAPPID]);
							$state = 1;
							$t1.continueWith($sm);
							return;
						}
						case 1: {
							$state = -1;
							$t1.getAwaitedResult();
							$JSLib_UnionLogin_QQLogin.$isPreparedJs = true;
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
				$tcs.setResult(null);
			}
			catch ($t2) {
				$tcs.setException(ss.Exception.wrap($t2));
			}
		};
		$sm();
		return $tcs.task;
	};
	$JSLib_UnionLogin_QQLogin.GetUserInfo = function() {
		var tsk = new ss.TaskCompletionSource();
		QC.Login.getMe(function(openId, accessToken) {
			var url = ss.formatString('https://graph.qq.com/user/get_user_info?oauth_consumer_key={0}&access_token={1}&openid={2}&format=json', $JSLib_UnionLogin_QQLogin.$QQAPPID, accessToken, openId);
			var option = {
				url: url,
				success: function(data, status, request) {
					tsk.setResult(JSON.parse(data));
				}
			};
			$.ajax(option);
		});
		return tsk.task;
	};
	$JSLib_UnionLogin_QQLogin.ShowPopUp = function(redirectUrl, OnWindowClose) {
		var windower = QC.Login.showPopup({ appId: $JSLib_UnionLogin_QQLogin.$QQAPPID, redirectURI: redirectUrl });
		windower.addEventListener('beforeunload', function(event) {
			if (!ss.staticEquals(OnWindowClose, null)) {
				OnWindowClose();
			}
		});
		return windower;
	};
	$JSLib_UnionLogin_QQLogin.GetMe = function() {
		var tsk = new ss.TaskCompletionSource();
		QC.Login.getMe(function(openId, accessToken) {
			var $t1 = new $JSLib_UnionLogin_QQLoignAccessInfo();
			$t1.openId = openId;
			$t1.accessToken = accessToken;
			tsk.setResult($t1);
		});
		return tsk.task;
	};
	global.JSLib.UnionLogin.QQLogin = $JSLib_UnionLogin_QQLogin;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.UnionLogin.QQLoignAccessInfo
	var $JSLib_UnionLogin_QQLoignAccessInfo = function() {
		this.openId = null;
		this.accessToken = null;
	};
	$JSLib_UnionLogin_QQLoignAccessInfo.__typeName = 'JSLib.UnionLogin.QQLoignAccessInfo';
	global.JSLib.UnionLogin.QQLoignAccessInfo = $JSLib_UnionLogin_QQLoignAccessInfo;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Util.IocContainer
	var $JSLib_Util_IocContainer = function() {
		this.$TypeRegistrations = new (ss.makeGenericType(ss.Dictionary$2, [String, $JSLib_$Util_IocContainer$TypeRegistration]))();
		this.AutoRegistration = true;
		this.DefaultLifetime = 1;
		this.$debug_resolve = false;
	};
	$JSLib_Util_IocContainer.__typeName = 'JSLib.Util.IocContainer';
	global.JSLib.Util.IocContainer = $JSLib_Util_IocContainer;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Util.IocContainer.ObjectLifetime
	var $JSLib_Util_IocContainer$ObjectLifetime = function() {
	};
	$JSLib_Util_IocContainer$ObjectLifetime.__typeName = 'JSLib.Util.IocContainer$ObjectLifetime';
	global.JSLib.Util.IocContainer$ObjectLifetime = $JSLib_Util_IocContainer$ObjectLifetime;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Util.IsScrollExtension
	var $JSLib_Util_IsScrollExtension = function() {
	};
	$JSLib_Util_IsScrollExtension.__typeName = 'JSLib.Util.IsScrollExtension';
	$JSLib_Util_IsScrollExtension.CheckRefresh = function(iscroll) {
		if (ss.isNullOrUndefined(iscroll)) {
			return;
		}
		var check = iscroll.scrollerH !== iscroll.scroller.offsetHeight || iscroll.scrollerW !== iscroll.scroller.offsetWidth;
		if (check) {
			iscroll.wrapperH = iscroll.wrapper.clientHeight;
			iscroll.scrollerW = iscroll.scroller.offsetWidth;
			iscroll.scrollerH = iscroll.scroller.offsetHeight + iscroll.minScrollY;
			iscroll.maxScrollX = iscroll.wrapperW - iscroll.scrollerW;
			iscroll.maxScrollY = iscroll.wrapperH - iscroll.scrollerH + iscroll.minScrollY;
		}
	};
	global.JSLib.Util.IsScrollExtension = $JSLib_Util_IsScrollExtension;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Util.SizeCalculator
	var $JSLib_Util_SizeCalculator = function() {
	};
	$JSLib_Util_SizeCalculator.__typeName = 'JSLib.Util.SizeCalculator';
	$JSLib_Util_SizeCalculator.GetLeftHeight = function(data) {
		var totalHeight = 0;
		for (var $t1 = 0; $t1 < data.length; $t1++) {
			var item = data[$t1];
			totalHeight += item.outerHeight(true);
		}
		return window.innerHeight - totalHeight;
	};
	global.JSLib.Util.SizeCalculator = $JSLib_Util_SizeCalculator;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Util.URL
	var $JSLib_Util_URL = function() {
	};
	$JSLib_Util_URL.__typeName = 'JSLib.Util.URL';
	$JSLib_Util_URL.GetParamValue = function(paramName, queryString) {
		var query = '';
		if (!ss.isNullOrEmptyString(queryString)) {
			query = queryString;
		}
		else {
			query = window.location.search.substring(1);
		}
		var vars = query.split(String.fromCharCode(38));
		for (var $t1 = 0; $t1 < vars.length; $t1++) {
			var item = vars[$t1];
			var pair = item.split(String.fromCharCode(61));
			if (ss.referenceEquals(decodeURIComponent(pair[0]), paramName)) {
				return decodeURIComponent(pair[1]);
			}
		}
		return '';
	};
	$JSLib_Util_URL.GetAllExceptHash = function(url) {
		var href = window.location.href;
		if (!ss.isNullOrEmptyString(url)) {
			href = url;
		}
		var lastSharpIndex = href.lastIndexOf('#');
		var resultStr = '';
		if (lastSharpIndex !== -1) {
			resultStr = href.substring(0, lastSharpIndex);
		}
		else {
			resultStr = href;
		}
		return resultStr;
	};
	global.JSLib.Util.URL = $JSLib_Util_URL;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.WebSql.WebSqlExtension
	var $JSLib_WebSql_WebSqlExtension = function() {
	};
	$JSLib_WebSql_WebSqlExtension.__typeName = 'JSLib.WebSql.WebSqlExtension';
	$JSLib_WebSql_WebSqlExtension.ExecuteSQL = function(db, sql, parameters) {
		var tsk = new ss.TaskCompletionSource();
		db.transaction(function(transaction) {
			var taragetSql = sql;
			if (ss.isValue(parameters) && parameters.length > 0) {
				for (var i = 1; i <= parameters.length; i++) {
					taragetSql = ss.replaceAllString(taragetSql, '?' + i, "'" + parameters[i - 1] + "'");
				}
			}
			console.info('执行SQL:\r\n');
			console.info(taragetSql);
			transaction.executeSql(sql, parameters, function(sqlTransaction, results) {
				tsk.setResult(results);
			}, function(sqlTransaction1, error) {
				console.info(error);
				tsk.setResult(null);
			});
		}, function(transaction1, error1) {
			console.info(error1);
		}, function() {
		});
		return tsk.task;
	};
	$JSLib_WebSql_WebSqlExtension.Query = function(T) {
		return function(db, sql, parameters) {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1, result, data, i;
			var $sm = function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								$t1 = $JSLib_WebSql_WebSqlExtension.ExecuteSQL(db, sql, parameters);
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								result = $t1.getAwaitedResult();
								data = [];
								if (result.rows.length > 0) {
									for (i = 0; i < result.rows.length; i++) {
										data.push(result.rows.item(i));
									}
								}
								if (data.length === 0) {
									console.info('查询结果为空!');
								}
								$tcs.setResult(data);
								return;
							}
							default: {
								break $sm1;
							}
						}
					}
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			};
			$sm();
			return $tcs.task;
		};
	};
	$JSLib_WebSql_WebSqlExtension.QuerySingle = function(T) {
		return function(db, sql, parameters) {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1, result;
			var $sm = function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								$t1 = $JSLib_WebSql_WebSqlExtension.Query(T).call(null, db, sql, parameters);
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								result = $t1.getAwaitedResult();
								if (ss.isValue(result) && result.length > 0) {
									$tcs.setResult(result[0]);
									return;
								}
								else {
									$tcs.setResult(ss.getDefaultValue(T));
									return;
								}
							}
							default: {
								break $sm1;
							}
						}
					}
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			};
			$sm();
			return $tcs.task;
		};
	};
	$JSLib_WebSql_WebSqlExtension.QuerySingleValue = function(T) {
		return function(db, sql, parameters) {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1, result, target, dic;
			var $sm = function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								$t1 = $JSLib_WebSql_WebSqlExtension.Query(T).call(null, db, sql, parameters);
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								result = $t1.getAwaitedResult();
								if (ss.isValue(result) && result.length > 0) {
									target = result[0];
									dic = target;
									$tcs.setResult(dic[Object.keys(target)[0]]);
									return;
								}
								else {
									$tcs.setResult(ss.getDefaultValue(T));
									return;
								}
							}
							default: {
								break $sm1;
							}
						}
					}
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			};
			$sm();
			return $tcs.task;
		};
	};
	global.JSLib.WebSql.WebSqlExtension = $JSLib_WebSql_WebSqlExtension;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.BackArgs
	var $JSLib_Widget_BackArgs = function() {
		this.IsContinueGoBack = false;
	};
	$JSLib_Widget_BackArgs.__typeName = 'JSLib.Widget.BackArgs';
	global.JSLib.Widget.BackArgs = $JSLib_Widget_BackArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.IBackEvent
	var $JSLib_Widget_IBackEvent = function() {
	};
	$JSLib_Widget_IBackEvent.__typeName = 'JSLib.Widget.IBackEvent';
	global.JSLib.Widget.IBackEvent = $JSLib_Widget_IBackEvent;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Behavior.SortableBehavior
	var $JSLib_Widget_Behavior_SortableBehavior = function() {
	};
	$JSLib_Widget_Behavior_SortableBehavior.__typeName = 'JSLib.Widget.Behavior.SortableBehavior';
	$JSLib_Widget_Behavior_SortableBehavior.Sortable = function(parentContainer, childItemSelector) {
		if (ss.isNullOrEmptyString(childItemSelector)) {
			childItemSelector = '>*';
		}
		var currentDraging = null;
		//克隆一个子元素，用作占位符
		var simpleChildItem = $(parentContainer.children()[0]);
		var placeHolder = simpleChildItem.clone().empty();
		window.setTimeout(function() {
			placeHolder.height(simpleChildItem.height());
		}, 500);
		placeHolder.addClass('c-sortableplaceholder');
		parentContainer.on('dragstart', childItemSelector, ss.thisFix(function(elem, event) {
			if (ss.isValue(currentDraging)) {
				return;
			}
			var dataTransfer = event.originalEvent.dataTransfer;
			dataTransfer.effectAllowed = 'copyMove';
			dataTransfer.dropEffect = 'copy';
			currentDraging = $(elem);
			currentDraging.after(placeHolder);
			placeHolder.show();
		}));
		parentContainer.on('dragover', childItemSelector, ss.thisFix(function(elem1, event1) {
			event1.preventDefault();
			if (ss.isValue(currentDraging)) {
				currentDraging.hide();
				var target = $(elem1);
				if (!ss.referenceEquals(target, placeHolder)) {
					if (placeHolder.index() < target.index()) {
						target.after(placeHolder);
					}
					else {
						target.before(placeHolder);
					}
				}
			}
		}));
		parentContainer.on('drop dragend', childItemSelector, ss.thisFix(function(elem2, event2) {
			if (ss.isValue(currentDraging)) {
				placeHolder.before(currentDraging);
				placeHolder.detach().hide();
				currentDraging.show();
				currentDraging = null;
			}
		}));
		return parentContainer;
	};
	global.JSLib.Widget.Behavior.SortableBehavior = $JSLib_Widget_Behavior_SortableBehavior;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Common.CommonDialog
	var $JSLib_Widget_Common_CommonDialog = function() {
		this.Title = null;
		this.Message = null;
		this.$3$OnOKClickedField = null;
		this.$3$OnCanceledField = null;
		this.btnOK = null;
		this.btnCancel = null;
		$JSLib_Controller_ControllerBase.call(this);
		this.add_OnLoad(ss.mkdel(this, this.$CommonDialog_OnLoad));
		$JSLib_Mobile_NavigationManager.RegisterBackControl(this);
	};
	$JSLib_Widget_Common_CommonDialog.__typeName = 'JSLib.Widget.Common.CommonDialog';
	$JSLib_Widget_Common_CommonDialog.Confirm = function(title, message) {
		var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1, dialog, container, $t2, result;
		var $sm = function() {
			try {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							$t1 = new $JSLib_Widget_Common_CommonDialog();
							$t1.Title = title;
							$t1.Message = message;
							dialog = $t1;
							container = $JSLib_Controller_Panel.FromElement(document.body);
							$t2 = container.AddChild(dialog, true, false, null);
							$state = 1;
							$t2.continueWith($sm);
							return;
						}
						case 1: {
							$state = -1;
							$t2.getAwaitedResult();
							result = new ss.TaskCompletionSource();
							dialog.add_OnOKClicked(function(sender, args) {
								result.setResult(true);
								dialog.Destroy();
							});
							dialog.add_OnCanceled(function(sender1, args1) {
								result.setResult(false);
								dialog.Destroy();
							});
							$state = 2;
							result.task.continueWith($sm);
							return;
						}
						case 2: {
							$state = -1;
							$tcs.setResult(result.task.getAwaitedResult());
							return;
						}
						default: {
							break $sm1;
						}
					}
				}
			}
			catch ($t3) {
				$tcs.setException(ss.Exception.wrap($t3));
			}
		};
		$sm();
		return $tcs.task;
	};
	global.JSLib.Widget.Common.CommonDialog = $JSLib_Widget_Common_CommonDialog;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Common.DateRange
	var $JSLib_Widget_Common_DateRange = function() {
		this.BeginDate = null;
		this.EndDate = null;
		this.MinDate = null;
		this.MaxDate = null;
	};
	$JSLib_Widget_Common_DateRange.__typeName = 'JSLib.Widget.Common.DateRange';
	global.JSLib.Widget.Common.DateRange = $JSLib_Widget_Common_DateRange;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Common.DateRangeSelector
	var $JSLib_Widget_Common_DateRangeSelector = function(initData) {
		this.OnDateRrangeSelected = null;
		this.MinDate = new Date(1901, 1 - 1, 1);
		this.MaxDate = new Date(2999, 1 - 1, 1);
		this.pnlDateAreaFrom = null;
		this.lstYearFrom = null;
		this.lstMonthFrom = null;
		this.lstDayFrom = null;
		this.pnlDateAreaTo = null;
		this.lstYearTo = null;
		this.lstMonthTo = null;
		this.lstDayTo = null;
		$JSLib_Widget_Panel_ModalPanel.call(this);
		if (ss.isValue(initData)) {
			if (!ss.staticEquals(initData.MinDate, null)) {
				this.MinDate = ss.unbox(initData.MinDate);
			}
			if (!ss.staticEquals(initData.MaxDate, null)) {
				this.MaxDate = ss.unbox(initData.MaxDate);
			}
		}
		$JSLib_Widget_Panel_ModalPanel.prototype.set_ContainerClass.call(this, 'c-DateRangeSelectorContainer');
		this.add_OnLoad(ss.mkdel(this, function(sender, args) {
			this.lstMonthFrom.change(ss.mkdel(this, function(event) {
				var year = $CoreEx.ToInt(this.lstYearFrom.val(), 0);
				var month = $CoreEx.ToInt(this.lstMonthFrom.val(), 0);
				this.lstDayFrom.html(this.GetSelectItemsHtml(this.GetAvaiableDays(year, month)).toString());
				this.lstDayFrom.val('1');
			}));
			this.lstYearFrom.change(ss.mkdel(this, function(event1) {
				this.lstMonthFrom.val('1');
				this.lstMonthFrom.change();
			}));
			this.lstMonthTo.change(ss.mkdel(this, function(event2) {
				var year1 = $CoreEx.ToInt(this.lstYearTo.val(), 0);
				var month1 = $CoreEx.ToInt(this.lstMonthTo.val(), 0);
				this.lstDayTo.html(this.GetSelectItemsHtml(this.GetAvaiableDays(year1, month1)).toString());
				this.lstDayTo.val('1');
			}));
			this.lstYearTo.change(ss.mkdel(this, function(event3) {
				this.lstMonthTo.val('1');
				this.lstMonthTo.change();
			}));
			if (ss.isNullOrUndefined(initData)) {
				this.lstYearFrom.val((new Date()).getFullYear().toString());
				this.lstMonthFrom.val(((new Date()).getMonth() + 1).toString());
				this.lstMonthFrom.change();
				this.lstDayFrom.val((new Date()).getDate().toString());
				this.lstYearTo.val((new Date()).getFullYear().toString());
				this.lstMonthTo.val(((new Date()).getMonth() + 1).toString());
				this.lstMonthTo.change();
				this.lstDayTo.val((new Date()).getDate().toString());
			}
			else {
				this.lstYearFrom.val(ss.unbox(initData.BeginDate).getFullYear().toString());
				this.lstMonthFrom.val((ss.unbox(initData.BeginDate).getMonth() + 1).toString());
				this.lstMonthFrom.change();
				this.lstDayFrom.val(ss.unbox(initData.BeginDate).getDate().toString());
				this.lstYearTo.val(ss.unbox(initData.EndDate).getFullYear().toString());
				this.lstMonthTo.val((ss.unbox(initData.EndDate).getMonth() + 1).toString());
				this.lstMonthTo.change();
				this.lstDayTo.val(ss.unbox(initData.EndDate).getDate().toString());
			}
		}));
		$JSLib_Mobile_NavigationManager.RegisterBackControl(this);
		this.add_OnOKButtonClick(ss.mkdel(this, this.$DateRangeSelector_OnOKButtonClick));
	};
	$JSLib_Widget_Common_DateRangeSelector.__typeName = 'JSLib.Widget.Common.DateRangeSelector';
	$JSLib_Widget_Common_DateRangeSelector.ChooseDateRange = function(initData, parentContainer) {
		var tsk = new ss.TaskCompletionSource();
		var dateTimeSelector = new $JSLib_Widget_Common_DateRangeSelector(initData);
		var container = $JSLib_Controller_Panel.FromElement(parentContainer);
		dateTimeSelector.OnDateRrangeSelected = function(range) {
			tsk.setResult(range);
			dateTimeSelector.Destroy();
		};
		container.AddChild(dateTimeSelector, true, false, null);
		return tsk.task;
	};
	global.JSLib.Widget.Common.DateRangeSelector = $JSLib_Widget_Common_DateRangeSelector;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Common.DateTimeSelector
	var $JSLib_Widget_Common_DateTimeSelector = function(para) {
		this.CurrentDateTime = null;
		this.Para = null;
		this.$5$OnChooseDateTimeCompletedField = null;
		this.pnlDateArea = null;
		this.lstYear = null;
		this.lstMonth = null;
		this.lstDay = null;
		this.pnlTimeArea = null;
		this.lstHour = null;
		this.lstMinute = null;
		this.lstSecond = null;
		this.btnOK = null;
		this.btnClear = null;
		this.btnCancel = null;
		$JSLib_Widget_Panel_ModalPanel.call(this);
		this.Para = para;
		if (ss.isValue(para)) {
		}
		else {
			this.CurrentDateTime = new Date();
		}
		this.add_OnLoad(ss.mkdel(this, this.$DateTimeSelector_OnLoad));
		$JSLib_Mobile_NavigationManager.RegisterBackControl(this);
		this.ShowOKButton = false;
		this.ShowCancelButton = false;
	};
	$JSLib_Widget_Common_DateTimeSelector.__typeName = 'JSLib.Widget.Common.DateTimeSelector';
	$JSLib_Widget_Common_DateTimeSelector.ChooseDate = function(target, parentContainer) {
		var tsk = new ss.TaskCompletionSource();
		var dateTimeSelector = new $JSLib_Widget_Common_DateTimeSelector(target);
		var container = $JSLib_Controller_Panel.FromElement(parentContainer);
		dateTimeSelector.$5$OnChooseDateTimeCompletedField = function(sender, args) {
			tsk.setResult(dateTimeSelector.CurrentDateTime);
			dateTimeSelector.Destroy();
		};
		container.AddChild(dateTimeSelector, true, false, null);
		return tsk.task;
	};
	$JSLib_Widget_Common_DateTimeSelector.BindTo = function(ele) {
		$(ele).click(ss.thisFix(function(elem, event) {
			var $state = 0, target, dateTimeTarget, oriDate, $t1, result;
			var $sm = function() {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							target = $(ele);
							dateTimeTarget = new $JSLib_Widget_Common_DateTimeTarget();
							oriDate = $DateTimeEx.ToDateTime(target.val());
							dateTimeTarget.set_DefaultDate(oriDate);
							dateTimeTarget.ShowTime = false;
							dateTimeTarget.EnalbeClean = false;
							$t1 = $JSLib_Widget_Common_DateTimeSelector.ChooseDate(dateTimeTarget, document.body);
							$state = 1;
							$t1.continueWith($sm);
							return;
						}
						case 1: {
							$state = -1;
							result = $t1.getAwaitedResult();
							target.val(ss.formatDate(ss.unbox(result), 'yyyy-MM-dd'));
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			};
			$sm();
		}));
	};
	global.JSLib.Widget.Common.DateTimeSelector = $JSLib_Widget_Common_DateTimeSelector;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Common.DateTimeTarget
	var $JSLib_Widget_Common_DateTimeTarget = function() {
		this.$_MaxDate = null;
		this.$_MinDate = null;
		this.$_defaultDate = new Date();
		this.ShowDate = true;
		this.ShowTime = false;
		this.EnalbeClean = true;
	};
	$JSLib_Widget_Common_DateTimeTarget.__typeName = 'JSLib.Widget.Common.DateTimeTarget';
	global.JSLib.Widget.Common.DateTimeTarget = $JSLib_Widget_Common_DateTimeTarget;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Common.LoadingDialog
	var $JSLib_Widget_Common_LoadingDialog = function() {
		$JSLib_Controller_ControllerBase.call(this);
		this.add_OnLoad(ss.mkdel(this, this.$LoadingDialog_OnLoad));
	};
	$JSLib_Widget_Common_LoadingDialog.__typeName = 'JSLib.Widget.Common.LoadingDialog';
	global.JSLib.Widget.Common.LoadingDialog = $JSLib_Widget_Common_LoadingDialog;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Pager.PageEventArgs
	var $JSLib_Widget_Pager_PageEventArgs = function() {
		this.PrePageIndex = 0;
		this.CurrentPageIndex = 0;
		ss.EventArgs.call(this);
	};
	$JSLib_Widget_Pager_PageEventArgs.__typeName = 'JSLib.Widget.Pager.PageEventArgs';
	global.JSLib.Widget.Pager.PageEventArgs = $JSLib_Widget_Pager_PageEventArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Pager.SimplePager
	var $JSLib_Widget_Pager_SimplePager = function() {
		this.$1$OnPageIndexChangedField = null;
		this.$pageIndex = 0;
		this.$pageSize = 0;
		this.$1$TotalRecordCountField = 0;
		this.$rootElement = $("<div class='fh_mpage'>\r\n    <span class='page_all' id='lblPageAll'></span>\r\n        <a href='#' class='btn_pre' id='btnPrePage'> 上一页</a>\r\n        <a href='#' class='btn_next' id='btnNext'>下一页</a>\r\n</div>\r\n\r\n");
		this.lblPageAll = null;
		this.btnPrePage = null;
		this.btnNext = null;
		this.$1$OnLoadField = null;
		this.add_$OnLoad(ss.mkdel(this, this.$Control_Load));
	};
	$JSLib_Widget_Pager_SimplePager.__typeName = 'JSLib.Widget.Pager.SimplePager';
	global.JSLib.Widget.Pager.SimplePager = $JSLib_Widget_Pager_SimplePager;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Pager.WideSelectPager
	var $JSLib_Widget_Pager_WideSelectPager = function() {
		this.$1$OnPageIndexChangedField = null;
		this.$pageIndex = 0;
		this.$pageSize = 0;
		this.$totalRecordCount = 0;
		this.$rootElement = $("<div class='pg fh_mpage'>\r\n    <span class='page_all' id='lblPageAll'> ( <span data-en='Total' data-cn='共'>共</span> <span id='lblRecoredAll'> </span> <span data-cn='条' data-en='record'>个</span> )</span>\r\n        <a   class='pgBtn btn_pre' id='btnPrePage' data-en='Pre Page' data-cn='上一页'> 上一页</a>\r\n        <a  class='pgBtn' id='btnFirst'>1</a>\r\n        <span id='btnPreDot'>…</span>\r\n        <a  class='pgBtn' id='btnPre1'>8</a>\r\n        <a  class='pgBtn' id='btnPre2'>9</a>\r\n        <span class='curr' id='btnCurrent'> 10</span>\r\n        <a  class='pgBtn' id='btnNext1'>11</a>\r\n        <a  class='pgBtn' id='btnNext2'>12</a>\r\n        <span  id='btnNextDot'>…</span> \r\n        <a  class='pgBtn' id='btnEnd'></a>\r\n        <a  class='pgBtn btn_next' id='btnNext' data-en='Next Page' data-cn='下一页'>下一页</a>\r\n</div>\r\n\r\n");
		this.lblPageAll = null;
		this.lblRecoredAll = null;
		this.btnPrePage = null;
		this.btnFirst = null;
		this.btnPreDot = null;
		this.btnPre1 = null;
		this.btnPre2 = null;
		this.btnCurrent = null;
		this.btnNext1 = null;
		this.btnNext2 = null;
		this.btnNextDot = null;
		this.btnEnd = null;
		this.btnNext = null;
		this.$1$OnLoadField = null;
		this.add_$OnLoad(ss.mkdel(this, this.$Control_Load));
	};
	$JSLib_Widget_Pager_WideSelectPager.__typeName = 'JSLib.Widget.Pager.WideSelectPager';
	global.JSLib.Widget.Pager.WideSelectPager = $JSLib_Widget_Pager_WideSelectPager;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Panel.ModalPanel
	var $JSLib_Widget_Panel_ModalPanel = function() {
		this.CloseOnModalBackClick = true;
		this.ShowOKButton = false;
		this.ShowCancelButton = false;
		this.Title = null;
		this.$4$OnOKButtonClickField = null;
		this.$4$ContainerClassField = null;
		this.$4$OnModalWindowClickField = null;
		$JSLib_Controller_UIController.call(this);
		this.add_OnLoad(ss.mkdel(this, this.$ModalPanel_OnLoad));
	};
	$JSLib_Widget_Panel_ModalPanel.__typeName = 'JSLib.Widget.Panel.ModalPanel';
	global.JSLib.Widget.Panel.ModalPanel = $JSLib_Widget_Panel_ModalPanel;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.Panel.ModalPanelTemplate
	var $JSLib_Widget_Panel_ModalPanelTemplate = function() {
		$JSLib_Controller_RazorTemplate.call(this);
	};
	$JSLib_Widget_Panel_ModalPanelTemplate.__typeName = 'JSLib.Widget.Panel.ModalPanelTemplate';
	global.JSLib.Widget.Panel.ModalPanelTemplate = $JSLib_Widget_Panel_ModalPanelTemplate;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.SwipeView.SwipeView
	var $JSLib_Widget_SwipeView_SwipeView = function(target, showLoadingMessage) {
		this.$target = null;
		this.$1$OnSwipeThredEndField = null;
		this.$snapThreshold = 80;
		this.$lblMessage = null;
		this.$_showLoadingMessage = false;
		this.$target = target;
		var startX = 0;
		var startY = 0;
		var currentX = 0;
		var currentY = 0;
		var endX = 0;
		var endY = 0;
		var deltaX = 0;
		var deltaY = 0;
		var isStart = false;
		var lblMessage = null;
		this.$_showLoadingMessage = showLoadingMessage;
		if (this.$_showLoadingMessage) {
			lblMessage = $("<div style='position:absolute;top:20px;left:20px;z-index:2;background-color:black;color:white;padding:8px;'>松开即加载下一页</div>").appendTo($JSLib_Mobile_MobilePage.PageContainer).hide();
		}
		target.on('touchstart', ss.mkdel(this, function(event) {
			isStart = true;
			this.$target[0].style.webkitTransitionDuration = '0ms';
			startX = event.originalEvent.touches[0].pageX;
			startY = event.originalEvent.touches[0].pageY;
		}));
		target.on('touchmove', ss.mkdel(this, function(event1) {
			if (!isStart) {
				return;
			}
			currentX = event1.originalEvent.touches[0].pageX;
			currentY = event1.originalEvent.touches[0].pageY;
			deltaX = currentX - startX;
			deltaY = currentY - startY;
			if (Math.abs(deltaX) < Math.abs(deltaY)) {
				isStart = false;
				return;
			}
			this.$pos(deltaX);
			if (this.$_showLoadingMessage) {
				if (Math.abs(deltaX) >= this.$snapThreshold) {
					if (deltaX > 0) {
						lblMessage.text('松开即加载上一页');
					}
					else {
						lblMessage.text('松开即加载下一页');
					}
					lblMessage.show();
				}
				else {
					lblMessage.hide();
				}
			}
		}));
		target.on('touchend', ss.mkdel(this, function(event2) {
			var $state = 0, $t2, $t1, $t3, $t4;
			var $sm = ss.mkdel(this, function() {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							if (!isStart) {
								return;
							}
							if (this.$_showLoadingMessage) {
								lblMessage.hide();
							}
							endX = event2.originalEvent.changedTouches[0].pageX;
							deltaX = endX - startX;
							this.$target[0].style.webkitTransitionDuration = '500ms';
							if (Math.abs(deltaX) < this.$snapThreshold) {
								startX = 0;
								isStart = false;
								this.$pos(0);
								$state = -1;
								break $sm1;
							}
							else {
								startX = 0;
								isStart = false;
								this.$target[0].style.webkitTransitionDuration = '0ms';
								this.$pos(0);
								if (!ss.staticEquals(this.$1$OnSwipeThredEndField, null)) {
									$t2 = this.$1$OnSwipeThredEndField;
									$t1 = new $JSLib_Widget_SwipeView_SwipViewEventArgs();
									$t1.swipViewDirection = ((deltaX > 0) ? 1 : 2);
									$t2(this, $t1);
									if (this.$_showLoadingMessage) {
										this.$lblMessage = lblMessage.text('正在加载数据...').show();
										$t3 = ss.Task.delay(2000);
										$state = 1;
										$t3.continueWith($sm);
										return;
									}
									$state = -1;
									break $sm1;
								}
								$state = -1;
								break $sm1;
							}
						}
						case 1: {
							$state = -1;
							$t3.getAwaitedResult();
							$t4 = ss.Task.fromDoneCallback(lblMessage, 'hide', 1000);
							$state = 2;
							$t4.continueWith($sm);
							return;
						}
						case 2: {
							$state = -1;
							$t4.getAwaitedResult();
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			});
			$sm();
		}));
	};
	$JSLib_Widget_SwipeView_SwipeView.__typeName = 'JSLib.Widget.SwipeView.SwipeView';
	global.JSLib.Widget.SwipeView.SwipeView = $JSLib_Widget_SwipeView_SwipeView;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.SwipeView.SwipViewDirection
	var $JSLib_Widget_SwipeView_SwipViewDirection = function() {
	};
	$JSLib_Widget_SwipeView_SwipViewDirection.__typeName = 'JSLib.Widget.SwipeView.SwipViewDirection';
	global.JSLib.Widget.SwipeView.SwipViewDirection = $JSLib_Widget_SwipeView_SwipViewDirection;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.SwipeView.SwipViewEventArgs
	var $JSLib_Widget_SwipeView_SwipViewEventArgs = function() {
		this.swipViewDirection = 0;
		ss.EventArgs.call(this);
	};
	$JSLib_Widget_SwipeView_SwipViewEventArgs.__typeName = 'JSLib.Widget.SwipeView.SwipViewEventArgs';
	global.JSLib.Widget.SwipeView.SwipViewEventArgs = $JSLib_Widget_SwipeView_SwipViewEventArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.FixedInfiniteQuee
	var $JSLib_Widget_ViewPager_FixedInfiniteQuee$1 = function(T) {
		var $type = function(getData, havePrefetechData, maxLength) {
			this.$1$OnItemRemoveField = null;
			this.$1$OnItemAddField = null;
			this.$1$OnItemSetField = null;
			this.$1$OnCurrentDataChangeField = null;
			this.$1$OnCurPosChangeField = null;
			this.getData = null;
			this.havePrefetechData = null;
			this.data = [];
			this.$MaxLength = 3;
			this.curPos = 0;
			this.getData = getData;
			this.$MaxLength = maxLength;
			this.havePrefetechData = havePrefetechData;
		};
		ss.registerGenericClassInstance($type, $JSLib_Widget_ViewPager_FixedInfiniteQuee$1, [T], {
			InlitialData: function() {
				var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1, inlitialData, $t2, rightData, $t3, leftData, $t5, $t4, $t7, $t6;
				var $sm = ss.mkdel(this, function() {
					try {
						$sm1:
						for (;;) {
							switch ($state) {
								case 0: {
									$state = -1;
									//添加初始数据
									$t1 = this.getData(ss.getDefaultValue(T), 1);
									$state = 1;
									$t1.continueWith($sm);
									return;
								}
								case 1: {
									$state = -1;
									inlitialData = $t1.getAwaitedResult();
									this.Add(inlitialData, 1);
									//尝试向左或向右各添加一条数据
									$t2 = this.getData(inlitialData, 1);
									$state = 2;
									$t2.continueWith($sm);
									return;
								}
								case 2: {
									$state = -1;
									rightData = $t2.getAwaitedResult();
									if (ss.isValue(rightData)) {
										this.Add(rightData, 1);
									}
									$t3 = this.getData(inlitialData, -1);
									$state = 3;
									$t3.continueWith($sm);
									return;
								}
								case 3: {
									$state = -1;
									leftData = $t3.getAwaitedResult();
									if (ss.isValue(leftData)) {
										this.Add(leftData, -1);
										this.curPos++;
									}
									if (!ss.staticEquals(this.$1$OnCurrentDataChangeField, null)) {
										$t5 = this.$1$OnCurrentDataChangeField;
										$t4 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, [T]))();
										$t4.Data = inlitialData;
										$t4.gestureDirection = 1;
										$t5(this, $t4);
									}
									if (!ss.staticEquals(this.$1$OnCurPosChangeField, null)) {
										$t7 = this.$1$OnCurPosChangeField;
										$t6 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, [T]))();
										$t6.Data = inlitialData;
										$t6.gestureDirection = 1;
										$t7(this, $t6);
									}
									$state = -1;
									break $sm1;
								}
								default: {
									break $sm1;
								}
							}
						}
						$tcs.setResult(null);
					}
					catch ($t8) {
						$tcs.setException(ss.Exception.wrap($t8));
					}
				});
				$sm();
				return $tcs.task;
			},
			Add: function(target, gestureDirection) {
				var isRemovedData = false;
				//如果当前暂存数据已达到定义的最大大小，则移除反向方向的末端数据
				if (this.data.length >= this.$MaxLength) {
					var removed;
					var removedIndex = ((gestureDirection === 1) ? 0 : (this.data.length - 1));
					removed = this.data[removedIndex];
					ss.removeAt(this.data, removedIndex);
					if (!ss.staticEquals(this.$1$OnItemRemoveField, null)) {
						var $t2 = this.$1$OnItemRemoveField;
						var $t1 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, [T]))();
						$t1.Data = removed;
						$t1.gestureDirection = ((gestureDirection === 1) ? -1 : 1);
						$t2(this, $t1);
					}
					isRemovedData = true;
				}
				if (gestureDirection === 1) {
					this.data.push(target);
					if (!ss.staticEquals(this.$1$OnItemAddField, null)) {
						var $t4 = this.$1$OnItemAddField;
						var $t3 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, [T]))();
						$t3.Data = target;
						$t3.gestureDirection = 1;
						$t4(this, $t3);
					}
				}
				else {
					ss.insert(this.data, 0, target);
					if (!ss.staticEquals(this.$1$OnItemAddField, null)) {
						var $t6 = this.$1$OnItemAddField;
						var $t5 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, [T]))();
						$t5.Data = target;
						$t5.gestureDirection = -1;
						$t6(this, $t5);
					}
				}
				return isRemovedData;
			},
			MoveStep: function(gestureDirection) {
				//如果已到末端,则取消移动操作
				var isEnd = ((gestureDirection === 1) ? (this.curPos === this.data.length - 1) : (this.curPos === 0));
				if (isEnd) {
					return false;
				}
				//当前位置为紧邻末端一条数据时，预先加载该末端数据
				var isNearEnd = ((gestureDirection === 1) ? (this.curPos === this.data.length - 2) : (this.curPos === 1));
				//左边是否添加元素
				var isLeftAdd = false;
				var isAddAndReverseRemoved = false;
				var isAddData = false;
				if (isNearEnd) {
					var endIndex = ((gestureDirection === 1) ? (this.data.length - 1) : 0);
					try {
						isAddData = this.havePrefetechData(this.data[endIndex], gestureDirection);
					}
					catch ($t1) {
						var ex = ss.Exception.wrap($t1);
						return false;
					}
					if (isAddData) {
						var index = endIndex;
						var temptarget = this.data[endIndex];
						isAddAndReverseRemoved = this.Add(ss.getDefaultValue(T), gestureDirection);
						window.setTimeout(ss.mkdel(this, function() {
							var $state = 0, $t2, getedDataItem;
							var $sm = ss.mkdel(this, function() {
								$sm1:
								for (;;) {
									switch ($state) {
										case 0: {
											$state = -1;
											$t2 = this.getData(temptarget, gestureDirection);
											$state = 1;
											$t2.continueWith($sm);
											return;
										}
										case 1: {
											$state = -1;
											getedDataItem = $t2.getAwaitedResult();
											if (ss.isValue(getedDataItem)) {
												if (!isAddAndReverseRemoved && gestureDirection === 1) {
													this.$SetData(index + 1, getedDataItem);
												}
												else {
													this.$SetData(index, getedDataItem);
												}
											}
											$state = -1;
											break $sm1;
										}
										default: {
											break $sm1;
										}
									}
								}
							});
							$sm();
						}), 0);
					}
				}
				if (gestureDirection === 1) {
					this.curPos++;
				}
				else {
					this.curPos--;
				}
				//左边添加数据
				if (gestureDirection === -1 && isAddData) {
					this.curPos++;
				}
				//左边减少数据
				if (isAddAndReverseRemoved && gestureDirection === 1) {
					this.curPos--;
				}
				if (!ss.staticEquals(this.$1$OnCurPosChangeField, null)) {
					this.$1$OnCurPosChangeField(null, null);
				}
				//触发事件
				if (!ss.staticEquals(this.$1$OnCurrentDataChangeField, null)) {
					var $t5 = this.$1$OnCurrentDataChangeField;
					var $t4 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, [T]))();
					$t4.Data = this.data[this.curPos];
					$t4.gestureDirection = gestureDirection;
					$t5(this, $t4);
				}
				//PrintData();
				return true;
			},
			$SetData: function(index, getedDataItem) {
				this.data[index] = getedDataItem;
				if (!ss.staticEquals(this.$1$OnItemSetField, null)) {
					var $t2 = this.$1$OnItemSetField;
					var $t1 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, [T]))();
					$t1.index = index;
					$t1.Data = getedDataItem;
					$t2(null, $t1);
				}
			},
			get_Item: function(index) {
				return this.data[index];
			},
			add_OnItemRemove: function(value) {
				this.$1$OnItemRemoveField = ss.delegateCombine(this.$1$OnItemRemoveField, value);
			},
			remove_OnItemRemove: function(value) {
				this.$1$OnItemRemoveField = ss.delegateRemove(this.$1$OnItemRemoveField, value);
			},
			add_OnItemAdd: function(value) {
				this.$1$OnItemAddField = ss.delegateCombine(this.$1$OnItemAddField, value);
			},
			remove_OnItemAdd: function(value) {
				this.$1$OnItemAddField = ss.delegateRemove(this.$1$OnItemAddField, value);
			},
			add_OnItemSet: function(value) {
				this.$1$OnItemSetField = ss.delegateCombine(this.$1$OnItemSetField, value);
			},
			remove_OnItemSet: function(value) {
				this.$1$OnItemSetField = ss.delegateRemove(this.$1$OnItemSetField, value);
			},
			add_OnCurrentDataChange: function(value) {
				this.$1$OnCurrentDataChangeField = ss.delegateCombine(this.$1$OnCurrentDataChangeField, value);
			},
			remove_OnCurrentDataChange: function(value) {
				this.$1$OnCurrentDataChangeField = ss.delegateRemove(this.$1$OnCurrentDataChangeField, value);
			},
			add_OnCurPosChange: function(value) {
				this.$1$OnCurPosChangeField = ss.delegateCombine(this.$1$OnCurPosChangeField, value);
			},
			remove_OnCurPosChange: function(value) {
				this.$1$OnCurPosChangeField = ss.delegateRemove(this.$1$OnCurPosChangeField, value);
			},
			PrintData: function() {
				console.info('--开始打印数据:');
				for (var $t1 = 0; $t1 < this.data.length; $t1++) {
					var item = this.data[$t1];
					console.log(item);
				}
				console.info('--当前数据:');
				console.log(this.data[this.curPos]);
				console.info('--结束打印数据');
			}
		}, function() {
			return null;
		}, function() {
			return [];
		});
		return $type;
	};
	$JSLib_Widget_ViewPager_FixedInfiniteQuee$1.__typeName = 'JSLib.Widget.ViewPager.FixedInfiniteQuee$1';
	ss.initGenericClass($JSLib_Widget_ViewPager_FixedInfiniteQuee$1, $asm, 1);
	global.JSLib.Widget.ViewPager.FixedInfiniteQuee$1 = $JSLib_Widget_ViewPager_FixedInfiniteQuee$1;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.FixedInfiniteQuee.ItemArgs
	var $JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs = function(T) {
		var $type = function() {
			this.index = 0;
			this.Data = ss.getDefaultValue(T);
			this.gestureDirection = 0;
			ss.EventArgs.call(this);
		};
		ss.registerGenericClassInstance($type, $JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, [T], {}, function() {
			return ss.EventArgs;
		}, function() {
			return [];
		});
		return $type;
	};
	$JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs.__typeName = 'JSLib.Widget.ViewPager.FixedInfiniteQuee$1$ItemArgs';
	ss.initGenericClass($JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs, $asm, 1);
	global.JSLib.Widget.ViewPager.FixedInfiniteQuee$1$ItemArgs = $JSLib_Widget_ViewPager_FixedInfiniteQuee$1$ItemArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.FixedLengthDictionary
	var $JSLib_Widget_ViewPager_FixedLengthDictionary$2 = function(TKey, TValue) {
		var $type = function(maxCount) {
			this.$2$BeforeItemRemoveField = null;
			this.$2$OnItemAddField = null;
			this.$MaxCount = 3;
			this.$keyList = [];
			ss.makeGenericType(ss.Dictionary$2, [TKey, TValue]).call(this);
			this.$MaxCount = maxCount;
		};
		ss.registerGenericClassInstance($type, $JSLib_Widget_ViewPager_FixedLengthDictionary$2, [TKey, TValue], {
			add_BeforeItemRemove: function(value) {
				this.$2$BeforeItemRemoveField = ss.delegateCombine(this.$2$BeforeItemRemoveField, value);
			},
			remove_BeforeItemRemove: function(value) {
				this.$2$BeforeItemRemoveField = ss.delegateRemove(this.$2$BeforeItemRemoveField, value);
			},
			add_OnItemAdd: function(value) {
				this.$2$OnItemAddField = ss.delegateCombine(this.$2$OnItemAddField, value);
			},
			remove_OnItemAdd: function(value) {
				this.$2$OnItemAddField = ss.delegateRemove(this.$2$OnItemAddField, value);
			},
			AddItem: function(key, value, isTail) {
				if (this.containsKey(key) || ss.isNullOrUndefined(value)) {
					return false;
				}
				if (this.get_count() >= this.$MaxCount + 1) {
					if (isTail) {
						var removeKey = Enumerable.from(this.$keyList).first();
						ss.remove(this.$keyList, removeKey);
						if (!ss.staticEquals(this.$2$BeforeItemRemoveField, null)) {
							var $t2 = this.$2$BeforeItemRemoveField;
							var $t1 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedLengthDictionary$2$ItemArgs, [TKey, TValue]))();
							$t1.key = key;
							$t1.value = this.get_item(removeKey);
							$t1.IsTail = false;
							$t2(this, $t1);
						}
						this.remove(removeKey);
					}
					else {
						var removeKey1 = Enumerable.from(this.$keyList).last();
						ss.remove(this.$keyList, removeKey1);
						if (!ss.staticEquals(this.$2$BeforeItemRemoveField, null)) {
							var $t4 = this.$2$BeforeItemRemoveField;
							var $t3 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedLengthDictionary$2$ItemArgs, [TKey, TValue]))();
							$t3.key = key;
							$t3.value = this.get_item(removeKey1);
							$t3.IsTail = true;
							$t4(this, $t3);
						}
						this.remove(removeKey1);
					}
				}
				if (isTail) {
					this.$keyList.push(key);
				}
				else {
					ss.insert(this.$keyList, 0, key);
				}
				this.set_item(key, value);
				if (!ss.staticEquals(this.$2$OnItemAddField, null)) {
					var $t6 = this.$2$OnItemAddField;
					var $t5 = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedLengthDictionary$2$ItemArgs, [TKey, TValue]))();
					$t5.key = key;
					$t5.value = value;
					$t5.IsTail = isTail;
					$t6(this, $t5);
				}
				return true;
			}
		}, function() {
			return ss.makeGenericType(ss.Dictionary$2, [TKey, TValue]);
		}, function() {
			return [ss.IEnumerable, ss.IEnumerable, ss.IDictionary];
		});
		return $type;
	};
	$JSLib_Widget_ViewPager_FixedLengthDictionary$2.__typeName = 'JSLib.Widget.ViewPager.FixedLengthDictionary$2';
	ss.initGenericClass($JSLib_Widget_ViewPager_FixedLengthDictionary$2, $asm, 2);
	global.JSLib.Widget.ViewPager.FixedLengthDictionary$2 = $JSLib_Widget_ViewPager_FixedLengthDictionary$2;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.FixedLengthDictionary.ItemArgs
	var $JSLib_Widget_ViewPager_FixedLengthDictionary$2$ItemArgs = function(TKey, TValue) {
		var $type = function() {
			this.IsTail = false;
			this.key = ss.getDefaultValue(TKey);
			this.value = ss.getDefaultValue(TValue);
			ss.EventArgs.call(this);
		};
		ss.registerGenericClassInstance($type, $JSLib_Widget_ViewPager_FixedLengthDictionary$2$ItemArgs, [TKey, TValue], {}, function() {
			return ss.EventArgs;
		}, function() {
			return [];
		});
		return $type;
	};
	$JSLib_Widget_ViewPager_FixedLengthDictionary$2$ItemArgs.__typeName = 'JSLib.Widget.ViewPager.FixedLengthDictionary$2$ItemArgs';
	ss.initGenericClass($JSLib_Widget_ViewPager_FixedLengthDictionary$2$ItemArgs, $asm, 2);
	global.JSLib.Widget.ViewPager.FixedLengthDictionary$2$ItemArgs = $JSLib_Widget_ViewPager_FixedLengthDictionary$2$ItemArgs;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.IViewPagerItem
	var $JSLib_Widget_ViewPager_IViewPagerItem = function() {
	};
	$JSLib_Widget_ViewPager_IViewPagerItem.__typeName = 'JSLib.Widget.ViewPager.IViewPagerItem';
	global.JSLib.Widget.ViewPager.IViewPagerItem = $JSLib_Widget_ViewPager_IViewPagerItem;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.LateBindViewPager
	var $JSLib_Widget_ViewPager_LateBindViewPager$1 = function(T) {
		var $type = function(getData, haveprefetchData, snapThreshold, maxPageCount) {
			ss.makeGenericType($JSLib_Widget_ViewPager_ViewPager$1, [T]).call(this, getData, haveprefetchData, snapThreshold, maxPageCount);
		};
		ss.registerGenericClassInstance($type, $JSLib_Widget_ViewPager_LateBindViewPager$1, [T], {
			InlitialData: function() {
				var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1;
				var $sm = ss.mkdel(this, function() {
					try {
						$sm1:
						for (;;) {
							switch ($state) {
								case 0: {
									$state = -1;
									$t1 = ss.makeGenericType($JSLib_Widget_ViewPager_ViewPager$1, [T]).prototype.InlitialData.call(this);
									$state = 1;
									$t1.continueWith($sm);
									return;
								}
								case 1: {
									$state = -1;
									$t1.getAwaitedResult();
									$state = -1;
									break $sm1;
								}
								default: {
									break $sm1;
								}
							}
						}
						$tcs.setResult(null);
					}
					catch ($t2) {
						$tcs.setException(ss.Exception.wrap($t2));
					}
				});
				$sm();
				return $tcs.task;
			}
		}, function() {
			return ss.makeGenericType($JSLib_Widget_ViewPager_ViewPager$1, [T]);
		}, function() {
			return [];
		});
		return $type;
	};
	$JSLib_Widget_ViewPager_LateBindViewPager$1.__typeName = 'JSLib.Widget.ViewPager.LateBindViewPager$1';
	ss.initGenericClass($JSLib_Widget_ViewPager_LateBindViewPager$1, $asm, 1);
	global.JSLib.Widget.ViewPager.LateBindViewPager$1 = $JSLib_Widget_ViewPager_LateBindViewPager$1;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.PageChanged
	var $JSLib_Widget_ViewPager_PageChanged$1 = function(T) {
		var $type = function() {
			this.Data = ss.getDefaultValue(T);
			ss.EventArgs.call(this);
		};
		ss.registerGenericClassInstance($type, $JSLib_Widget_ViewPager_PageChanged$1, [T], {}, function() {
			return ss.EventArgs;
		}, function() {
			return [];
		});
		return $type;
	};
	$JSLib_Widget_ViewPager_PageChanged$1.__typeName = 'JSLib.Widget.ViewPager.PageChanged$1';
	ss.initGenericClass($JSLib_Widget_ViewPager_PageChanged$1, $asm, 1);
	global.JSLib.Widget.ViewPager.PageChanged$1 = $JSLib_Widget_ViewPager_PageChanged$1;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.ViewPageItem
	var $JSLib_Widget_ViewPager_ViewPageItem = function(pageIndex, getPageDom) {
		ss.makeGenericType($JSLib_Widget_ViewPager_ViewPageItem$1, [Object]).call(this, pageIndex, getPageDom);
	};
	$JSLib_Widget_ViewPager_ViewPageItem.__typeName = 'JSLib.Widget.ViewPager.ViewPageItem';
	$JSLib_Widget_ViewPager_ViewPageItem.$ctor1 = function(data, getPageDomByData) {
		ss.makeGenericType($JSLib_Widget_ViewPager_ViewPageItem$1, [Object]).$ctor1.call(this, data, getPageDomByData);
	};
	global.JSLib.Widget.ViewPager.ViewPageItem = $JSLib_Widget_ViewPager_ViewPageItem;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.ViewPageItem
	var $JSLib_Widget_ViewPager_ViewPageItem$1 = function(T) {
		var $type = function(pageIndex, getPageDom) {
			this.pageDom = null;
			this.PageIndex = 0;
			this.PageData = ss.getDefaultValue(T);
			this.pageDom = getPageDom(pageIndex);
		};
		$type.$ctor1 = function(data, getPageDomByData) {
			this.pageDom = null;
			this.PageIndex = 0;
			this.PageData = ss.getDefaultValue(T);
			this.PageData = data;
			this.pageDom = getPageDomByData(data);
		};
		ss.registerGenericClassInstance($type, $JSLib_Widget_ViewPager_ViewPageItem$1, [T], {
			GetController: function() {
				return this.pageDom;
			},
			Dispose: function() {
				this.pageDom.unbind();
				this.pageDom.remove();
				this.pageDom = null;
			}
		}, function() {
			return null;
		}, function() {
			return [$JSLib_Widget_ViewPager_IViewPagerItem];
		});
		$type.$ctor1.prototype = $type.prototype;
		return $type;
	};
	$JSLib_Widget_ViewPager_ViewPageItem$1.__typeName = 'JSLib.Widget.ViewPager.ViewPageItem$1';
	ss.initGenericClass($JSLib_Widget_ViewPager_ViewPageItem$1, $asm, 1);
	global.JSLib.Widget.ViewPager.ViewPageItem$1 = $JSLib_Widget_ViewPager_ViewPageItem$1;
	////////////////////////////////////////////////////////////////////////////////
	// JSLib.Widget.ViewPager.ViewPager
	var $JSLib_Widget_ViewPager_ViewPager$1 = function(T) {
		var $type = function(getData, haveprefetchData, snapThreshold, maxPageCount) {
			this.ViewPageDic = null;
			this.$getData = null;
			this.$_havePrefetchData = null;
			this.$4$OnPageChangedField = null;
			this.$ulList = null;
			this.$deltaX = 0;
			this.$deltaY = 0;
			this.containerWidth = 0;
			this.transX = 0;
			this._maxPageCount = 0;
			this.$padddingLeft = 0;
			this.$CurPostion = 0;
			this.$_snapThreshold = 0;
			$JSLib_Controller_UIController.call(this);
			this._maxPageCount = maxPageCount;
			this.$getData = getData;
			this.$_havePrefetchData = haveprefetchData || function() {
				return true;
			};
			this.$_snapThreshold = snapThreshold;
			this.add_OnLoad(ss.mkdel(this, this.$ViewPager_OnLoad));
		};
		ss.registerGenericClassInstance($type, $JSLib_Widget_ViewPager_ViewPager$1, [T], {
			add_OnPageChanged: function(value) {
				this.$4$OnPageChangedField = ss.delegateCombine(this.$4$OnPageChangedField, value);
			},
			remove_OnPageChanged: function(value) {
				this.$4$OnPageChangedField = ss.delegateRemove(this.$4$OnPageChangedField, value);
			},
			get_$IsRightToLeft: function() {
				return this.$deltaX < 0;
			},
			Reload: function() {
				var $state = 0, $t1;
				var $sm = ss.mkdel(this, function() {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								this.$ulList.empty();
								$t1 = this.InlitialData();
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								$t1.getAwaitedResult();
								$state = -1;
								break $sm1;
							}
							default: {
								break $sm1;
							}
						}
					}
				});
				$sm();
			},
			BuildController: function() {
				var $state = 0, $tcs = new ss.TaskCompletionSource(), task;
				var $sm = ss.mkdel(this, function() {
					try {
						$sm1:
						for (;;) {
							switch ($state) {
								case 0: {
									$state = -1;
									task = this.BeforeLoad();
									if (ss.isValue(task)) {
										$state = 2;
										task.continueWith($sm);
										return;
									}
									$state = 1;
									continue $sm1;
								}
								case 2: {
									$state = -1;
									task.getAwaitedResult();
									$state = 1;
									continue $sm1;
								}
								case 1: {
									$state = -1;
									this.Content = $('<div></div>');
									$state = -1;
									break $sm1;
								}
								default: {
									break $sm1;
								}
							}
						}
						$tcs.setResult(null);
					}
					catch ($t1) {
						$tcs.setException(ss.Exception.wrap($t1));
					}
				});
				$sm();
				return $tcs.task;
			},
			$ViewPager_OnLoad: function(sender, e) {
				var $state = 0, startX, startY, currentX, currentY, endX, isStart, $t1;
				var $sm = ss.mkdel(this, function() {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								startX = 0;
								startY = 0;
								currentX = 0;
								currentY = 0;
								endX = 0;
								isStart = false;
								//滑动多长的距离后进行翻页
								this.transX = 0;
								this.containerWidth = window.innerWidth;
								this.Content.css('min-width', this.containerWidth * this._maxPageCount + 'px');
								this.Content.on('touchstart', ss.mkdel(this, function(event) {
									isStart = true;
									this.Content[0].style.webkitTransitionDuration = '0ms';
									startX = event.originalEvent.touches[0].pageX;
									startY = event.originalEvent.touches[0].pageY;
								}));
								this.Content.on('touchmove', ss.mkdel(this, function(event1) {
									if (!isStart) {
										return;
									}
									event1.preventDefault();
									currentX = event1.originalEvent.touches[0].pageX;
									currentY = event1.originalEvent.touches[0].pageY;
									this.$deltaX = currentX - startX;
									this.$deltaY = currentY - startY;
									//如果主要是上下滑动，则取消左右的翻页效果
									if (Math.abs(this.$deltaX) < Math.abs(this.$deltaY)) {
										//isStart = false;
										return;
									}
									this.TranslateTo(this.$CurPostion + this.$deltaX, 0);
								}));
								this.Content.on('touchend', ss.mkdel(this, function(event2) {
									if (!isStart) {
										return;
									}
									endX = event2.originalEvent.changedTouches[0].pageX;
									this.$deltaX = endX - startX;
									//位移量不足，取消翻到下一页
									if (Math.abs(this.$deltaX) < this.$_snapThreshold) {
										startX = 0;
										isStart = false;
										this.TranslateTo(this.$CurPostion, 500);
										return;
									}
									startX = 0;
									isStart = false;
									var gestureDirection = (this.get_$IsRightToLeft() ? 1 : -1);
									if (this.ViewPageDic.MoveStep(gestureDirection)) {
										this.UpdateElementClass();
									}
									//TranslateTo(CurPostion);
									window.setTimeout(ss.mkdel(this, function() {
										this.TranslateTo(this.$CurPostion, 500);
									}), 100);
								}));
								$t1 = this.InlitialData();
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								$t1.getAwaitedResult();
								$state = -1;
								break $sm1;
							}
							default: {
								break $sm1;
							}
						}
					}
				});
				$sm();
			},
			InlitialData: function() {
				var $state = 0, $tcs = new ss.TaskCompletionSource(), $t3;
				var $sm = ss.mkdel(this, function() {
					try {
						$sm1:
						for (;;) {
							switch ($state) {
								case 0: {
									$state = -1;
									this.$ulList = $(ss.formatString("<ul class='c-ViewPager' style='min-width:{0}px'></ul>", this.containerWidth * this._maxPageCount));
									this.ViewPageDic = new (ss.makeGenericType($JSLib_Widget_ViewPager_FixedInfiniteQuee$1, [T]))(this.$getData, this.$_havePrefetchData, this._maxPageCount);
									this.ViewPageDic.add_OnCurPosChange(ss.mkdel(this, function() {
										this.$CurPostion = this.ViewPageDic.curPos * this.containerWidth * -1;
									}));
									this.ViewPageDic.add_OnItemRemove(ss.mkdel(this, function(sender, args) {
										if (args.gestureDirection === -1) {
											//padddingLeft += containerWidth;
											//ulList[0].Style.PaddingLeft = padddingLeft.px();
										}
										args.Data.Dispose();
										if (args.gestureDirection === 1) {
											this.$ulList.children().last().unbind().remove();
										}
										else {
											this.$ulList.children().first().unbind().remove();
										}
									}));
									this.ViewPageDic.add_OnItemAdd(ss.mkdel(this, function(sender1, args1) {
										var li;
										if (ss.isNullOrUndefined(args1.Data)) {
											li = this.$WarpDataItem($('<div>加载中...</div>'));
										}
										else {
											li = this.$WarpDataItem(args1.Data.GetController());
										}
										if (args1.gestureDirection === 1) {
											this.$ulList.append(li);
										}
										else {
											this.$ulList.prepend(li);
										}
									}));
									this.ViewPageDic.add_OnItemSet(ss.mkdel(this, function(sender2, args2) {
										this.$ulList.children().each(function(index, element) {
											if (index === args2.index) {
												$(element).html(args2.Data.GetController());
											}
										});
									}));
									this.ViewPageDic.add_OnCurrentDataChange(ss.mkdel(this, function(sender3, args3) {
										this.UpdateElementClass();
										this.$CurPostion = this.ViewPageDic.curPos * this.containerWidth * -1;
										this.TranslateTo(this.$CurPostion, 0);
										if (!ss.staticEquals(this.$4$OnPageChangedField, null)) {
											var $t2 = this.$4$OnPageChangedField;
											var $t1 = new (ss.makeGenericType($JSLib_Widget_ViewPager_PageChanged$1, [T]))();
											$t1.Data = args3.Data;
											$t2(this, $t1);
										}
										this.ScrollToTop();
									}));
									$t3 = this.ViewPageDic.InlitialData();
									$state = 1;
									$t3.continueWith($sm);
									return;
								}
								case 1: {
									$state = -1;
									$t3.getAwaitedResult();
									this.$ulList.appendTo(this.Content);
									$state = -1;
									break $sm1;
								}
								default: {
									break $sm1;
								}
							}
						}
						$tcs.setResult(null);
					}
					catch ($t4) {
						$tcs.setException(ss.Exception.wrap($t4));
					}
				});
				$sm();
				return $tcs.task;
			},
			$WarpDataItem: function(itemController) {
				var li = $('<li>');
				li.append(itemController);
				li.width(this.containerWidth);
				return li;
			},
			TranslateTo: function(x, duration) {
				this.Content[0].style.webkitTransform = 'translate3d(' + x + 'px,0,0)';
				this.Content[0].style.webkitTransitionDuration = duration + 'ms';
			},
			UpdateElementClass: function() {
				var curItemClass = 'c-vpi-cur';
				this.$ulList.children('li').removeClass();
				var curItem = this.$ulList.find(ss.formatString('li:nth-child({0})', this.ViewPageDic.curPos + 1)).addClass(curItemClass);
				curItem.prev().addClass('c-vpi-pre');
				curItem.next().addClass('c-vpi-next');
			},
			GetCurrentPageIndex: function() {
				return this.ViewPageDic.curPos;
			},
			GetCurrentPageData: function() {
				return this.ViewPageDic.get_Item(this.ViewPageDic.curPos);
			},
			MoveStep: function(direciton) {
				this.ViewPageDic.MoveStep(direciton);
			},
			ScrollToTop: function() {
				this.Content[0].style.overflow = 'hidden';
				//this.ParentContainer[0].ScrollTop = 0;
				this.Content.scrollTop(0);
				this.Content[0].style.overflowX = 'scroll';
			},
			Destroy: function() {
				if (ss.isValue(this.ViewPageDic) && ss.isValue(this.ViewPageDic.data) && this.ViewPageDic.data.length > 0) {
					for (var $t1 = 0; $t1 < this.ViewPageDic.data.length; $t1++) {
						var item = this.ViewPageDic.data[$t1];
						item.Dispose();
					}
				}
				this.ViewPageDic = null;
				this.$CurPostion = 0;
				this.Content.empty();
				this.Content.unbind();
				$JSLib_Controller_ControllerBase.prototype.Destroy.call(this);
			}
		}, function() {
			return $JSLib_Controller_UIController;
		}, function() {
			return [];
		});
		$type.TranslateDuration = 500;
		return $type;
	};
	$JSLib_Widget_ViewPager_ViewPager$1.__typeName = 'JSLib.Widget.ViewPager.ViewPager$1';
	ss.initGenericClass($JSLib_Widget_ViewPager_ViewPager$1, $asm, 1);
	global.JSLib.Widget.ViewPager.ViewPager$1 = $JSLib_Widget_ViewPager_ViewPager$1;
	////////////////////////////////////////////////////////////////////////////////
	// Phone.RazorMobilePage
	var $Phone_RazorMobilePage = function() {
		this.$3$PrepareErrroInfoField = null;
		this.$LastPageData = null;
		this.$3$IsDataChangedField = false;
		$JSLib_Mobile_BasePage.call(this);
		$JSLib_Mobile_MobilePage.showLoadingMessage = false;
		this.add_OnPageBeforeShow(ss.mkdel(this, function(sender, args) {
			this.set_IsDataChanged(!ss.referenceEquals(this.$LastPageData, this.InitialData));
			this.$LastPageData = this.InitialData;
		}));
		$JSLib_Mobile_MobilePage.transtionMethod = 'slide';
	};
	$Phone_RazorMobilePage.__typeName = 'Phone.RazorMobilePage';
	global.Phone.RazorMobilePage = $Phone_RazorMobilePage;
	////////////////////////////////////////////////////////////////////////////////
	// System.ComponentModel.DataAnnotations.Schema.NoClass
	var $System_ComponentModel_DataAnnotations_Schema_NoClass = function() {
	};
	$System_ComponentModel_DataAnnotations_Schema_NoClass.__typeName = 'System.ComponentModel.DataAnnotations.Schema.NoClass';
	global.System.ComponentModel.DataAnnotations.Schema.NoClass = $System_ComponentModel_DataAnnotations_Schema_NoClass;
	////////////////////////////////////////////////////////////////////////////////
	// System.Web.WebPages.HelperResult
	var $System_Web_WebPages_HelperResult = function(action) {
		this.sb = new ss.StringBuilder();
		action(this.sb);
	};
	$System_Web_WebPages_HelperResult.__typeName = 'System.Web.WebPages.HelperResult';
	global.System.Web.WebPages.HelperResult = $System_Web_WebPages_HelperResult;
	ss.initClass($BasePageEx, $asm, {});
	ss.initClass($ConsoleEx, $asm, {});
	ss.initClass($ControllerBaseEx, $asm, {});
	ss.initClass($CoreEx, $asm, {});
	ss.initClass($CssEx, $asm, {});
	ss.initClass($CssGeter, $asm, {});
	ss.initClass($DateTimeEx, $asm, {});
	ss.initClass($DateTimeExtension, $asm, {});
	ss.initClass($DebugEx, $asm, {});
	ss.initClass($ElementEx, $asm, {});
	ss.initClass($EnumImport, $asm, {});
	ss.initClass($FileReaderUtil, $asm, {});
	ss.initClass($FlagEnumExtension, $asm, {});
	ss.initClass($ForeignKeyAttribute, $asm, {});
	ss.initClass($FromBodyAttribute, $asm, {});
	ss.initClass($GenerateInfoAttribute, $asm, {
		get_ClassName: function() {
			return this.$2$ClassNameField;
		},
		set_ClassName: function(value) {
			this.$2$ClassNameField = value;
		},
		get_BaseClassName: function() {
			return this.$2$BaseClassNameField;
		},
		set_BaseClassName: function(value) {
			this.$2$BaseClassNameField = value;
		},
		get_Using: function() {
			return this.$2$UsingField;
		},
		set_Using: function(value) {
			this.$2$UsingField = value;
		},
		get_NameSpace: function() {
			return this.$2$NameSpaceField;
		},
		set_NameSpace: function(value) {
			this.$2$NameSpaceField = value;
		}
	});
	ss.initClass($HttpPostAttribute, $asm, {});
	ss.initClass($IScrollEx, $asm, {});
	ss.initClass($jQueryObjectEx, $asm, {});
	ss.initClass($KeyAttribute, $asm, {});
	ss.initClass($NotMappedAttribute, $asm, {});
	ss.initClass($ObjectEx, $asm, {});
	ss.initClass($RequiredAttribute, $asm, {});
	ss.initClass($StringHelper, $asm, {});
	ss.initClass($StringLengthAttribute, $asm, {});
	ss.initClass($TableAttribute, $asm, {});
	ss.initClass($UIDataEx, $asm, {});
	ss.initClass($JSLib_CommonService, $asm, {
		GetCurrentPosition: function(baiduMapKey, showTurnOnGPSPrompt, showLoading) {
			if (showLoading) {
				$JSLib_Mobile_Application.ShowLoading();
			}
			var tsk = new ss.TaskCompletionSource();
			if ($JSLib_Mobile_Application.DeviceType === 1 || $JSLib_Mobile_Application.IsBrowser) {
				var point = null;
				window.navigator.geolocation.getCurrentPosition(function(location) {
					var $state = 0, $t1, result;
					var $sm = function() {
						$sm1:
						for (;;) {
							switch ($state) {
								case 0: {
									$state = -1;
									$JSLib_Mobile_Application.HideLoading();
									$t1 = (new $JSLib_Map_BaiduGeocodingService()).GetLocationAddress(baiduMapKey, location.coords.longitude, location.coords.latitude);
									$state = 1;
									$t1.continueWith($sm);
									return;
								}
								case 1: {
									$state = -1;
									result = $t1.getAwaitedResult();
									point = new $JSLib_MapPoint.$ctor1(location.coords.longitude, location.coords.latitude, result, '');
									tsk.setResult(point);
									$state = -1;
									break $sm1;
								}
								default: {
									break $sm1;
								}
							}
						}
					};
					$sm();
				}, function(error) {
					if ($JSLib_Mobile_Application.DeviceType !== 10 && showTurnOnGPSPrompt) {
						$JSLib_Mobile_MessageBox.Alert('获取位置失败，请在设置中打开GPS，并允许本软件使用GPS!', '警告');
					}
					$JSLib_Mobile_Application.HideLoading();
					tsk.setResult(point);
				}, { timeout: 10000 });
			}
			else if (!$JSLib_Mobile_Application.IsBrowser && $JSLib_Mobile_Application.DeviceType === 10) {
				cordova.exec(function(o) {
					var $state1 = 0, point1;
					var $sm1 = function() {
						$sm1:
						for (;;) {
							switch ($state1) {
								case 0: {
									$state1 = -1;
									$JSLib_Mobile_Application.HideLoading();
									point1 = o;
									if (ss.isNullOrUndefined(point1)) {
										$JSLib_Mobile_MessageBox.Alert('获取位置失败，请在设置中打开GPS，并允许本软件使用GPS!', '警告');
										return;
									}
									tsk.setResult(point1);
									$state1 = -1;
									break $sm1;
								}
								default: {
									break $sm1;
								}
							}
						}
					};
					$sm1();
				}, function(o1) {
					$JSLib_Mobile_Application.HideLoading();
					tsk.setResult(null);
				}, 'BaiduMapPlugin', 'GetCurrentPosition', []);
			}
			return tsk.task;
		},
		StartMapApp: function(point, AppName) {
			//Window.Location.Href = "tel:" + No;
			var anchor = document.createElement('a');
			var addr = $JSLib_CommonService.GenerateNatvieMapAppUrl(point, AppName);
			anchor.setAttribute('href', addr);
			anchor.setAttribute('target', '_self');
			var dispatch = document.createEvent('HTMLEvents');
			dispatch.initEvent('click', true, true);
			anchor.dispatchEvent(dispatch);
			return addr;
		}
	});
	ss.initClass($JSLib_Device, $asm, {});
	ss.initClass($JSLib_JSLibConfig, $asm, {});
	ss.initClass($JSLib_MapPoint, $asm, {});
	$JSLib_MapPoint.$ctor1.prototype = $JSLib_MapPoint.prototype;
	ss.initClass($JSLib_SingleTask, $asm, {});
	ss.initClass($JSLib_TakePictureResult, $asm, {
		get_FileURIData: function() {
			return 'data:image/x-jpeg;base64,' + this.base64Data;
		}
	});
	ss.initClass($JSLib_TaskEx, $asm, {});
	ss.initClass($JSLib_TaskQuee, $asm, {});
	ss.initClass($JSLib_WebData, $asm, {});
	ss.initClass($JSLib_$Util_IocContainer$TypeRegistration, $asm, {
		get_$IsSingleton: function() {
			return this.$isSingleton;
		},
		get_$ImplementationType: function() {
			return this.$implementationType;
		},
		get_$ImplementationInstance: function() {
			return this.$implementationInstance;
		},
		set_$ImplementationInstance: function(value) {
			this.$implementationInstance = value;
		},
		get_$Constructor: function() {
			return this.$constructor;
		}
	});
	$JSLib_$Util_IocContainer$TypeRegistration.$ctor2.prototype = $JSLib_$Util_IocContainer$TypeRegistration.$ctor1.prototype = $JSLib_$Util_IocContainer$TypeRegistration.prototype;
	ss.initClass($JSLib_Mobile_UIBase, $asm, {
		add_OnOrientationChange: function(value) {
			this.$1$OnOrientationChangeField = ss.delegateCombine(this.$1$OnOrientationChangeField, value);
		},
		remove_OnOrientationChange: function(value) {
			this.$1$OnOrientationChangeField = ss.delegateRemove(this.$1$OnOrientationChangeField, value);
		},
		Execute: function() {
		},
		WriteLiteral: function(content) {
			this.contentBuilder.append(content);
		},
		Write: function(content) {
			this.contentBuilder.append(content);
		},
		WriteLiteralTo: function(textWriter, content) {
			textWriter.append(content);
		},
		WriteTo: function(textWriter, content) {
			if (ss.isNullOrUndefined(content)) {
				textWriter.append('');
			}
			else {
				textWriter.append(content);
			}
		},
		GetHtmlContent: function() {
			this.Execute();
			return this.contentBuilder.toString();
		},
		InitialComponent: function() {
		},
		DestoryComponent: function() {
		},
		SetScrollable: function(ele, showScrollBar, vScroll, hScroll, onScrollStart, onScrollMove, onScrollEnd, containerClassName, enablePullDownToRefresh, refreshAction, enableBounce, enableMomentum) {
			var container = new $JSLib_IScrollWraper_IScrollContainer.$ctor1(ele, showScrollBar, vScroll, hScroll, onScrollStart, onScrollMove, onScrollEnd, containerClassName, enablePullDownToRefresh, refreshAction, enableBounce, enableMomentum);
			this.$iscrollContainers.push(container);
			return container;
		},
		SetIscrollList: function(target, pageSize, limitPageCount, enableRefreshData, autoRemoveDom, showScrollBar) {
			var container = new $JSLib_IScrollWraper_IScrollList(target, pageSize, limitPageCount, enableRefreshData, autoRemoveDom);
			this.$iscrollListContainers.push(container);
			container.WrapPullAndRefresh(showScrollBar);
			return container;
		},
		RefreshPageScroll: function() {
			window.setTimeout(ss.mkdel(this, function() {
				if (ss.isValue(this.$iscrollContainers)) {
					for (var $t1 = 0; $t1 < this.$iscrollContainers.length; $t1++) {
						var iscrollContainer = this.$iscrollContainers[$t1];
						iscrollContainer.iscroll.refresh();
					}
				}
				if (ss.isValue(this.$iscrollListContainers)) {
					for (var $t2 = 0; $t2 < this.$iscrollListContainers.length; $t2++) {
						var iscrollListContainer = this.$iscrollListContainers[$t2];
						iscrollListContainer.Refresh();
					}
				}
			}), 300);
		},
		DestoryIscroll: function() {
			try {
				//销毁IscrollContainer
				for (var $t1 = 0; $t1 < this.$iscrollContainers.length; $t1++) {
					var item = this.$iscrollContainers[$t1];
					if (ss.isValue(item)) {
						item.Destroy();
					}
				}
				for (var $t2 = 0; $t2 < this.$iscrollListContainers.length; $t2++) {
					var item1 = this.$iscrollListContainers[$t2];
					if (ss.isValue(item1)) {
						item1.Destroy();
					}
				}
			}
			catch ($t3) {
				var ex = ss.Exception.wrap($t3);
				console.info(ex);
			}
		},
		Destroy: function() {
			this.$1$OnOrientationChangeField = null;
		}
	});
	ss.initClass($JSLib_Controller_ControllerBase, $asm, {
		get_PageContainer: function() {
			return this.$2$PageContainerField;
		},
		set_PageContainer: function(value) {
			this.$2$PageContainerField = value;
		},
		add_OnLoad: function(value) {
			this.$2$OnLoadField = ss.delegateCombine(this.$2$OnLoadField, value);
		},
		remove_OnLoad: function(value) {
			this.$2$OnLoadField = ss.delegateRemove(this.$2$OnLoadField, value);
		},
		add_OnShow: function(value) {
			this.$2$OnShowField = ss.delegateCombine(this.$2$OnShowField, value);
		},
		remove_OnShow: function(value) {
			this.$2$OnShowField = ss.delegateRemove(this.$2$OnShowField, value);
		},
		BeforeLoad: function() {
			return null;
		},
		BuildController: function() {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), task;
			var $sm = ss.mkdel(this, function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								task = this.BeforeLoad();
								if (ss.isValue(task)) {
									$state = 2;
									task.continueWith($sm);
									return;
								}
								$state = 1;
								continue $sm1;
							}
							case 2: {
								$state = -1;
								task.getAwaitedResult();
								$state = 1;
								continue $sm1;
							}
							case 1: {
								$state = -1;
								this.HtmlContent = this.GetHtmlContent();
								this.Content = $(this.HtmlContent);
								this.AfterBuildController();
								$state = -1;
								break $sm1;
							}
							default: {
								break $sm1;
							}
						}
					}
					$tcs.setResult(null);
				}
				catch ($t1) {
					$tcs.setException(ss.Exception.wrap($t1));
				}
			});
			$sm();
			return $tcs.task;
		},
		AfterBuildController: function() {
		},
		FindMemberAndBindEvent: function() {
			this.pageObject = this.Content;
			this.InitialComponent();
			if (!ss.staticEquals(this.$2$OnLoadField, null)) {
				this.$2$OnLoadField(null, null);
			}
		},
		Show: function() {
			this.pageObject.show();
			if (!ss.staticEquals(this.$2$OnShowField, null)) {
				this.$2$OnShowField(null, null);
			}
		},
		DataBind: function(data) {
			return $JSLib_Data_DataBindHelper.Bind(data, ss.getTypeFullName(ss.getInstanceType(this)));
		},
		Destroy: function() {
			$JSLib_Mobile_UIBase.prototype.Destroy.call(this);
			$JSLib_Data_DataBindHelper.RemoveBindData(this.UniqueId);
			this.DestoryIscroll();
			this.DestoryComponent();
			if (ss.isValue(this.Content)) {
				this.Content.unbind().off().remove();
				this.Content = null;
			}
			if (ss.isValue(this.pageObject)) {
				this.pageObject.unbind().off().remove();
				this.pageObject = null;
			}
		}
	}, $JSLib_Mobile_UIBase);
	ss.initClass($JSLib_Controller_UIController, $asm, {
		get_Width: function() {
			return this.$3$WidthField;
		},
		set_Width: function(value) {
			this.$3$WidthField = value;
		},
		get_Height: function() {
			return this.$3$HeightField;
		},
		set_Height: function(value) {
			this.$3$HeightField = value;
		}
	}, $JSLib_Controller_ControllerBase);
	ss.initClass($JSLib_Controller_Panel, $asm, {
		Destroy: function() {
			this.RemoveChild();
			$JSLib_Controller_ControllerBase.prototype.Destroy.call(this);
		},
		AddChild: function(controller, isPrePend, isHidden, page) {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1;
			var $sm = ss.mkdel(this, function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								controller.set_PageContainer(page);
								this.Child.push(controller);
								$t1 = controller.BuildController();
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								$t1.getAwaitedResult();
								if (isPrePend) {
									this.Content.prepend(controller.Content);
								}
								else {
									this.Content.append(controller.Content);
								}
								controller.FindMemberAndBindEvent();
								if (isHidden) {
									controller.Content.hide();
								}
								$state = -1;
								break $sm1;
							}
							default: {
								break $sm1;
							}
						}
					}
					$tcs.setResult(null);
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			});
			$sm();
			return $tcs.task;
		},
		RemoveChild$1: function(index) {
			var item = this.Child[index];
			item.Destroy();
			ss.removeAt(this.Child, index);
		},
		RemoveChild: function() {
			for (var $t1 = 0; $t1 < this.Child.length; $t1++) {
				var item = this.Child[$t1];
				try {
					item.Destroy();
				}
				catch ($t2) {
				}
			}
		},
		GetChild: function(index) {
			return this.Child[index];
		}
	}, $JSLib_Controller_UIController);
	ss.initClass($JSLib_Controller_RazorTemplate, $asm, {
		Execute: function() {
		},
		WriteLiteral: function(content) {
			this.contentBuilder.append(content);
		},
		Write: function(content) {
			this.contentBuilder.append(content);
		},
		WriteLiteralTo: function(textWriter, content) {
			textWriter.append(content);
		},
		WriteTo: function(textWriter, content) {
			if (ss.isNullOrUndefined(content)) {
				textWriter.append('');
			}
			else {
				textWriter.append(content);
			}
		},
		InitialComponent: function() {
		},
		DestoryComponent: function() {
		}
	});
	ss.initClass($JSLib_Data_BindData, $asm, {});
	ss.initClass($JSLib_Data_BindDataItem, $asm, {});
	ss.initClass($JSLib_Data_DataBindHelper, $asm, {});
	ss.initEnum($JSLib_Data_GestureDirection, $asm, { LeftToRight: -1, RightToLeft: 1 });
	ss.initClass($JSLib_DesktopComponent_DataGrid, $asm, {
		$LoadData: function(pageIndex) {
			var $state = 0, $t1, result;
			var $sm = ss.mkdel(this, function() {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							$t1 = this.$option.LoadGridData(pageIndex, this.$option.PageSize);
							$state = 1;
							$t1.continueWith($sm);
							return;
						}
						case 1: {
							$state = -1;
							result = $t1.getAwaitedResult();
							if (ss.isValue(result)) {
								this.$option.PnlTableContent.html(result.TableBody);
								this.$CreatePagination(result.TotalRecords, this.$option.PageSize);
							}
							else {
								this.$option.Pager.data('settings').page = 0;
								this.$CreatePagination(0, this.$option.PageSize);
							}
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			});
			$sm();
		},
		$CreatePagination: function(totalRecords, pageSize) {
			var pages;
			if (totalRecords % pageSize === 0) {
				pages = ss.Int32.div(totalRecords, pageSize);
			}
			else {
				pages = ss.Int32.div(totalRecords, pageSize) + 1;
			}
			if (pages === 0) {
				pages = 1;
			}
			this.$option.Pager.bootpag({ total: pages, maxVisible: 15, first: '首页', last: '尾页', firstLastUse: true });
			if (ss.isValue(this.$option.lblSummary)) {
				this.$option.lblSummary.text(ss.formatString('合计{0}条数据', totalRecords));
			}
		},
		Reload: function() {
			this.$option.Pager.data('settings').page = 1;
			this.$LoadData(0);
		},
		Refresh: function() {
			var pageIndex = this.$option.Pager.data('settings').page - 1;
			this.$LoadData(ss.unbox(ss.cast(pageIndex, ss.Int32)));
		}
	});
	ss.initClass($JSLib_DesktopComponent_DataGridOption, $asm, {});
	ss.initClass($JSLib_DesktopComponent_LoadGridDataResult, $asm, {});
	ss.initClass($JSLib_Extension_BBQ, $asm, {});
	ss.initClass($JSLib_Extension_ImageExtension, $asm, {});
	ss.initEnum($JSLib_Extension_MergeMode, $asm, {});
	ss.initClass($JSLib_Extension_StyleSheetEx, $asm, {});
	ss.initEnum($JSLib_Extension_TouchDirection, $asm, { None: 0, LeftToRight: 1, RightToLeft: 2, UpToDown: 4, DownToUp: 8 });
	ss.initClass($JSLib_Extension_TouchHandler, $asm, {
		add_OnFling: function(value) {
			this.$1$OnFlingField = ss.delegateCombine(this.$1$OnFlingField, value);
		},
		remove_OnFling: function(value) {
			this.$1$OnFlingField = ss.delegateRemove(this.$1$OnFlingField, value);
		},
		add_OnComplete: function(value) {
			this.$1$OnCompleteField = ss.delegateCombine(this.$1$OnCompleteField, value);
		},
		remove_OnComplete: function(value) {
			this.$1$OnCompleteField = ss.delegateRemove(this.$1$OnCompleteField, value);
		},
		get_currentX: function() {
			return this.$_currentX;
		},
		set_currentX: function(value) {
			this.$_preX = this.$_currentX;
			this.$_currentX = value;
		},
		get_$currentY: function() {
			return this.$_currentY;
		},
		set_$currentY: function(value) {
			this.$_preY = this.$_currentY;
			this.$_currentY = value;
		},
		toString: function() {
			return ss.formatString('CurrentX: {0},CurrentY:{1}', this.get_currentX(), this.get_$currentY());
		},
		BeginListen: function() {
			this.$mouseContainer.on('touchstart', ss.mkdel(this, this.$ontouchStart));
			this.$mouseContainer.on('touchmove', ss.mkdel(this, this.$ontouchMove));
			this.$mouseContainer.on('touchend', ss.mkdel(this, this.$ontouchEnd));
		},
		$ontouchEnd: function(event) {
			var touchInfo = ss.cast(event.originalEvent, TouchEvent).changedTouches[0];
			this.set_currentX(touchInfo.screenX);
			this.set_$currentY(touchInfo.screenY);
			this.$timeIDList.forEach(window.clearInterval);
			var deltaX = this.get_currentX() - this.$_preX;
			var deltaY = this.get_$currentY() - this.$_preY;
			this.$_totalDeltaX += deltaX;
			this.$_totalDeltaY += deltaY;
			var $t1 = new $JSLib_Extension_TouchHandlerArgs();
			$t1.currentX = this.$_currentX;
			$t1.currentY = this.$_currentY;
			$t1.deltaX = deltaX;
			$t1.deltaY = deltaY;
			$t1.TotalDeltaX = this.$_totalDeltaX;
			$t1.TotalDeltaY = this.$_totalDeltaY;
			$t1.IsComplete = true;
			var touchHandlerArgs = $t1;
			if (!ss.staticEquals(this.$1$OnCompleteField, null)) {
				this.$1$OnCompleteField(this, touchHandlerArgs);
			}
			this.$_totalDeltaX = 0;
			this.$_totalDeltaY = 0;
		},
		$ontouchMove: function(event) {
			if (this.$canRecord) {
				var touchInfo = ss.cast(event.originalEvent, TouchEvent).touches[0];
				this.set_currentX(touchInfo.screenX);
				this.set_$currentY(touchInfo.screenY);
				this.$canRecord = false;
				if (!ss.staticEquals(this.$1$OnFlingField, null)) {
					var deltaX = this.get_currentX() - this.$_preX;
					var deltaY = this.get_$currentY() - this.$_preY;
					this.$_totalDeltaX += deltaX;
					this.$_totalDeltaY += deltaY;
					var $t2 = this.$1$OnFlingField;
					var $t1 = new $JSLib_Extension_TouchHandlerArgs();
					$t1.currentX = this.$_currentX;
					$t1.currentY = this.$_currentY;
					$t1.deltaX = deltaX;
					$t1.deltaY = deltaY;
					$t1.TotalDeltaX = this.$_totalDeltaX;
					$t1.TotalDeltaY = this.$_totalDeltaY;
					$t2(this, $t1);
				}
			}
		},
		$ontouchStart: function(event) {
			var touchInfo = ss.cast(event.originalEvent, TouchEvent).touches[0];
			this.set_currentX(touchInfo.screenX);
			this.set_$currentY(touchInfo.screenY);
			this.$timeIDList.push(window.setInterval(ss.mkdel(this, function() {
				this.$canRecord = true;
			}), this.$waitTime));
			event.preventDefault();
		},
		StopListen: function() {
			this.$mouseContainer.unbind('touchstart', ss.mkdel(this, this.$ontouchStart));
			this.$mouseContainer.unbind('touchmove', ss.mkdel(this, this.$ontouchMove));
			this.$mouseContainer.unbind('touchend', ss.mkdel(this, this.$ontouchEnd));
		}
	});
	ss.initClass($JSLib_Extension_TouchHandlerArgs, $asm, {
		get_Direction: function() {
			var dir = 0;
			if (this.TotalDeltaX < 0) {
				dir = dir | 2;
			}
			else if (this.TotalDeltaX > 0) {
				dir = dir | 1;
			}
			if (this.TotalDeltaY < 0) {
				dir = dir | 8;
			}
			else if (this.TotalDeltaY > 0) {
				dir = dir | 4;
			}
			return dir;
		},
		get_IsRightToLeft: function() {
			return (this.get_Direction() & 2) === 2;
		},
		get_IsLeftToRight: function() {
			return (this.get_Direction() & 1) === 1;
		},
		get_IsUpToDown: function() {
			return (this.get_Direction() & 4) === 4;
		},
		get_IsDownToUp: function() {
			return (this.get_Direction() & 8) === 8;
		}
	}, ss.EventArgs);
	ss.initClass($JSLib_Extension_TransFormEx, $asm, {});
	ss.initClass($JSLib_Extension_md5_MD5, $asm, {});
	ss.initClass($JSLib_Helper_CookieHelper, $asm, {});
	ss.initClass($JSLib_IScrollWraper_Carousel, $asm, {
		add_OnItemClick: function(value) {
			this.$1$OnItemClickField = ss.delegateCombine(this.$1$OnItemClickField, value);
		},
		remove_OnItemClick: function(value) {
			this.$1$OnItemClickField = ss.delegateRemove(this.$1$OnItemClickField, value);
		},
		$onScrollEnd: function(carouselIndicatorList) {
			carouselIndicatorList.children('li.active').removeClass('active');
			carouselIndicatorList.children(ss.formatString('li:nth-child({0})', this._iscroll.currPageX + 1)).addClass('active');
			var currentULLi = this.$targetUl.children(ss.formatString('li:nth-child({0})', this._iscroll.currPageX + 1));
			var title = currentULLi.attr('data-title');
			if (!ss.isNullOrEmptyString(title)) {
				this.$titleObject.text(title);
			}
		},
		Dispose: function() {
			if (this.$autoSwipeIntervalID !== 0) {
				window.clearInterval(this.$autoSwipeIntervalID);
			}
			if (ss.isValue(this._iscroll)) {
				this._iscroll.destroy();
			}
		},
		GotoPage: function(pageIndex) {
			this._iscroll.scrollToPage(pageIndex, 0, 500);
		}
	});
	ss.initClass($JSLib_IScrollWraper_CarouselItemArgs, $asm, {}, ss.EventArgs);
	ss.initClass($JSLib_IScrollWraper_IScrollContainer, $asm, {
		$BuildIscroller: function(ele, showScrollBar, vScroll, hScroll, onScrollStart, onScrollMove, onScrollEnd, containerClassName, enablePullDownToRefresh, refreshAction, enableBounce, enableMomentum) {
			this.$enablePullDownToRefresh = enablePullDownToRefresh;
			this.$onScrollMove = onScrollMove;
			this.$onScrollEnd = onScrollEnd;
			if (window.getComputedStyle(ele).webkitBoxFlex === '1') {
				console.info('请设置为可滚动的元素为自然高度,以避免iscroll高度计算错误!', ele);
			}
			var warped = $(ele).wrap('<div></div>').parent().addClass('c-scroll-container').addClass(containerClassName);
			if (enablePullDownToRefresh) {
				warped.before("<div id='pullDown' class='loading'><span class='pullDownIcon'></span><span class='pullDownLabel'>向下拉以刷新...</span></div>");
				this.$refreshAction = refreshAction;
				this.$pullDownEl = warped.prev();
				this.$pullDownLabel = this.$pullDownEl.find('.pullDownLabel');
				this.$pullDownEl.hide();
			}
			var scrollOptions = { fadeScrollbar: true, hideScrollbar: true, onScrollStart: onScrollStart, onScrollMove: ss.mkdel(this, this.$OnScrollMove), onScrollEnd: ss.mkdel(this, this.$OnScrollEnd), vScroll: vScroll, hScroll: hScroll };
			if (showScrollBar) {
				scrollOptions.scrollbars = true;
				scrollOptions.fadeScrollbars = true;
			}
			else {
				scrollOptions.vScrollbar = false;
				scrollOptions.hScrollbar = false;
			}
			scrollOptions.onBeforeScrollStart = function(event) {
				$JSLib_IScrollWraper_IScrollContainer.HandleBeforeScrollStart(event);
			};
			scrollOptions.bounce = enableBounce;
			scrollOptions.momentum = enableMomentum;
			this.iscroll = new iScroll(warped[0], scrollOptions);
			window.setTimeout(ss.mkdel(this, function() {
				this.iscroll.refresh();
			}), 200);
		},
		$OnScrollEnd: function(arg) {
			var $state = 0, $t1, $t2;
			var $sm = ss.mkdel(this, function() {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							if (this.$enablePullDownToRefresh && this.$pullDownEl.hasClass('flip')) {
								this.$pullDownEl[0].className = 'loading';
								this.$pullDownLabel.text('载入中...');
								//执行刷新任务
								if (!ss.staticEquals(this.$refreshAction, null)) {
									$t1 = this.$refreshAction();
									$state = 3;
									$t1.continueWith($sm);
									return;
								}
								else {
									$t2 = ss.Task.delay(3000);
									$state = 4;
									$t2.continueWith($sm);
									return;
								}
							}
							$state = 1;
							continue $sm1;
						}
						case 3: {
							$state = -1;
							$t1.getAwaitedResult();
							$state = 2;
							continue $sm1;
						}
						case 4: {
							$state = -1;
							$t2.getAwaitedResult();
							$state = 2;
							continue $sm1;
						}
						case 2: {
							$state = -1;
							this.$pullDownEl.removeClass('flip');
							this.$pullDownEl.hide();
							$state = 1;
							continue $sm1;
						}
						case 1: {
							$state = -1;
							if (!ss.staticEquals(this.$onScrollEnd, null)) {
								this.$onScrollEnd(null);
							}
							$JSLib_Util_IsScrollExtension.CheckRefresh(this.iscroll);
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			});
			$sm();
		},
		$OnScrollMove: function(e) {
			if (this.$enablePullDownToRefresh && this.iscroll.distY > 0 && this.iscroll.y > 15 && !this.$pullDownEl.hasClass('flip')) {
				this.$isPullDownBegin = true;
				this.$pullDownEl.show();
				this.$pullDownEl[0].className = 'flip';
				this.$pullDownLabel.text($JSLib_IScrollWraper_IScrollContainer.$releaseToRefresh);
			}
			if (!ss.staticEquals(this.$onScrollMove, null)) {
				this.$onScrollMove(null);
			}
		},
		Disable: function() {
			if (ss.isValue(this.iscroll)) {
				this.iscroll.disable();
			}
		},
		Enable: function() {
			if (ss.isValue(this.iscroll)) {
				this.iscroll.enable();
			}
		},
		Destroy: function() {
			if (ss.isValue(this.iscroll)) {
				this.iscroll.destroy();
			}
		}
	});
	$JSLib_IScrollWraper_IScrollContainer.$ctor1.prototype = $JSLib_IScrollWraper_IScrollContainer.prototype;
	ss.initClass($JSLib_IScrollWraper_IScrollList, $asm, {
		get_GetPageData: function() {
			return this.$1$GetPageDataField;
		},
		set_GetPageData: function(value) {
			this.$1$GetPageDataField = value;
		},
		WrapPullAndRefresh: function(showScrollBar) {
			if (this.$isWarped) {
				console.error('当前方法已执行!', []);
				throw new ss.Exception('当前方法已执行!');
			}
			this.$isWarped = true;
			this.$targetList.wrapAll("<div class='c-scroll-container'/>").wrapAll("<div class='c-scroll-warper'/>");
			this.$targetList.before("<div id='pullDown' class='loading'><span class='pullDownIcon'></span><span class='pullDownLabel'>向下拉以刷新...</span></div>");
			this.$targetList.after("<div id='pullUp' class='loading' ><span class='pullUpIcon'></span><span class='pullUpLabel'>上拉加载更多数据...</span></div>");
			this.$pullDownEl = this.$targetList.prev();
			this.$pullUpEl = this.$targetList.next();
			this.$pullDownEl.show();
			this.$pullUpEl.show();
			this.$wraper = this.$targetList.parent().parent();
			var scrollOptions = { fadeScrollbar: true, vScrollbar: true, hideScrollbar: true, useTransform: true, useTransition: true, probeType: 3, click: true, onScrollEnd: ss.mkdel(this, this.$OnScrollEnd), onScrollMove: ss.mkdel(this, this.$OnScrollMove) };
			if (!showScrollBar) {
				scrollOptions.fadeScrollbar = false;
				scrollOptions.vScrollbar = false;
			}
			else {
				scrollOptions.scrollbars = true;
				scrollOptions.fadeScrollbars = true;
			}
			this.iscroll = new iScroll(this.$wraper[0], scrollOptions);
			this.$pullDownLabel = this.$pullDownEl.find('.pullDownLabel');
			this.$pullUpLabel = this.$pullUpEl.find('.pullUpLabel');
			this.$pullDownEl.hide();
			this.$pullUpEl.hide();
		},
		$OnScrollMove: function(e) {
			if (!ss.staticEquals(this.OnScroll, null)) {
				this.OnScroll(e);
			}
			if (this.$IsLoadingData) {
				return;
			}
			if (ss.isNullOrUndefined(this.$pullUpEl)) {
				return;
			}
			if (this.iscroll.distY > 0 && this.iscroll.y > 15) {
				console.info(this.iscroll.distY, this.iscroll.y);
				//不足一页数据
				if (this.$MaxPageIndex === $JSLib_Mobile_Application.get_PageIndexStart()) {
					if (!this.$EnableRefreshData) {
						return;
					}
				}
				this.$isPullDownBegin = true;
				if (!$jQueryObjectEx.IsVisible(this.$pullDownEl)) {
					this.$pullDownEl.show();
					this.$pullDownEl[0].className = 'flip';
					this.$pullDownLabel.text($JSLib_IScrollWraper_IScrollList.$releaseToRefresh);
				}
				return;
			}
			else {
				console.info('else', this.iscroll.distY, this.iscroll.y);
				this.$isPullDownBegin = false;
				this.$pullDownEl.hide();
			}
			if (this.iscroll.distY < 0 && this.iscroll.y < this.iscroll.maxScrollY - 50) {
				if (this.$MaxPageIndex === $JSLib_Mobile_Application.get_PageIndexStart()) {
					return;
				}
				if (this.ToPageIndex === this.$MaxPageIndex) {
					return;
				}
				this.$isPullUpBegin = true;
				if (!$jQueryObjectEx.IsVisible(this.$pullUpEl)) {
					this.$pullUpEl.show();
					this.$pullUpEl[0].className = 'flip';
					this.$pullUpLabel.text($JSLib_IScrollWraper_IScrollList.$releaseToRefresh);
					$JSLib_Util_IsScrollExtension.CheckRefresh(this.iscroll);
				}
			}
			else {
				//Console.Info("else", iscroll.distY, iscroll.y);
				this.$isPullUpBegin = false;
				//pullUpEl.Hide();
			}
		},
		$OnScrollEnd: function(e) {
			$JSLib_Util_IsScrollExtension.CheckRefresh(this.iscroll);
			if (this.$isPullUpBegin) {
				if (this.$pullUpEl.hasClass('flip')) {
					this.$pullUpEl[0].className = 'loading';
					this.$pullUpLabel.text('载入中...');
				}
				this.$isPullUpBegin = false;
				this.$OnPullToEnd(1);
				return;
			}
			else {
				this.$pullUpEl.hide();
			}
			if (this.$isPullDownBegin) {
				if (this.$pullDownEl.hasClass('flip')) {
					this.$pullDownEl[0].className = 'loading';
					this.$pullDownLabel.text('载入中...');
				}
				this.$isPullDownBegin = false;
				this.$OnPullToEnd(2);
				return;
			}
		},
		$OnPullToEnd: function(direction) {
			var $state = 0, $t1, result, addHeight, removedHeight, limitCount, haveCount, removeElement, beginCount, removeElement1;
			var $sm = ss.mkdel(this, function() {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							if (this.$IsLoadingData) {
								return;
							}
							if (direction === 2) {
								if (this.FromPageIndex === $JSLib_Mobile_Application.get_PageIndexStart()) {
									this.ReLoad();
									return;
								}
								this.FromPageIndex--;
								this.currentPageIndex--;
							}
							else if (direction === 1) {
								if (this.ToPageIndex >= this.$MaxPageIndex) {
									console.info('  pullUpEl.Hide();');
									this.$pullUpEl.hide();
									return;
								}
								if ($JSLib_JSLibConfig.IsUseZepto) {
									this.iscroll.maxScrollY -= this.$pullUpEl.height();
								}
								else {
									this.iscroll.maxScrollY -= this.$pullUpEl.outerHeight();
								}
								//iscroll.scrollTo(0, iscroll.maxScrollY, 0, false);
								this.ToPageIndex += 1;
								this.currentPageIndex++;
							}
							if (!ss.staticEquals(this.get_GetPageData(), null)) {
								this.$IsLoadingData = true;
								$t1 = this.get_GetPageData()(this.currentPageIndex);
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							$state = -1;
							break $sm1;
						}
						case 1: {
							$state = -1;
							result = $t1.getAwaitedResult();
							this.$IsLoadingData = false;
							if (direction === 1 && result.RecordCount < this.$PageSize) {
								this.$MaxPageIndex = this.ToPageIndex;
							}
							addHeight = 0;
							//绑定数据
							if (result.RecordCount !== 0) {
								if (direction === 1) {
									this.$targetList.append(result.ResultElement);
								}
								else {
									this.$targetList.prepend(result.ResultElement);
								}
							}
							result.ResultElement = null;
							result = null;
							removedHeight = 0;
							if (this.$autoRemoveDom) {
								limitCount = this.LimitPageCount * this.$PageSize;
								haveCount = this.$targetList.children().length;
								if (haveCount > limitCount) {
									if (direction === 1) {
										removeElement = this.$targetList.find(ss.formatString('>:nth-child(-n+{0})', this.$PageSize));
										removedHeight += $jQueryObjectEx.TotalHeight(removeElement);
										removeElement.unbind();
										removeElement.remove();
										removeElement = null;
										this.FromPageIndex++;
									}
									else {
										beginCount = ss.Int32.div(haveCount, this.$PageSize) * this.$PageSize;
										removeElement1 = this.$targetList.find(ss.formatString('>:nth-child(n+{0})', beginCount));
										removedHeight += $jQueryObjectEx.TotalHeight(removeElement1);
										removeElement1.unbind();
										removeElement1.remove();
										removeElement1 = null;
										this.ToPageIndex--;
									}
								}
							}
							this.Refresh();
							//targetList.Children().Each(delegate(int index, Element element)
							//      {
							//          if (index < PageSize)
							//          {
							//              addHeight += element.ToJQueryObject().GetHeight();
							//          }
							//      });
							//if (direction == PullDirection.Up)
							//{
							//    if (removedHeight > 0)
							//    {
							//        iscroll.scrollTo(0, iscroll.y + addHeight, 0, false);
							//    }
							//}
							//else if (direction == PullDirection.Down)
							//{
							//    iscroll.scrollTo(0, -addHeight, 0, false);
							//}
							this.$pullUpEl.hide();
							console.info('  pullUpEl.Hide();');
							this.$pullDownEl.hide();
							this.Refresh();
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			});
			$sm();
		},
		SetWarperWidth: function(width) {
			this.$wraper.width(width);
		},
		Refresh: function() {
			if (ss.isValue(this.iscroll)) {
				this.iscroll.refresh();
			}
		},
		ReLoad: function() {
			var $state = 0, $t1, result;
			var $sm = ss.mkdel(this, function() {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							this.FromPageIndex = $JSLib_Mobile_Application.get_PageIndexStart();
							this.ToPageIndex = $JSLib_Mobile_Application.get_PageIndexStart();
							this.$MaxPageIndex = 2147483647;
							this.currentPageIndex = $JSLib_Mobile_Application.get_PageIndexStart();
							$t1 = this.get_GetPageData()($JSLib_Mobile_Application.get_PageIndexStart());
							$state = 1;
							$t1.continueWith($sm);
							return;
						}
						case 1: {
							$state = -1;
							result = $t1.getAwaitedResult();
							this.$pullDownEl.hide();
							this.$targetList.empty();
							if (ss.isNullOrUndefined(result)) {
								return;
							}
							if (result.RecordCount < this.$PageSize) {
								this.$MaxPageIndex = $JSLib_Mobile_Application.get_PageIndexStart();
							}
							this.$targetList.append(result.ResultElement);
							this.Refresh();
							$IScrollEx.GotoTop(this.iscroll);
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			});
			$sm();
		},
		Destroy: function() {
			if (ss.isValue(this.iscroll)) {
				this.iscroll.destroy();
				this.iscroll = null;
			}
			//targetList.Remove();
			//pullDownLabel.Remove();
			//pullUpLabel.Remove();
			//pullUpEl.Remove();
			//pullDownEl.Remove();
			//wraper.Remove();
			this.$pullDownLabel = null;
			this.$pullUpLabel = null;
			this.$pullUpEl = null;
			this.$pullDownEl = null;
			this.$targetList = null;
			this.$wraper = null;
			this.set_GetPageData(null);
		},
		Disable: function() {
			if (ss.isValue(this.iscroll)) {
				this.iscroll.disable();
			}
		},
		Enable: function() {
			if (ss.isValue(this.iscroll)) {
				this.iscroll.enable();
			}
		}
	});
	ss.initEnum($JSLib_IScrollWraper_IScrollList$PullDirection, $asm, { Up: 1, Down: 2 });
	ss.initClass($JSLib_IScrollWraper_NaivgateBar, $asm, {
		add_OnItemClick: function(value) {
			this.$1$OnItemClickField = ss.delegateCombine(this.$1$OnItemClickField, value);
		},
		remove_OnItemClick: function(value) {
			this.$1$OnItemClickField = ss.delegateRemove(this.$1$OnItemClickField, value);
		},
		adjustText: function() {
			this.$datalist.find('li>span').each(function(index, element) {
				element.style.marginLeft = (-ss.Int32.div($(element).outerWidth(), 2)).toString() + 'px';
			});
		},
		ClickFirst: function() {
			this.$datalist.children().first().click();
		},
		SetActived: function(data) {
			this.$datalist.find('li').removeClass('active');
			this.$datalist.find('li[data-sid=' + data + ']').addClass('active');
		}
	});
	ss.initClass($JSLib_IScrollWraper_NavigateBarOption, $asm, {});
	ss.initClass($JSLib_IScrollWraper_NavigateClickArgs, $asm, {}, ss.EventArgs);
	ss.initClass($JSLib_IScrollWraper_PageData, $asm, {});
	ss.initClass($JSLib_Map_BaiduGeocodingService, $asm, {
		GetLocationAddress: function(baiduMapKey, lng, lat) {
			var tsk = new ss.TaskCompletionSource();
			var options = {
				dataType: 'jsonp',
				url: ss.formatString('http://api.map.baidu.com/geocoder/v2/?ak={0}&location={1},{2}&output=json&pois=0', baiduMapKey, lat, lng),
				success: function(data, status, request) {
					var result = data;
					tsk.setResult(result.result.formatted_address);
					console.info(data);
				},
				error: function(request1, status1, error) {
					tsk.setResult(null);
				}
			};
			$.ajax(options);
			return tsk.task;
		}
	});
	ss.initClass($JSLib_Map_DbGeographyValue, $asm, {});
	ss.initClass($JSLib_Map_GeographyEx, $asm, {});
	ss.initClass($JSLib_Map_GeographyType, $asm, {});
	ss.initClass($JSLib_Mobile_AppGlobalTemplate, $asm, {
		GetErrorPage: function(errorInfo) {
			return new $System_Web_WebPages_HelperResult(ss.mkdel(this, function(__razor_helper_writer) {
				this.WriteLiteralTo(__razor_helper_writer, '    <div data-role=\'page\' class=\'ErrorPage\'>\r\n        <p class="ErrorTitle"> 软件出现异常：</p>\r\n        <p class="ErrorMessage">');
				this.WriteTo(__razor_helper_writer, errorInfo);
				this.WriteLiteralTo(__razor_helper_writer, '</p>\r\n        <a class="ErrorBackButton" data-appback=\'\'>返回</a>\r\n    </div>\r\n');
			}));
		},
		Execute: function() {
			this.WriteLiteral('\r\n');
		}
	}, $JSLib_Controller_RazorTemplate);
	ss.initClass($JSLib_Mobile_Application, $asm, {});
	ss.initClass($JSLib_Mobile_ApplicationErrorArgs, $asm, {}, ss.EventArgs);
	ss.initEnum($JSLib_Mobile_ApplicationMode, $asm, { WebMode: 1, DeviceMode: 2 });
	ss.initInterface($JSLib_Mobile_IShowLoading, $asm, { ShowLoading: null });
	ss.initClass($JSLib_Mobile_BasePage, $asm, {
		add_OnPageInit: function(value) {
			this.$2$OnPageInitField = ss.delegateCombine(this.$2$OnPageInitField, value);
		},
		remove_OnPageInit: function(value) {
			this.$2$OnPageInitField = ss.delegateRemove(this.$2$OnPageInitField, value);
		},
		add_OnPageBeforeShow: function(value) {
			this.$2$OnPageBeforeShowField = ss.delegateCombine(this.$2$OnPageBeforeShowField, value);
		},
		remove_OnPageBeforeShow: function(value) {
			this.$2$OnPageBeforeShowField = ss.delegateRemove(this.$2$OnPageBeforeShowField, value);
		},
		add_OnPageShow: function(value) {
			this.$2$OnPageShowField = ss.delegateCombine(this.$2$OnPageShowField, value);
		},
		remove_OnPageShow: function(value) {
			this.$2$OnPageShowField = ss.delegateRemove(this.$2$OnPageShowField, value);
		},
		add_OnPageHide: function(value) {
			this.$2$OnPageHideField = ss.delegateCombine(this.$2$OnPageHideField, value);
		},
		remove_OnPageHide: function(value) {
			this.$2$OnPageHideField = ss.delegateRemove(this.$2$OnPageHideField, value);
		},
		get_pageTranslateAnimation: function() {
			return this.$2$pageTranslateAnimationField;
		},
		set_pageTranslateAnimation: function(value) {
			this.$2$pageTranslateAnimationField = value;
		},
		GetTraslationInOutClass: function() {
			var baseInClass = 'slideIn';
			var baseOutClass = 'slideInReverse';
			if (this.get_pageTranslateAnimation() === 1) {
				return { item1: 'no_' + baseInClass, item2: 'no_' + baseOutClass };
			}
			else if (this.get_pageTranslateAnimation() === 0) {
				return { item1: baseInClass, item2: baseOutClass };
			}
			return null;
		},
		PreparePage: null,
		ClearOldUIData: function() {
		},
		Hide: function() {
			if (ss.isValue(this.pageObject)) {
				this.pageObject.hide();
			}
			if (!ss.staticEquals(this.$2$OnPageHideField, null)) {
				this.$2$OnPageHideField(null, null);
			}
			return this.pageObject;
		},
		Show: function(animate) {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), isFirstPage, pageInClass, tsk, onAnimationEnd;
			var $sm = ss.mkdel(this, function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								isFirstPage = $JSLib_Mobile_MobilePage.DomPageRepository.length === 1;
								if (!$JSLib_JSLibConfig.PageTransitionEffect || isFirstPage) {
									animate = false;
								}
								this.LastShowDateTime = new Date();
								if (!ss.staticEquals(this.$2$OnPageBeforeShowField, null)) {
									this.$2$OnPageBeforeShowField(null, null);
								}
								if (ss.isValue(this.pageObject)) {
									this.pageObject.css('zIndex', ++$JSLib_Mobile_MobilePage.CurrentPageZIndex);
									//前进使用的动画
									if (animate) {
										pageInClass = this.GetTraslationInOutClass().item1;
										tsk = new ss.TaskCompletionSource();
										onAnimationEnd = null;
										onAnimationEnd = ss.thisFix(ss.mkdel(this, function(elem, event) {
											this.pageObject.off('webkitAnimationEnd', onAnimationEnd);
											this.pageObject.removeClass(pageInClass);
											tsk.setResult(null);
										}));
										this.pageObject.on('webkitAnimationEnd', onAnimationEnd);
										this.pageObject.addClass(pageInClass).show();
										$state = 2;
										tsk.task.continueWith($sm);
										return;
									}
									else {
										this.pageObject.show();
										$state = 1;
										continue $sm1;
									}
								}
								$state = 1;
								continue $sm1;
							}
							case 2: {
								$state = -1;
								tsk.task.getAwaitedResult();
								$state = 1;
								continue $sm1;
							}
							case 1: {
								$state = -1;
								if (!ss.staticEquals(this.$2$OnPageShowField, null)) {
									window.setTimeout(ss.mkdel(this, function() {
										try {
											this.$2$OnPageShowField(null, null);
										}
										catch ($t1) {
											var ex = ss.Exception.wrap($t1);
											$JSLib_Mobile_BasePage.$LogError(ex);
										}
									}), this.get_delayTime());
								}
								//页面显示100ms后，重新计算滚动区域
								window.setTimeout(ss.mkdel(this, function() {
									this.RefreshPageScroll();
								}), 100);
								$state = -1;
								break $sm1;
							}
							default: {
								break $sm1;
							}
						}
					}
					$tcs.setResult(null);
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			});
			$sm();
			return $tcs.task;
		},
		add_OnDestroy: function(value) {
			this.$2$OnDestroyField = ss.delegateCombine(this.$2$OnDestroyField, value);
		},
		remove_OnDestroy: function(value) {
			this.$2$OnDestroyField = ss.delegateRemove(this.$2$OnDestroyField, value);
		},
		DataBind: function(data) {
			return $JSLib_Data_DataBindHelper.Bind(data, ss.getTypeFullName(ss.getInstanceType(this)));
		},
		Destroy: function() {
			$JSLib_Mobile_UIBase.prototype.Destroy.call(this);
			$JSLib_Data_DataBindHelper.RemoveBindData(this.UniqueId);
			$JSLib_Mobile_Application.SystemLog$1('销毁页面:', ss.getTypeFullName(ss.getInstanceType(this)));
			try {
				if (!ss.staticEquals(this.$2$OnDestroyField, null)) {
					this.$2$OnDestroyField(this, null);
				}
			}
			catch ($t1) {
				var ex = ss.Exception.wrap($t1);
				console.error(ex);
			}
			this.DestroyImpl(true);
		},
		DestroyImpl: function(Deep) {
			if (this.$isDestroyed) {
				return;
			}
			this.$isDestroyed = true;
			this.DestoryComponent();
			this.DestoryIscroll();
			var currentPageType = ss.getInstanceType(this);
			var removePage = Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).firstOrDefault(ss.mkdel(this, function(n) {
				return ss.referenceEquals(n.BasePage, this);
			}), ss.getDefaultValue($JSLib_Mobile_PageInstanceData));
			ss.remove($JSLib_Mobile_MobilePage.DomPageRepository, removePage);
			//检查该类型在Dom树中没有存在的实例对像，则移除该类型的样式表链接
			var isExistCurrentTypeInstance = Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).any(function(n1) {
				return ss.referenceEquals(n1.PageData.PageType, removePage.PageData.PageType);
			});
			this.pageObject.unbind().off();
			this.pageObject.remove();
			if (Deep) {
				if ($JSLib_Mobile_Application.AutoLoadCss && !$JSLib_Mobile_Application.PreserveCss) {
					if (!isExistCurrentTypeInstance) {
						$(ss.formatString('link[data-type={0}]', $JSLib_Mobile_MobilePage.GeneratePageId$1(currentPageType, null))).remove();
					}
				}
			}
			if (Deep) {
				this.pageObject = null;
				this.InitialData = null;
				this.$2$OnPageInitField = null;
				this.$2$OnPageShowField = null;
				this.$2$OnPageHideField = null;
				this.$2$OnPageBeforeShowField = null;
				this.LastShowDateTime = null;
			}
		},
		get_delayTime: function() {
			return (($JSLib_Mobile_Application.DeviceType === 10) ? 400 : 0);
		},
		TriggerInitEvent: function() {
			$JSLib_Mobile_Application.SystemLog$1('触发页面Init事件:', ss.getTypeFullName(ss.getInstanceType(this)));
			this.pageObject.on('click', '[data-role=BackButton]', function(event) {
				$JSLib_Mobile_Application.Back(1);
			});
			//向右滑的手势执行后退
			if ($JSLib_Mobile_Application.EnableSwipeLeftToRightToBack && !this.get_DisableSwipBack()) {
				this.pageObject.on('swipeRight', function(event1) {
					$JSLib_Mobile_Application.Back(1);
				});
			}
			if ($JSLib_Mobile_Application.EnableSwipeRightToLeftFoward && !this.get_DisableSwipBack()) {
				this.pageObject.on('swipeLeft', function(event2) {
					$JSLib_Mobile_Application.Foward();
				});
			}
			if (!ss.staticEquals(this.$2$OnPageInitField, null)) {
				//Android机器等候400毫秒才绑定页面事件，阻止事件穿透的影响
				window.setTimeout(ss.mkdel(this, function() {
					try {
						this.$2$OnPageInitField(this, null);
					}
					catch ($t1) {
						var ex = ss.Exception.wrap($t1);
						console.error(ex);
						$JSLib_Mobile_BasePage.$LogError(ex);
					}
				}), this.get_delayTime());
			}
		},
		TriggerPageShowEvent: function() {
			if (!ss.staticEquals(this.$2$OnPageBeforeShowField, null)) {
				try {
					this.$2$OnPageBeforeShowField(null, null);
				}
				catch ($t1) {
					var ex = ss.Exception.wrap($t1);
					$JSLib_Mobile_BasePage.$LogError(ex);
				}
			}
			if (!ss.staticEquals(this.$2$OnPageShowField, null)) {
				//Android机器等候400毫秒才绑定页面事件，阻止事件穿透的影响
				window.setTimeout(ss.mkdel(this, function() {
					try {
						this.$2$OnPageShowField(null, null);
					}
					catch ($t2) {
						var ex1 = ss.Exception.wrap($t2);
						$JSLib_Mobile_BasePage.$LogError(ex1);
					}
				}), this.get_delayTime());
			}
		},
		TriggerHideEvent: function() {
			this.pageObject.hide();
			if (!ss.staticEquals(this.$2$OnPageHideField, null)) {
				try {
					this.$2$OnPageHideField(null, null);
				}
				catch ($t1) {
					var ex = ss.Exception.wrap($t1);
					$JSLib_Mobile_BasePage.$LogError(ex);
				}
			}
		},
		ReloadPage: function() {
			var $state = 0, $t1, pageType, isExistPage, $t3, $t2, $t4, ex1;
			var $sm = ss.mkdel(this, function() {
				$sm1:
				for (;;) {
					switch ($state) {
						case 0: {
							$state = -1;
							this.DestroyImpl(false);
							$t1 = this.PreparePage();
							$state = 1;
							$t1.continueWith($sm);
							return;
						}
						case 1: {
							$state = -1;
							this.pageObject = $t1.getAwaitedResult();
							$JSLib_Mobile_MobilePage.PageContainer.append(this.pageObject);
							this.InitialComponent();
							pageType = ss.getInstanceType(this);
							this.pageObject.attr('data-sid', $JSLib_Mobile_MobilePage.GeneratePageId$1(pageType, this.InitialData));
							//如果加载的页面中不包含即将跳转的页面，则创建对应的页面，并将其加载到dom树中
							isExistPage = Enumerable.from($JSLib_Mobile_MobilePage.DomPageRepository).any(ss.mkdel(this, function(n) {
								return n.PageData.getHashCode() === (new $JSLib_Mobile_PageData(pageType, this.InitialData)).getHashCode();
							}));
							if (!isExistPage) {
								$t3 = $JSLib_Mobile_MobilePage.DomPageRepository;
								$t2 = new $JSLib_Mobile_PageInstanceData();
								$t2.PageData = new $JSLib_Mobile_PageData(pageType, this.InitialData);
								$t2.BasePage = this;
								$t3.push($t2);
							}
							if (!this.IsPrepareDataError) {
								$state = 3;
								continue $sm1;
							}
							else {
								this.pageObject.show();
								$state = 2;
								continue $sm1;
							}
						}
						case 3:
						case 4:
						case 5: {
							if ($state === 3) {
								$state = 4;
							}
							try {
								$sm2:
								for (;;) {
									switch ($state) {
										case 4: {
											$state = -1;
											this.TriggerInitEvent();
											$t4 = this.Show(true);
											$state = 5;
											$t4.continueWith($sm);
											return;
										}
										case 5: {
											$state = -1;
											$t4.getAwaitedResult();
											if (!ss.staticEquals(this.$2$OnPageShowField, null)) {
												window.setTimeout(ss.mkdel(this, function() {
													try {
														this.$2$OnPageShowField(null, null);
													}
													catch ($t5) {
														var ex = ss.Exception.wrap($t5);
														$JSLib_Mobile_BasePage.$LogError(ex);
													}
												}), this.get_delayTime());
											}
											$state = -1;
											break $sm2;
										}
										default: {
											break $sm2;
										}
									}
								}
							}
							catch ($t6) {
								ex1 = ss.Exception.wrap($t6);
								console.error(ex1);
								$JSLib_Mobile_BasePage.$LogError(ex1);
							}
							$state = 2;
							continue $sm1;
						}
						case 2: {
							$state = -1;
							this.$isDestroyed = false;
							$state = -1;
							break $sm1;
						}
						default: {
							break $sm1;
						}
					}
				}
			});
			$sm();
		},
		get_DisableSwipBack: function() {
			return false;
		},
		get_PageContainer: function() {
			if (ss.isNullOrUndefined(this.pageObject) || this.pageObject.length === 0) {
				return null;
			}
			return $JSLib_Controller_Panel.FromElement(this.pageObject[0]);
		},
		ShowLoading: function(maxShowTime) {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), loadingDialog, $t1;
			var $sm = ss.mkdel(this, function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								loadingDialog = new $JSLib_Widget_Common_LoadingDialog();
								if (ss.isNullOrUndefined(this.get_PageContainer())) {
									$tcs.setResult(loadingDialog);
									return;
								}
								$t1 = this.get_PageContainer().AddChild(loadingDialog, true, false, null);
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								$t1.getAwaitedResult();
								if (maxShowTime > 0) {
									window.setTimeout(function() {
										loadingDialog.Destroy();
									}, maxShowTime * 1000);
								}
								$tcs.setResult(loadingDialog);
								return;
							}
							default: {
								break $sm1;
							}
						}
					}
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			});
			$sm();
			return $tcs.task;
		}
	}, $JSLib_Mobile_UIBase, [$JSLib_Mobile_IShowLoading]);
	ss.initEnum($JSLib_Mobile_DeviceType, $asm, { Unknown: 0, Iphone: 1, Android: 10, Desktop: 20 });
	ss.initClass($JSLib_Mobile_HashChangeArgs, $asm, {}, ss.EventArgs);
	ss.initClass($JSLib_Mobile_MessageBox, $asm, {});
	ss.initClass($JSLib_Mobile_MobilePage, $asm, {});
	ss.initClass($JSLib_Mobile_NavigationManager, $asm, {});
	ss.initEnum($JSLib_Mobile_NetWorkState, $asm, { OnLine: 1, OffLine: 2 });
	ss.initClass($JSLib_Mobile_OrientationChangArgs, $asm, {});
	ss.initClass($JSLib_Mobile_PageData, $asm, {
		get_HashString: function() {
			return $JSLib_Mobile_MobilePage.GeneratePageId$1(this.PageType, this.PageInitialData);
		},
		equals: function(targetObject) {
			if (ss.isInstanceOfType(targetObject, String)) {
				return ss.referenceEquals(this.get_HashString(), ss.cast(targetObject, String));
			}
			{
				return this.getHashCode() === ss.getHashCode(targetObject);
			}
		},
		getHashCode: function() {
			var page = ss.replaceAllString(ss.getTypeFullName(this.PageType), '.', '-');
			var pageData = '';
			if (ss.isValue(this.PageInitialData)) {
				pageData = '--' + JSON.stringify(this.PageInitialData);
			}
			var data = page + pageData;
			return ss.getHashCode(data);
		},
		Clone: function() {
			return new $JSLib_Mobile_PageData(this.PageType, this.PageInitialData);
		}
	});
	ss.initClass($JSLib_Mobile_PageInstanceData, $asm, {});
	ss.initClass($JSLib_Mobile_PageLoader, $asm, {});
	ss.initClass($JSLib_Mobile_PageResourceManager, $asm, {});
	ss.initEnum($JSLib_Mobile_PageTranslateAnimation, $asm, { Default: 0, No: 1 });
	ss.initClass($JSLib_Mobile_PhoneGap, $asm, {});
	ss.initClass($JSLib_Mobile_UserGeoLocation, $asm, {});
	ss.initClass($JSLib_Mobile_UserGeoLocationArgs, $asm, {}, ss.EventArgs);
	ss.initClass($JSLib_Oauth_SinaLogin, $asm, {});
	ss.initClass($JSLib_Plugin_Printer, $asm, {});
	ss.initClass($JSLib_ResourceManager_ResourceLoader, $asm, {
		LoadJS: function(jsPath, nameValues) {
			var tsk = new ss.TaskCompletionSource();
			this.$GetScript(jsPath, function() {
				tsk.setResult(true);
			}, nameValues);
			return tsk.task;
		},
		$GetScript: function(url, callBackAction, nameValues) {
			var script = document.createElement('script');
			script.async = true;
			script.src = url;
			if (!ss.staticEquals(callBackAction, null)) {
				script.onload = callBackAction;
			}
			var scripttarget = $(script);
			if (ss.isValue(nameValues) && nameValues.length >= 2) {
				for (var i = 0; i < ss.Int32.div(nameValues.length, 2); i += 2) {
					scripttarget.attr(nameValues[i], nameValues[i + 1]);
				}
			}
			document.getElementsByTagName('head')[0].appendChild(script);
		}
	});
	ss.initClass($JSLib_UnionLogin_QQLogin, $asm, {});
	ss.initClass($JSLib_UnionLogin_QQLoignAccessInfo, $asm, {});
	ss.initClass($JSLib_Util_IocContainer, $asm, {
		$SetRegistrationForType: function(type, TypeRegistration) {
			this.$TypeRegistrations.set_item(ss.getTypeFullName(type), TypeRegistration);
		},
		$GetRegistrationForType: function(type) {
			if (this.$TypeRegistrations.containsKey(ss.getTypeFullName(type))) {
				return this.$TypeRegistrations.get_item(ss.getTypeFullName(type));
			}
			else {
				if (this.AutoRegistration) {
					if (this.DefaultLifetime === 0) {
						this.$Register(type, type, true, null, null);
					}
					else {
						this.$Register(type, type, false, null, null);
					}
					// Register as a transient object
					return this.$TypeRegistrations.get_item(ss.getTypeFullName(type));
				}
				return null;
			}
		},
		$Register: function(type, implementationType, singleton, constructor, instance) {
			if (ss.isNullOrUndefined(instance)) {
				this.$SetRegistrationForType(type, new $JSLib_$Util_IocContainer$TypeRegistration.$ctor2(implementationType, singleton, constructor));
			}
			else {
				this.$SetRegistrationForType(type, new $JSLib_$Util_IocContainer$TypeRegistration.$ctor1(implementationType, instance));
			}
		},
		$Resolve: function(type) {
			return this.$GetImplementationForType(type, this.$GetRegistrationForType(type));
		},
		$GetImplementationForType: function(type, TypeRegistration) {
			var instance = null;
			var TypeRegistrationImpl = this.$GetRegistrationForType(TypeRegistration.get_$ImplementationType());
			if (this.$debug_resolve) {
				ss.Debug.writeln(ss.formatString('trying to get implementation for type {0}', ss.getTypeFullName(type)));
			}
			// Singleton and have existing implementation instance
			if (TypeRegistration.get_$IsSingleton() && ss.isValue(TypeRegistration.get_$ImplementationInstance())) {
				return TypeRegistration.get_$ImplementationInstance();
			}
			// Singleton and implementation instance needs to be instantiated
			if (TypeRegistration.get_$IsSingleton()) {
				if (TypeRegistrationImpl.get_$IsSingleton()) {
					instance = TypeRegistrationImpl.get_$ImplementationInstance();
				}
				// can be null
				if (ss.isNullOrUndefined(instance)) {
					instance = this.$CreateInstanceForType(TypeRegistration.get_$ImplementationType(), TypeRegistrationImpl);
				}
				TypeRegistration.set_$ImplementationInstance(instance);
				return instance;
			}
			// Transient lifecycle
			return this.$CreateInstanceForType(TypeRegistration.get_$ImplementationType(), TypeRegistrationImpl);
		},
		$CreateInstanceForType: function(type, typeRegistration) {
			if (this.$debug_resolve) {
				ss.Debug.writeln(ss.formatString('\tcreating instance for type {0}', ss.getTypeName(type)));
			}
			var instance = null;
			// Check if a custom constructor function is registered. If so, then call it to create the object.
			// Any *constructor* injection is skipped. Otherwise get the constructor function, resolve the dependencies 
			// and create the object.
			if (!ss.staticEquals(typeRegistration.get_$Constructor(), null)) {
				instance = typeRegistration.get_$Constructor()();
			}
			else {
				var ctor = ss.getMembers(type, 1, 28)[0];
				// First constructor function
				var numberCtorParameters = (ss.isValue(ctor) ? ctor.params.length : 0);
				var parameterInstances = new Array(numberCtorParameters);
				if (this.$debug_resolve && numberCtorParameters > 0) {
					ss.Debug.writeln(ss.formatString('\t\ttype {0} has constructor dependencies: {1}', ss.getTypeFullName(type), Enumerable.from(ctor.params).select(function(x) {
						return ss.getTypeName(x);
					}).toJoinedString(', ')));
				}
				for (var i = 0; i < numberCtorParameters; i++) {
					parameterInstances[i] = this.$Resolve(ctor.params[i]);
				}
				instance = new (type.bind.apply(type, [type].concat(parameterInstances)));
			}
			if (typeRegistration.get_$IsSingleton()) {
				typeRegistration.set_$ImplementationInstance(instance);
			}
			// Property injection
			var properties = ss.getMembers(type, 16, 28);
			for (var i1 = 0; i1 < (ss.isValue(properties) ? Enumerable.from(properties).count() : 0); i1++) {
				ss.midel(properties[i1].setter, instance)(this.$Resolve(properties[i1].returnType));
			}
			if (this.$debug_resolve && ss.isValue(properties) && Enumerable.from(properties).count() > 0) {
				ss.Debug.writeln(ss.formatString('\t\ttype {0} has property dependencies: {1}', ss.getTypeFullName(type), Enumerable.from(properties).select(function(x1) {
					return x1.name;
				}).toJoinedString(', ')));
			}
			return instance;
		},
		Register: function(T) {
			return function() {
				this.$Register(T, T, false, null, null);
			};
		},
		Register$2: function(T) {
			return function(constructor) {
				this.$Register(T, T, false, constructor, null);
			};
		},
		Register$1: function(T, Timpl) {
			return function() {
				this.$Register(T, Timpl, false, null, null);
			};
		},
		Register$3: function(T, Timpl) {
			return function(constructor) {
				this.$Register(T, Timpl, false, constructor, null);
			};
		},
		RegisterAsSingle: function(T) {
			return function() {
				this.RegisterAsSingle$1(T, T).call(this);
			};
		},
		RegisterAsSingle$4: function(T) {
			return function(instance) {
				this.RegisterAsSingle$5(T, T).call(this, instance);
			};
		},
		RegisterAsSingle$2: function(T) {
			return function(constructor) {
				this.RegisterAsSingle$3(T, T).call(this, constructor);
			};
		},
		RegisterAsSingle$1: function(T, Timpl) {
			return function() {
				this.$Register(T, Timpl, true, null, null);
			};
		},
		RegisterAsSingle$5: function(T, Timpl) {
			return function(instance) {
				this.$Register(T, Timpl, true, null, instance);
			};
		},
		RegisterAsSingle$3: function(T, Timpl) {
			return function(constructor) {
				this.$Register(T, Timpl, true, constructor, null);
			};
		},
		Resolve: function(T) {
			return function() {
				return ss.safeCast(this.$Resolve(T), T);
			};
		}
	});
	ss.initEnum($JSLib_Util_IocContainer$ObjectLifetime, $asm, { Single: 0, Transient: 1 });
	ss.initClass($JSLib_Util_IsScrollExtension, $asm, {});
	ss.initClass($JSLib_Util_SizeCalculator, $asm, {});
	ss.initClass($JSLib_Util_URL, $asm, {});
	ss.initClass($JSLib_WebSql_WebSqlExtension, $asm, {});
	ss.initClass($JSLib_Widget_BackArgs, $asm, {});
	ss.initInterface($JSLib_Widget_IBackEvent, $asm, { OnBackButton: null });
	ss.initClass($JSLib_Widget_Behavior_SortableBehavior, $asm, {});
	ss.initClass($JSLib_Widget_Common_CommonDialog, $asm, {
		add_OnOKClicked: function(value) {
			this.$3$OnOKClickedField = ss.delegateCombine(this.$3$OnOKClickedField, value);
		},
		remove_OnOKClicked: function(value) {
			this.$3$OnOKClickedField = ss.delegateRemove(this.$3$OnOKClickedField, value);
		},
		add_OnCanceled: function(value) {
			this.$3$OnCanceledField = ss.delegateCombine(this.$3$OnCanceledField, value);
		},
		remove_OnCanceled: function(value) {
			this.$3$OnCanceledField = ss.delegateRemove(this.$3$OnCanceledField, value);
		},
		$CommonDialog_OnLoad: function(sender, e) {
			this.btnOK.click(ss.mkdel(this, function(event) {
				if (!ss.staticEquals(this.$3$OnOKClickedField, null)) {
					this.$3$OnOKClickedField(null, null);
				}
			}));
			this.btnCancel.click(ss.mkdel(this, function(event1) {
				if (!ss.staticEquals(this.$3$OnCanceledField, null)) {
					this.$3$OnCanceledField(null, null);
				}
				else {
					this.Destroy();
				}
			}));
		},
		OnBackButton: function(args) {
			//按下后退铵键时，关闭此窗体
			this.btnCancel.click();
			//关闭此窗口后，不再继续执行后退操作
			args.IsContinueGoBack = false;
			this.Destroy();
		},
		Destroy: function() {
			this.$3$OnOKClickedField = null;
			$JSLib_Mobile_NavigationManager.UnRegisterBackControl(this);
			$JSLib_Controller_ControllerBase.prototype.Destroy.call(this);
		},
		Execute: function() {
			this.WriteLiteral('<div class="c-commonDialog">\r\n    <div class="c-dialog-usercontent">\r\n        <div class="c-dialog-title">\r\n            ');
			this.Write(this.Title);
			this.WriteLiteral('\r\n        </div>\r\n        <div class="c-dialog-message">\r\n            ');
			this.Write(this.Message);
			this.WriteLiteral('\n        </div>\n        <div class="c-dialog-toolbar">\n            <div class="c-dialog-toolbar-button" id="btnOK">\n                确认\n            </div>\n            <div class="c-dialog-toolbar-button" id="btnCancel">\n                取消\n            </div>\n        </div>\n    </div>\n\n</div>\n');
		},
		InitialComponent: function() {
			this.btnOK = this.pageObject.find('#btnOK');
			this.btnCancel = this.pageObject.find('#btnCancel');
		},
		DestoryComponent: function() {
			if (ss.isValue(this.btnOK)) {
				this.btnOK.unbind().off();
				this.btnOK = null;
			}
			if (ss.isValue(this.btnCancel)) {
				this.btnCancel.unbind().off();
				this.btnCancel = null;
			}
		}
	}, $JSLib_Controller_ControllerBase, [$JSLib_Widget_IBackEvent]);
	ss.initClass($JSLib_Widget_Common_DateRange, $asm, {});
	ss.initClass($JSLib_Widget_Panel_ModalPanel, $asm, {
		add_OnOKButtonClick: function(value) {
			this.$4$OnOKButtonClickField = ss.delegateCombine(this.$4$OnOKButtonClickField, value);
		},
		remove_OnOKButtonClick: function(value) {
			this.$4$OnOKButtonClickField = ss.delegateRemove(this.$4$OnOKButtonClickField, value);
		},
		get_ContainerClass: function() {
			return this.$4$ContainerClassField;
		},
		set_ContainerClass: function(value) {
			this.$4$ContainerClassField = value;
		},
		AfterBuildController: function() {
			this.HtmlContent = (new $JSLib_Widget_Panel_ModalPanelTemplate()).GetModalPanelTemplate(this.HtmlContent, this.ShowOKButton, this.ShowCancelButton, this.Title, this.get_ContainerClass()).toString();
			this.Content = $(this.HtmlContent);
		},
		$ModalPanel_OnLoad: function(sender, e) {
			this.Content.click(ss.thisFix(ss.mkdel(this, function(elem, event) {
				if (event.target.className.indexOf('c-modal-window') !== -1) {
					if (!ss.staticEquals(this.$4$OnModalWindowClickField, null)) {
						this.$4$OnModalWindowClickField(null, null);
					}
					else if (this.CloseOnModalBackClick) {
						this.Destroy();
					}
				}
			})));
			if (this.ShowCancelButton) {
				this.Content.find('#btnCancel').click(ss.mkdel(this, function(event1) {
					this.Destroy();
				}));
			}
			if (this.ShowOKButton) {
				this.Content.find('#btnOK').click(ss.mkdel(this, function(event2) {
					if (!ss.staticEquals(this.$4$OnOKButtonClickField, null)) {
						this.$4$OnOKButtonClickField(this, null);
					}
				}));
			}
			$JSLib_Widget_Panel_ModalPanel.modalPanles.push(this);
		},
		Destroy: function() {
			ss.remove($JSLib_Widget_Panel_ModalPanel.modalPanles, this);
			$JSLib_Controller_ControllerBase.prototype.Destroy.call(this);
		},
		add_OnModalWindowClick: function(value) {
			this.$4$OnModalWindowClickField = ss.delegateCombine(this.$4$OnModalWindowClickField, value);
		},
		remove_OnModalWindowClick: function(value) {
			this.$4$OnModalWindowClickField = ss.delegateRemove(this.$4$OnModalWindowClickField, value);
		},
		ShowAsGlobalModalPanel: function() {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1;
			var $sm = ss.mkdel(this, function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								$t1 = $JSLib_Controller_Panel.FromElement($('body')[0]).AddChild(this, false, false, null);
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								$t1.getAwaitedResult();
								$tcs.setResult(this);
								return;
							}
							default: {
								break $sm1;
							}
						}
					}
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			});
			$sm();
			return $tcs.task;
		},
		ShowModalPanelTo: function(page) {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), $t1;
			var $sm = ss.mkdel(this, function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								$t1 = $JSLib_Controller_Panel.FromElement(page.pageObject[0]).AddChild(this, true, false, page);
								$state = 1;
								$t1.continueWith($sm);
								return;
							}
							case 1: {
								$state = -1;
								$t1.getAwaitedResult();
								$tcs.setResult(this);
								return;
							}
							default: {
								break $sm1;
							}
						}
					}
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			});
			$sm();
			return $tcs.task;
		}
	}, $JSLib_Controller_UIController);
	ss.initClass($JSLib_Widget_Common_DateRangeSelector, $asm, {
		$DateRangeSelector_OnOKButtonClick: function(sender, e) {
			var dateRange = new $JSLib_Widget_Common_DateRange();
			dateRange.BeginDate = new Date($CoreEx.ToInt(this.lstYearFrom.val(), 0), $CoreEx.ToInt(this.lstMonthFrom.val(), 0) - 1, $CoreEx.ToInt(this.lstDayFrom.val(), 0));
			dateRange.EndDate = new Date($CoreEx.ToInt(this.lstYearTo.val(), 0), $CoreEx.ToInt(this.lstMonthTo.val(), 0) - 1, $CoreEx.ToInt(this.lstDayTo.val(), 0));
			if (!ss.staticEquals(this.OnDateRrangeSelected, null)) {
				this.OnDateRrangeSelected(dateRange);
			}
		},
		OnBackButton: function(args) {
			this.Destroy();
			args.IsContinueGoBack = false;
		},
		GetAvaiableDays: function(year, month) {
			var monthMaxDay = new Date(year, month - 1, 1);
			var data = [];
			while (monthMaxDay.getFullYear() === year && monthMaxDay.getMonth() + 1 === month) {
				data.push(monthMaxDay.getDate());
				monthMaxDay = new Date(monthMaxDay.valueOf() + Math.round(1 * 86400000));
			}
			return data;
		},
		Destroy: function() {
			$JSLib_Mobile_NavigationManager.UnRegisterBackControl(this);
			$JSLib_Widget_Panel_ModalPanel.prototype.Destroy.call(this);
		},
		GetSelectItemsHtml: function(values) {
			return new $System_Web_WebPages_HelperResult(ss.mkdel(this, function(__razor_helper_writer) {
				for (var $t1 = 0; $t1 < values.length; $t1++) {
					var item = values[$t1];
					var itemStr = ((item < 100) ? ss.formatNumber(item, 'd2') : ss.formatNumber(item, 'd4'));
					this.WriteLiteralTo(__razor_helper_writer, '        <option value="');
					this.WriteTo(__razor_helper_writer, item);
					this.WriteLiteralTo(__razor_helper_writer, '">');
					this.WriteTo(__razor_helper_writer, itemStr);
					this.WriteLiteralTo(__razor_helper_writer, '</option>\r\n');
				}
			}));
		},
		Execute: function() {
			this.WriteLiteral('<div class="c-DateRangeSelector">\r\n    <div class="DateArea" id="pnlDateAreaFrom">\r\n        <div class="daterangeLabel">从:</div>\r\n        <div class="selectItem Year">\r\n            <select id="lstYearFrom">\r\n                ');
			this.Write(this.GetSelectItemsHtml(Enumerable.range(this.MinDate.getFullYear(), this.MaxDate.getFullYear() - this.MinDate.getFullYear() + 1).toArray()));
			this.WriteLiteral('\r\n            </select>\r\n            <label>年</label>\r\n        </div>\r\n        <div class="selectItem Month">\r\n            <select id="lstMonthFrom">\r\n');
			for (var i = 1; i <= 12; i++) {
				this.WriteLiteral('                    <option value="');
				this.Write(i);
				this.WriteLiteral('">');
				this.Write(ss.formatNumber(i, 'd2'));
				this.WriteLiteral('</option>\r\n');
			}
			this.WriteLiteral('            </select>\n            <label>月</label>\n        </div>\n        <div class="selectItem Day">\n            <select id="lstDayFrom"></select>\n            <label>日</label>\n        </div>\n    </div>\n    <div class="DateArea" id="pnlDateAreaTo">\n        <div class="daterangeLabel">至:</div>\n        <div class="selectItem Year">\n            <select id="lstYearTo">\n                ');
			this.Write(this.GetSelectItemsHtml(Enumerable.range(this.MinDate.getFullYear(), this.MaxDate.getFullYear() - this.MinDate.getFullYear() + 1).toArray()));
			this.WriteLiteral('\r\n            </select>\r\n            <label>年</label>\r\n        </div>\r\n        <div class="selectItem Month">\r\n            <select id="lstMonthTo">\r\n');
			for (var i1 = 1; i1 <= 12; i1++) {
				this.WriteLiteral('                    <option value="');
				this.Write(i1);
				this.WriteLiteral('">');
				this.Write(ss.formatNumber(i1, 'd2'));
				this.WriteLiteral('</option>\r\n');
			}
			this.WriteLiteral('            </select>\r\n            <label>月</label>\r\n        </div>\r\n        <div class="selectItem Day">\r\n            <select id="lstDayTo"></select>\r\n            <label>日</label>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n');
		},
		InitialComponent: function() {
			this.pnlDateAreaFrom = this.pageObject.find('#pnlDateAreaFrom');
			this.lstYearFrom = this.pageObject.find('#lstYearFrom');
			this.lstMonthFrom = this.pageObject.find('#lstMonthFrom');
			this.lstDayFrom = this.pageObject.find('#lstDayFrom');
			this.pnlDateAreaTo = this.pageObject.find('#pnlDateAreaTo');
			this.lstYearTo = this.pageObject.find('#lstYearTo');
			this.lstMonthTo = this.pageObject.find('#lstMonthTo');
			this.lstDayTo = this.pageObject.find('#lstDayTo');
		},
		DestoryComponent: function() {
			if (ss.isValue(this.pnlDateAreaFrom)) {
				this.pnlDateAreaFrom.unbind().off();
				this.pnlDateAreaFrom = null;
			}
			if (ss.isValue(this.lstYearFrom)) {
				this.lstYearFrom.unbind().off();
				this.lstYearFrom = null;
			}
			if (ss.isValue(this.lstMonthFrom)) {
				this.lstMonthFrom.unbind().off();
				this.lstMonthFrom = null;
			}
			if (ss.isValue(this.lstDayFrom)) {
				this.lstDayFrom.unbind().off();
				this.lstDayFrom = null;
			}
			if (ss.isValue(this.pnlDateAreaTo)) {
				this.pnlDateAreaTo.unbind().off();
				this.pnlDateAreaTo = null;
			}
			if (ss.isValue(this.lstYearTo)) {
				this.lstYearTo.unbind().off();
				this.lstYearTo = null;
			}
			if (ss.isValue(this.lstMonthTo)) {
				this.lstMonthTo.unbind().off();
				this.lstMonthTo = null;
			}
			if (ss.isValue(this.lstDayTo)) {
				this.lstDayTo.unbind().off();
				this.lstDayTo = null;
			}
		}
	}, $JSLib_Widget_Panel_ModalPanel, [$JSLib_Widget_IBackEvent]);
	ss.initClass($JSLib_Widget_Common_DateTimeSelector, $asm, {
		add_OnChooseDateTimeCompleted: function(value) {
			this.$5$OnChooseDateTimeCompletedField = ss.delegateCombine(this.$5$OnChooseDateTimeCompletedField, value);
		},
		remove_OnChooseDateTimeCompleted: function(value) {
			this.$5$OnChooseDateTimeCompletedField = ss.delegateRemove(this.$5$OnChooseDateTimeCompletedField, value);
		},
		$DateTimeSelector_OnOKButtonClick: function(sender, e) {
			this.CurrentDateTime = new Date($CoreEx.ToInt(this.lstYear.val(), 0), $CoreEx.ToInt(this.lstMonth.val(), 0) - 1, $CoreEx.ToInt(this.lstDay.val(), 0), $CoreEx.ToInt(this.lstHour.val(), 0), $CoreEx.ToInt(this.lstMinute.val(), 0), $CoreEx.ToInt(this.lstSecond.val(), 0));
			if (!ss.staticEquals(this.$5$OnChooseDateTimeCompletedField, null)) {
				this.$5$OnChooseDateTimeCompletedField(null, null);
			}
		},
		$DateTimeSelector_OnLoad: function(sender, e) {
			if (!this.Para.EnalbeClean) {
				this.btnClear.hide();
			}
			this.btnOK.click(ss.mkdel(this, function(event) {
				this.$DateTimeSelector_OnOKButtonClick(null, null);
			}));
			this.btnCancel.click(ss.mkdel(this, function(event1) {
				this.Destroy();
			}));
			this.btnClear.click(ss.mkdel(this, function(event2) {
				this.CurrentDateTime = null;
				if (!ss.staticEquals(this.$5$OnChooseDateTimeCompletedField, null)) {
					this.$5$OnChooseDateTimeCompletedField(null, null);
				}
			}));
			$jQueryObjectEx.SetVisible(this.pnlDateArea, this.Para.ShowDate);
			$jQueryObjectEx.SetVisible(this.pnlTimeArea, this.Para.ShowTime);
			this.lstMonth.change(ss.mkdel(this, function(event3) {
				this.lstDay.html(this.GetSelectItemsHtml(this.GetAvaiableDays()).toString());
				this.lstDay.val('1');
			}));
			this.lstYear.change(ss.mkdel(this, function(event4) {
				this.lstMonth.val('1');
				this.lstMonth.change();
			}));
			this.lstYear.html(this.GetSelectItemsHtml(this.GetAvaiableYears()).toString());
			this.lstYear.val(ss.unbox(this.Para.get_DefaultDate()).getFullYear().toString());
			this.lstMonth.val((ss.unbox(this.Para.get_DefaultDate()).getMonth() + 1).toString());
			this.lstMonth.change();
			this.lstDay.val(ss.unbox(this.Para.get_DefaultDate()).getDate().toString());
			this.lstHour.val(ss.unbox(this.Para.get_DefaultDate()).getHours().toString());
			this.lstMinute.val(ss.unbox(this.Para.get_DefaultDate()).getMinutes().toString());
			this.lstSecond.val(ss.unbox(this.Para.get_DefaultDate()).getSeconds().toString());
		},
		GetAvaiableYears: function() {
			var data = [];
			for (var i = ss.unbox(this.Para.get_MinDate()).getFullYear(); i <= ss.unbox(this.Para.get_MaxDate()).getFullYear(); i++) {
				data.push(i);
			}
			return data;
		},
		GetAvaiableDays: function() {
			var year = $CoreEx.ToInt(this.lstYear.val(), 0);
			var month = $CoreEx.ToInt(this.lstMonth.val(), 0);
			var monthMaxDay = new Date(year, month - 1, 1);
			var data = [];
			while (monthMaxDay.getFullYear() === year && monthMaxDay.getMonth() + 1 === month) {
				data.push(monthMaxDay.getDate());
				monthMaxDay = new Date(monthMaxDay.valueOf() + Math.round(1 * 86400000));
			}
			return data;
		},
		OnBackButton: function(args) {
			this.Destroy();
			args.IsContinueGoBack = false;
		},
		Destroy: function() {
			$JSLib_Mobile_NavigationManager.UnRegisterBackControl(this);
			$JSLib_Widget_Panel_ModalPanel.prototype.Destroy.call(this);
		},
		GetSelectItemsHtml: function(values) {
			return new $System_Web_WebPages_HelperResult(ss.mkdel(this, function(__razor_helper_writer) {
				for (var $t1 = 0; $t1 < values.length; $t1++) {
					var item = values[$t1];
					var itemStr = ((item < 100) ? ss.formatNumber(item, 'd2') : ss.formatNumber(item, 'd4'));
					this.WriteLiteralTo(__razor_helper_writer, '        <option value="');
					this.WriteTo(__razor_helper_writer, item);
					this.WriteLiteralTo(__razor_helper_writer, '">');
					this.WriteTo(__razor_helper_writer, itemStr);
					this.WriteLiteralTo(__razor_helper_writer, '</option>\r\n');
				}
			}));
		},
		Execute: function() {
			this.WriteLiteral('<div class="c-DateTimeSelector">\n\n    <div class="DateArea" id="pnlDateArea">\n        <div class="selectItem Year">\n\n            <select id="lstYear"></select>\n            <label>年</label>\n        </div>\n        <div class="selectItem Month">\n            <select id="lstMonth">\n');
			for (var i = 1; i <= 12; i++) {
				this.WriteLiteral('                    <option value="');
				this.Write(i);
				this.WriteLiteral('">');
				this.Write(ss.formatNumber(i, 'd2'));
				this.WriteLiteral('</option>\r\n');
			}
			this.WriteLiteral('            </select>\n            <label>月</label>\n        </div>\n        <div class="selectItem Day">\n            <select id="lstDay"></select>\n            <label>日</label>\n        </div>\n    </div>\n\n\n    <div class="TimeArea" id="pnlTimeArea">\n        <div class="selectItem Hour">\n\n            <select id="lstHour">\n');
			for (var i1 = 0; i1 < 24; i1++) {
				this.WriteLiteral('                    <option value="');
				this.Write(i1);
				this.WriteLiteral('">');
				this.Write(ss.formatNumber(i1, 'd2'));
				this.WriteLiteral('</option>\r\n');
			}
			this.WriteLiteral('            </select>\r\n            <label>:</label>\r\n        </div>\r\n        <div class="selectItem Minute">\r\n\r\n            <select id="lstMinute">\r\n');
			for (var i2 = 1; i2 < 60; i2++) {
				this.WriteLiteral('                    <option value="');
				this.Write(i2);
				this.WriteLiteral('">');
				this.Write(ss.formatNumber(i2, 'd2'));
				this.WriteLiteral('</option>\r\n');
			}
			this.WriteLiteral('            </select>\r\n            <label>:</label>\r\n        </div>\r\n        <div class="selectItem Second">\r\n\r\n            <select id="lstSecond">\r\n');
			for (var i3 = 1; i3 < 60; i3++) {
				this.WriteLiteral('                    <option value="');
				this.Write(i3);
				this.WriteLiteral('">');
				this.Write(ss.formatNumber(i3, 'd2'));
				this.WriteLiteral('</option>\r\n');
			}
			this.WriteLiteral('            </select>\n            <label>&nbsp;</label>\n\n        </div>\n    </div>\n\n\n    <div class="c-modal-toolbar">\n        <input type="button" value="确认" id="btnOK" class="c-btn c-modal-button">\n        <input type="button" value="清空" id="btnClear" class="c-btn c-modal-button">\n        <input type="button" value="取消" id="btnCancel" class="c-btn c-modal-button">\n    </div>\n</div>\n\n');
		},
		InitialComponent: function() {
			this.pnlDateArea = this.pageObject.find('#pnlDateArea');
			this.lstYear = this.pageObject.find('#lstYear');
			this.lstMonth = this.pageObject.find('#lstMonth');
			this.lstDay = this.pageObject.find('#lstDay');
			this.pnlTimeArea = this.pageObject.find('#pnlTimeArea');
			this.lstHour = this.pageObject.find('#lstHour');
			this.lstMinute = this.pageObject.find('#lstMinute');
			this.lstSecond = this.pageObject.find('#lstSecond');
			this.btnOK = this.pageObject.find('#btnOK');
			this.btnClear = this.pageObject.find('#btnClear');
			this.btnCancel = this.pageObject.find('#btnCancel');
		},
		DestoryComponent: function() {
			if (ss.isValue(this.pnlDateArea)) {
				this.pnlDateArea.unbind().off();
				this.pnlDateArea = null;
			}
			if (ss.isValue(this.lstYear)) {
				this.lstYear.unbind().off();
				this.lstYear = null;
			}
			if (ss.isValue(this.lstMonth)) {
				this.lstMonth.unbind().off();
				this.lstMonth = null;
			}
			if (ss.isValue(this.lstDay)) {
				this.lstDay.unbind().off();
				this.lstDay = null;
			}
			if (ss.isValue(this.pnlTimeArea)) {
				this.pnlTimeArea.unbind().off();
				this.pnlTimeArea = null;
			}
			if (ss.isValue(this.lstHour)) {
				this.lstHour.unbind().off();
				this.lstHour = null;
			}
			if (ss.isValue(this.lstMinute)) {
				this.lstMinute.unbind().off();
				this.lstMinute = null;
			}
			if (ss.isValue(this.lstSecond)) {
				this.lstSecond.unbind().off();
				this.lstSecond = null;
			}
			if (ss.isValue(this.btnOK)) {
				this.btnOK.unbind().off();
				this.btnOK = null;
			}
			if (ss.isValue(this.btnClear)) {
				this.btnClear.unbind().off();
				this.btnClear = null;
			}
			if (ss.isValue(this.btnCancel)) {
				this.btnCancel.unbind().off();
				this.btnCancel = null;
			}
		}
	}, $JSLib_Widget_Panel_ModalPanel, [$JSLib_Widget_IBackEvent]);
	ss.initClass($JSLib_Widget_Common_DateTimeTarget, $asm, {
		get_DefaultDate: function() {
			if (ss.Nullable$1.gt(this.$_defaultDate, this.get_MaxDate())) {
				this.$_defaultDate = this.get_MaxDate();
			}
			if (ss.Nullable$1.lt(this.$_defaultDate, this.get_MinDate())) {
				this.$_defaultDate = this.get_MinDate();
			}
			return this.$_defaultDate;
		},
		set_DefaultDate: function(value) {
			if (ss.staticEquals(value, null) || isNaN(ss.unbox(value))) {
				this.$_defaultDate = new Date();
			}
			else {
				this.$_defaultDate = value;
			}
		},
		get_MaxDate: function() {
			if (ss.staticEquals(this.$_MaxDate, null)) {
				return new Date(2999, 1 - 1, 1);
			}
			return this.$_MaxDate;
		},
		get_MinDate: function() {
			if (ss.staticEquals(this.$_MinDate, null)) {
				return new Date(1, 1 - 1, 1);
			}
			return this.$_MinDate;
		}
	});
	ss.initClass($JSLib_Widget_Common_LoadingDialog, $asm, {
		$LoadingDialog_OnLoad: function(sender, e) {
			if (this.pageObject.parent().length > 0) {
				var position = window.getComputedStyle(this.pageObject.parent()[0]).position;
				if (position !== 'relative' && position !== 'absolute') {
					this.pageObject.parent()[0].style.position = 'relative';
				}
			}
		},
		CloseLoading: function() {
			this.Destroy();
		},
		Execute: function() {
			this.WriteLiteral('<div class="c-n-loaingPanel" style="z-index:');
			this.Write($JSLib_Widget_Common_LoadingDialog.CurrentZIndex++);
			this.WriteLiteral('">\r\n    <img src="img/ajax-loader.gif" class="c-loadingGIF">\r\n</div>');
		},
		InitialComponent: function() {
		},
		DestoryComponent: function() {
		}
	}, $JSLib_Controller_ControllerBase);
	ss.initClass($JSLib_Widget_Pager_PageEventArgs, $asm, {}, ss.EventArgs);
	ss.initClass($JSLib_Widget_Pager_SimplePager, $asm, {
		add_OnPageIndexChanged: function(value) {
			this.$1$OnPageIndexChangedField = ss.delegateCombine(this.$1$OnPageIndexChangedField, value);
		},
		remove_OnPageIndexChanged: function(value) {
			this.$1$OnPageIndexChangedField = ss.delegateRemove(this.$1$OnPageIndexChangedField, value);
		},
		get_PageIndex: function() {
			return this.$pageIndex;
		},
		set_PageIndex: function(value) {
			if (value < 0) {
				this.$pageIndex = 0;
			}
			else if (value <= this.get_TotalPageCount() - 1) {
				this.$pageIndex = value;
			}
		},
		get_PageSize: function() {
			return this.$pageSize;
		},
		set_PageSize: function(value) {
			this.$pageSize = ((value < 1) ? 1 : value);
		},
		get_TotalRecordCount: function() {
			return this.$1$TotalRecordCountField;
		},
		set_TotalRecordCount: function(value) {
			this.$1$TotalRecordCountField = value;
		},
		get_TotalPageCount: function() {
			var pagecount = ss.Int32.div(this.get_TotalRecordCount(), this.get_PageSize());
			return ((pagecount > 0) ? (pagecount + 1) : pagecount);
		},
		$Control_Load: function(e) {
			//jQuery.Select("a", rootElement).Attribute("href", "javascript:void(0)");
			this.ReDisplay();
			this.btnPrePage.click(ss.mkdel(this, function(ex) {
				var temp = this.get_PageIndex();
				this.set_PageIndex(this.get_PageIndex() - 1);
				if (this.get_PageIndex() === temp) {
					return;
				}
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
			this.btnNext.click(ss.mkdel(this, function(ex1) {
				var temp1 = this.get_PageIndex();
				this.set_PageIndex(this.get_PageIndex() + 1);
				if (this.get_PageIndex() === temp1) {
					return;
				}
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
		},
		ReDisplay: function() {
			this.lblPageAll.text(ss.formatString('  ( {0}/{1} )', this.get_PageIndex() + 1, this.get_TotalPageCount()));
		},
		GotoPage: function(pageIndex) {
			this.set_PageIndex(pageIndex);
			this.ReDisplay();
		},
		add_$OnLoad: function(value) {
			this.$1$OnLoadField = ss.delegateCombine(this.$1$OnLoadField, value);
		},
		remove_$OnLoad: function(value) {
			this.$1$OnLoadField = ss.delegateRemove(this.$1$OnLoadField, value);
		},
		AddToParent: function(parentContainer) {
			parentContainer.append(this.$rootElement);
			this.$InitialComponent();
			if (!ss.staticEquals(this.$1$OnLoadField, null)) {
				this.$1$OnLoadField(null);
			}
			return parentContainer;
		},
		$InitialComponent: function() {
			this.lblPageAll = $('#lblPageAll', this.$rootElement);
			this.btnPrePage = $('#btnPrePage', this.$rootElement);
			this.btnNext = $('#btnNext', this.$rootElement);
		}
	});
	ss.initClass($JSLib_Widget_Pager_WideSelectPager, $asm, {
		add_OnPageIndexChanged: function(value) {
			this.$1$OnPageIndexChangedField = ss.delegateCombine(this.$1$OnPageIndexChangedField, value);
		},
		remove_OnPageIndexChanged: function(value) {
			this.$1$OnPageIndexChangedField = ss.delegateRemove(this.$1$OnPageIndexChangedField, value);
		},
		get_PageIndex: function() {
			return this.$pageIndex;
		},
		set_PageIndex: function(value) {
			if (value < 0) {
				this.$pageIndex = 0;
			}
			else if (value <= this.get_TotalPageCount() - 1) {
				this.$pageIndex = value;
			}
		},
		get_PageSize: function() {
			return this.$pageSize;
		},
		set_PageSize: function(value) {
			if (value < 1) {
				this.$pageSize = 1;
			}
			else {
				this.$pageSize = value;
			}
		},
		get_TotalRecordCount: function() {
			return this.$totalRecordCount;
		},
		set_TotalRecordCount: function(value) {
			this.$totalRecordCount = value;
		},
		get_TotalPageCount: function() {
			if (this.get_TotalRecordCount() % this.get_PageSize() > 0) {
				return ss.Int32.div(this.get_TotalRecordCount(), this.get_PageSize()) + 1;
			}
			return ss.Int32.div(this.get_TotalRecordCount(), this.get_PageSize());
		},
		$Control_Load: function(e) {
			//jQuery.Select("a", rootElement).Attribute("href", "javascript:void(0)");
			this.ReDisplay();
			this.btnPrePage.click(ss.mkdel(this, function(ex) {
				var temp = this.get_PageIndex();
				this.set_PageIndex(this.get_PageIndex() - 1);
				if (this.get_PageIndex() === temp) {
					return;
				}
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
			this.btnNext.click(ss.mkdel(this, function(ex1) {
				var temp1 = this.get_PageIndex();
				this.set_PageIndex(this.get_PageIndex() + 1);
				if (this.get_PageIndex() === temp1) {
					return;
				}
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
			this.btnPre1.click(ss.mkdel(this, function(ex2) {
				this.set_PageIndex(this.get_PageIndex() - 2);
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
			this.btnPre2.click(ss.mkdel(this, function(ex3) {
				this.set_PageIndex(this.get_PageIndex() - 1);
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
			this.btnNext1.click(ss.mkdel(this, function(ex4) {
				this.set_PageIndex(this.get_PageIndex() + 1);
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
			this.btnNext2.click(ss.mkdel(this, function(ex5) {
				this.set_PageIndex(this.get_PageIndex() + 2);
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
			this.btnFirst.click(ss.mkdel(this, function(ex6) {
				this.set_PageIndex(0);
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
			this.btnEnd.click(ss.mkdel(this, function(ex7) {
				this.set_PageIndex(this.get_TotalPageCount() - 1);
				this.ReDisplay();
				if (!ss.staticEquals(this.$1$OnPageIndexChangedField, null)) {
					this.$1$OnPageIndexChangedField(this, new $JSLib_Widget_Pager_PageEventArgs());
				}
			}));
		},
		ReDisplay: function() {
			this.btnCurrent.text((this.get_PageIndex() + 1).toString());
			this.btnPre1.text((this.get_PageIndex() + 1 - 2).toString());
			this.btnPre2.text((this.get_PageIndex() + 1 - 1).toString());
			this.btnNext1.text((this.get_PageIndex() + 1 + 1).toString());
			this.btnNext2.text((this.get_PageIndex() + 1 + 2).toString());
			this.btnFirst.text('1');
			this.btnEnd.text(this.get_TotalPageCount().toString());
			this.lblRecoredAll.text(this.$totalRecordCount.toString());
			this.$LoadNearButton();
			if (this.get_TotalRecordCount() <= this.$pageSize) {
				this.$rootElement.hide();
			}
			else {
				this.$rootElement.show();
			}
			this.$ShowBtnFirst();
			this.$ShowBtnEnd();
			this.$ShowPreDot();
			this.$ShowBtnPre1();
			this.$ShowBtnPre2();
			this.$ShowBtnNext2();
			this.$ShowBtnNextDot();
			this.$ShowBtnNext1();
		},
		$ShowBtnFirst: function() {
			if (this.get_PageIndex() === 0 || this.get_PageIndex() === 1 || this.get_PageIndex() === 2) {
				this.btnFirst.hide();
			}
			else {
				this.btnFirst.show();
			}
		},
		$ShowBtnEnd: function() {
			if (this.get_PageIndex() === this.get_TotalPageCount() - 1 || this.get_PageIndex() === this.get_TotalPageCount() - 2 || this.get_PageIndex() === this.get_TotalPageCount() - 3) {
				this.btnEnd.hide();
			}
			else {
				this.btnEnd.show();
			}
		},
		$ShowPreDot: function() {
			if (this.get_PageIndex() === 0 || this.get_PageIndex() === 1 || this.get_PageIndex() === 2) {
				this.btnPreDot.hide();
			}
			else {
				this.btnPreDot.show();
			}
		},
		$ShowBtnPre1: function() {
			if (this.get_PageIndex() === 0 || this.get_PageIndex() === 1) {
				this.btnPre1.hide();
			}
			else {
				this.btnPre1.show();
			}
		},
		$ShowBtnPre2: function() {
			if (this.get_PageIndex() === 0) {
				this.btnPre2.hide();
			}
			else {
				this.btnPre2.show();
			}
		},
		$ShowBtnNext2: function() {
			if (this.get_PageIndex() === this.get_TotalPageCount() - 1 || this.get_PageIndex() === this.get_TotalPageCount() - 2) {
				this.btnNext2.hide();
			}
			else {
				this.btnNext2.show();
			}
		},
		$ShowBtnNextDot: function() {
			if (this.get_PageIndex() === this.get_TotalPageCount() - 1 || this.get_PageIndex() === this.get_TotalPageCount() - 2 || this.get_PageIndex() === this.get_TotalPageCount() - 3) {
				this.btnNextDot.hide();
			}
			else {
				this.btnNextDot.show();
			}
		},
		$ShowBtnNext1: function() {
			if (this.get_PageIndex() === this.get_TotalPageCount() - 1) {
				this.btnNext1.hide();
			}
			else {
				this.btnNext1.show();
			}
		},
		$LoadNearButton: function() {
			this.$ShowPreDot();
			this.$ShowBtnPre1();
			this.$ShowBtnPre2();
			this.$ShowBtnNextDot();
			this.$ShowBtnNext1();
			this.$ShowBtnNext2();
			this.$ShowBtnFirst();
			this.$ShowBtnEnd();
		},
		GotoPage: function(pageIndex) {
			this.set_PageIndex(pageIndex);
			this.ReDisplay();
		},
		add_$OnLoad: function(value) {
			this.$1$OnLoadField = ss.delegateCombine(this.$1$OnLoadField, value);
		},
		remove_$OnLoad: function(value) {
			this.$1$OnLoadField = ss.delegateRemove(this.$1$OnLoadField, value);
		},
		AddToParent: function(parentContainer) {
			parentContainer.append(this.$rootElement);
			this.$InitialComponent();
			if (!ss.staticEquals(this.$1$OnLoadField, null)) {
				this.$1$OnLoadField(null);
			}
			return parentContainer;
		},
		$InitialComponent: function() {
			this.lblRecoredAll = $('#lblRecoredAll', this.$rootElement);
			this.btnPrePage = $('#btnPrePage', this.$rootElement);
			this.btnFirst = $('#btnFirst', this.$rootElement);
			this.btnPreDot = $('#btnPreDot', this.$rootElement);
			this.btnPre1 = $('#btnPre1', this.$rootElement);
			this.btnPre2 = $('#btnPre2', this.$rootElement);
			this.btnCurrent = $('#btnCurrent', this.$rootElement);
			this.btnNext1 = $('#btnNext1', this.$rootElement);
			this.btnNext2 = $('#btnNext2', this.$rootElement);
			this.btnNextDot = $('#btnNextDot', this.$rootElement);
			this.btnEnd = $('#btnEnd', this.$rootElement);
			this.btnNext = $('#btnNext', this.$rootElement);
		}
	});
	ss.initClass($JSLib_Widget_Panel_ModalPanelTemplate, $asm, {
		GetModalPanelTemplate: function(content, showOKButton, showCancelButton, title, ContainerClass) {
			return new $System_Web_WebPages_HelperResult(ss.mkdel(this, function(__razor_helper_writer) {
				this.WriteLiteralTo(__razor_helper_writer, "    <div class='c-modal-window ");
				this.WriteTo(__razor_helper_writer, ContainerClass);
				this.WriteLiteralTo(__razor_helper_writer, "' >\r\n        <div class='c-modal-content'>\r\n            <div class=\"c-modal-usercontent\">\r\n\r\n");
				if (!ss.isNullOrEmptyString(title)) {
					this.WriteLiteralTo(__razor_helper_writer, '                    <div class="c-modal-title">\r\n                        ');
					this.WriteTo(__razor_helper_writer, title);
					this.WriteLiteralTo(__razor_helper_writer, '\r\n                    </div>\r\n');
				}
				this.WriteLiteralTo(__razor_helper_writer, '\r\n                ');
				this.WriteTo(__razor_helper_writer, content);
				this.WriteLiteralTo(__razor_helper_writer, '\r\n');
				if (showOKButton || showCancelButton) {
					this.WriteLiteralTo(__razor_helper_writer, "                    <div class='c-modal-toolbar'>\r\n");
					if (showOKButton) {
						this.WriteLiteralTo(__razor_helper_writer, "                            <input type='button' value='确认' id='btnOK' class='c-btn c-modal-button' />\r\n");
					}
					this.WriteLiteralTo(__razor_helper_writer, '\r\n');
					if (showCancelButton) {
						this.WriteLiteralTo(__razor_helper_writer, "                            <input type='button' value='取消' id='btnCancel' class='c-btn c-modal-button' />\r\n");
					}
					this.WriteLiteralTo(__razor_helper_writer, '                    </div>\r\n');
				}
				this.WriteLiteralTo(__razor_helper_writer, '            </div>\r\n\r\n        </div>\r\n\r\n    </div>\r\n');
			}));
		},
		Execute: function() {
			this.WriteLiteral('\r\n');
		}
	}, $JSLib_Controller_RazorTemplate);
	ss.initClass($JSLib_Widget_SwipeView_SwipeView, $asm, {
		add_OnSwipeThredEnd: function(value) {
			this.$1$OnSwipeThredEndField = ss.delegateCombine(this.$1$OnSwipeThredEndField, value);
		},
		remove_OnSwipeThredEnd: function(value) {
			this.$1$OnSwipeThredEndField = ss.delegateRemove(this.$1$OnSwipeThredEndField, value);
		},
		$pos: function(x) {
			this.$target[0].style.webkitTransform = 'translate3d(' + x + 'px,0,0)';
		},
		HideTip: function() {
			if (ss.isValue(this.$lblMessage)) {
				this.$lblMessage.hide();
			}
		}
	});
	ss.initEnum($JSLib_Widget_SwipeView_SwipViewDirection, $asm, { Left: 1, Right: 2 });
	ss.initClass($JSLib_Widget_SwipeView_SwipViewEventArgs, $asm, {}, ss.EventArgs);
	ss.initInterface($JSLib_Widget_ViewPager_IViewPagerItem, $asm, { GetController: null, Dispose: null });
	ss.initClass($JSLib_Widget_ViewPager_ViewPageItem, $asm, {}, ss.makeGenericType($JSLib_Widget_ViewPager_ViewPageItem$1, [Object]), [$JSLib_Widget_ViewPager_IViewPagerItem]);
	$JSLib_Widget_ViewPager_ViewPageItem.$ctor1.prototype = $JSLib_Widget_ViewPager_ViewPageItem.prototype;
	ss.initClass($Phone_RazorMobilePage, $asm, {
		get_PrepareErrroInfo: function() {
			return this.$3$PrepareErrroInfoField;
		},
		set_PrepareErrroInfo: function(value) {
			this.$3$PrepareErrroInfoField = value;
		},
		PreparePage: function() {
			var $state = 0, $tcs = new ss.TaskCompletionSource(), content, prepareTask, ex, errorInfo;
			var $sm = ss.mkdel(this, function() {
				try {
					$sm1:
					for (;;) {
						switch ($state) {
							case 0: {
								$state = -1;
								content = '';
								$state = 1;
								continue $sm1;
							}
							case 1:
							case 3:
							case 4:
							case 5: {
								if ($state === 1) {
									$state = 3;
								}
								try {
									$sm2:
									for (;;) {
										switch ($state) {
											case 3: {
												$state = -1;
												prepareTask = this.PrepareData();
												if (ss.isValue(prepareTask)) {
													$state = 5;
													prepareTask.continueWith($sm);
													return;
												}
												$state = 4;
												continue $sm2;
											}
											case 5: {
												$state = -1;
												prepareTask.getAwaitedResult();
												$state = 4;
												continue $sm2;
											}
											case 4: {
												$state = -1;
												this.Execute();
												content = this.contentBuilder.toString();
												$state = -1;
												break $sm2;
											}
											default: {
												break $sm2;
											}
										}
									}
								}
								catch ($t1) {
									ex = ss.Exception.wrap($t1);
									this.IsPrepareDataError = true;
									console.error(ex, []);
									content = (new $JSLib_Mobile_AppGlobalTemplate()).GetErrorPage(this.get_PrepareErrroInfo()).toString();
								}
								$state = 2;
								continue $sm1;
							}
							case 2: {
								$state = -1;
								//清除构建器内容，防止内容重复
								this.contentBuilder.clear();
								this.pageObject = $(content);
								if (this.pageObject.length !== 1) {
									errorInfo = ss.getTypeFullName(ss.getInstanceType(this)) + '页面包含多个顶级对像!';
									console.error(errorInfo, []);
									throw new ss.ArgumentException(errorInfo);
								}
								this.pageObject.on('click', 'a[data-appback]', ss.thisFix(function(elem, event) {
									$JSLib_Mobile_Application.Back(1);
								}));
								$tcs.setResult(this.pageObject);
								return;
							}
							default: {
								break $sm1;
							}
						}
					}
				}
				catch ($t2) {
					$tcs.setException(ss.Exception.wrap($t2));
				}
			});
			$sm();
			return $tcs.task;
		},
		PrepareData: function() {
			return null;
		},
		get_IsDataChanged: function() {
			return this.$3$IsDataChangedField;
		},
		set_IsDataChanged: function(value) {
			this.$3$IsDataChangedField = value;
		}
	}, $JSLib_Mobile_BasePage, [$JSLib_Mobile_IShowLoading]);
	ss.initClass($System_ComponentModel_DataAnnotations_Schema_NoClass, $asm, {});
	ss.initClass($System_Web_WebPages_HelperResult, $asm, {
		toString: function() {
			return this.sb.toString();
		}
	});
	ss.setMetadata($JSLib_Extension_TouchDirection, { enumFlags: true });
	(function() {
		$JSLib_Data_DataBindHelper.bindDatas = [];
	})();
	(function() {
		$JSLib_JSLibConfig.IsUseZepto = false;
		$JSLib_JSLibConfig.PageTransitionEffect = true;
	})();
	(function() {
		$JSLib_SingleTask.$TaskList = null;
		$JSLib_SingleTask.$IsRunning = false;
		$JSLib_SingleTask.$TaskList = new Array();
	})();
	(function() {
		$JSLib_IScrollWraper_IScrollContainer.$releaseToRefresh = '释放以刷新数据...';
	})();
	(function() {
		$JSLib_Mobile_MessageBox.ConfirmHandler = null;
		$JSLib_Mobile_MessageBox.AlertHandler = null;
	})();
	(function() {
		$JSLib_Mobile_NavigationManager.HistoryData = [];
		$JSLib_Mobile_NavigationManager.CurrentBackPageCount = 1;
		$JSLib_Mobile_NavigationManager.CurrentBackPageType = null;
		$JSLib_Mobile_NavigationManager.ibackControls = [];
	})();
	(function() {
		$JSLib_Mobile_PageLoader.RegisterMobilePages = new (ss.makeGenericType(ss.Dictionary$2, [String, Function]))();
	})();
	(function() {
		$JSLib_Mobile_MobilePage.showLoadingMessage = true;
		$JSLib_Mobile_MobilePage.transtionMethod = 'none';
		$JSLib_Mobile_MobilePage.CurrentPage = null;
		$JSLib_Mobile_MobilePage.IsForwarding = false;
		$JSLib_Mobile_MobilePage.IsEntryPage = true;
		$JSLib_Mobile_MobilePage.DomPageRepository = null;
		$JSLib_Mobile_MobilePage.AllMobilePageTypes = [];
		$JSLib_Mobile_MobilePage.CurrentPageZIndex = 1;
		$JSLib_Mobile_MobilePage.OnNavigatingPage = null;
		$JSLib_Mobile_MobilePage.PageContainer = null;
		$JSLib_Mobile_MobilePage.DomPageRepository = [];
	})();
	(function() {
		$JSLib_Mobile_Application.DisableBodyMove = true;
		$JSLib_Mobile_Application.CordovaJSDir = './Resources/JS/';
		$JSLib_Mobile_Application.$PageCssSelector = '.ui-page';
		$JSLib_Mobile_Application.NetWorkState = 1;
		$JSLib_Mobile_Application.IsBrowser = false;
		$JSLib_Mobile_Application.AutoLoadCss = true;
		$JSLib_Mobile_Application.PreserveCss = true;
		$JSLib_Mobile_Application.LoadCssForce = false;
		$JSLib_Mobile_Application.PageIndexFromZero = true;
		$JSLib_Mobile_Application.EnableSwipeLeftToRightToBack = false;
		$JSLib_Mobile_Application.EnableSwipeRightToLeftFoward = false;
		$JSLib_Mobile_Application.BeforeBack = null;
		$JSLib_Mobile_Application.DeviceType = 0;
		$JSLib_Mobile_Application.ApplicationMode = 1;
		$JSLib_Mobile_Application.WebRequestTimeOut = 300000;
		$JSLib_Mobile_Application.$1$OnUserLocationChangeField = null;
		$JSLib_Mobile_Application.BaiduKey = 'lh0NnX14v1EFe2H2U00TseO3';
		$JSLib_Mobile_Application.$_baseNameSpace = null;
		$JSLib_Mobile_Application.RootPath = '../Pages/';
		$JSLib_Mobile_Application.$1$OnApplicationErrorField = null;
		$JSLib_Mobile_Application.EnableSystemLog = false;
		$JSLib_Mobile_Application.CurrentHashString = '';
		$JSLib_Mobile_Application.$1$OnHashChangeField = null;
		//检查 机器类型;
		var userAgent = window.navigator.userAgent;
		if (ss.isValue(userAgent.match(new RegExp('(iPhone|iPod|iPad)')))) {
			$JSLib_Mobile_Application.DeviceType = 1;
		}
		else if (ss.isValue(userAgent.match(new RegExp('Android')))) {
			$JSLib_Mobile_Application.DeviceType = 10;
		}
		else {
			$JSLib_Mobile_Application.DeviceType = 0;
		}
		if ($JSLib_Mobile_Application.DeviceType === 0) {
			$JSLib_Mobile_Application.IsBrowser = true;
		}
		$JSLib_Mobile_Application.RegisterCommonEvent();
	})();
	(function() {
		$JSLib_WebData.dataBaseUrl = null;
	})();
	(function() {
		$JSLib_Widget_Common_LoadingDialog.CurrentZIndex = 1000000;
	})();
	(function() {
		$JSLib_IScrollWraper_IScrollList.$releaseToRefresh = '释放以刷新数据...';
	})();
	(function() {
		$JSLib_TaskQuee.$TaskList = null;
		$JSLib_TaskQuee.$IsRunning = false;
		$JSLib_TaskQuee.$TaskList = new Array();
	})();
	(function() {
		$JSLib_Mobile_PageResourceManager.MaxPageCount = 1;
		window.setInterval($JSLib_Mobile_PageResourceManager.CheckAndReleasePage, 500000000);
	})();
	(function() {
		$JSLib_Oauth_SinaLogin.$isJSLoad = false;
	})();
	(function() {
		$JSLib_UnionLogin_QQLogin.$QQAPPID = '101032040';
		$JSLib_UnionLogin_QQLogin.$isPreparedJs = false;
	})();
	(function() {
		$JSLib_Widget_Panel_ModalPanel.modalPanles = [];
	})();
})();
